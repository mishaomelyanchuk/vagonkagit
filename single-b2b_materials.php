<?php


get_header();

function echo_filesize ( $file_url, $filesize_in_bytes) {
        $filesize_in_mb = number_format($filesize_in_bytes / 1048576, 2) . ' MB';
        $fileInfo = pathinfo($file_url);
        $extension = strtoupper( $fileInfo['extension'] );

        echo $extension . ' ' . $filesize_in_mb;
    }

function get_uri_from_filepath($file) {
    $file_parts = explode("/", $file);
    $need = [];
    $start = false;
    foreach ($file_parts as $part) {
        if($part == "wp-content") $start = true;
        if($start === true) {
            $need[] = $part;
        }
    }
    return "/" . implode("/", $need);
}

global $VG_user;

if($VG_user->user_type == 0) {
    wp_redirect("/");
    exit();
}

$new_posts_count = $VG_user->get_count_new_materials();

$id = get_the_ID();
if(!is_null($id)) {
    $VG_user->post_read($id);
    $post = get_post($id);
    $title = $post->post_title;
    $post_img = get_the_post_thumbnail_url($id);
    $post_content = get_field("text", $id);

    $image_slider = get_field( 'image_slider' );
    foreach ( $image_slider as &$item ) :
        $item = [
            'image' => $item['image']
        ];
    endforeach;

    $files = get_field('files_repeater');

}
 
if ( wpml_get_current_language() == 'uk' ) : 
    $arr = [
        'нулября',
        'січня',
        'лютого',
        'березня',
        'квітня',
        'травня',
        'червня',
        'липня',
        'серпня',
        'вересня',
        'жовтня',
        'листопада',
        'грудня'
    ];
else :
    $arr = [
        'нулября',
        'января',
        'февраля',
        'марта',
        'апреля',
        'мая',
        'июня',
        'июля',
        'августа',
        'сентября',
        'октября',
        'ноября',
        'декабря'
    ];
endif; 
$month = $arr[date('n', strtotime($post->post_date))];
$day = date('d', strtotime($post->post_date));
$year = date('Y', strtotime($post->post_date));
?>

<div class="article">
    <div class="container">
        <section class="section__outer">
            <section class="section__inner">
                <div class="account-wrapper" style="padding: 7px 0;">
                    <div class="account__header">
                        <div class="account__type business-account">
                            <?php if($VG_user->user_type == 0) { ?>
                                <p class="standart_account"><?php echo ( wpml_get_current_language() == 'uk' ) ? 'Стандартний акаунт' : 'Стандартный аккаунт'; ?></p>
                            <?php } else { ?>
                                <p class="b2b_account"><?php echo ( wpml_get_current_language() == 'uk' ) ? 'B2B акаунт' : 'B2B аккаунт'; ?></p>
                            <?php } ?>
                        </div>
                        <div class="account__title account">
                            <?php if ( wpml_get_current_language() == 'uk' ) : ?>
                                <a href="/materials"><span style="font-weight: 600; font-size: 22px;line-height: 33px;color: var(--color-main-accent)">Матеріали</span></a>
                            <?php else : ?>
                                <a href="/ru/materials"><span style="font-weight: 600; font-size: 22px;line-height: 33px;color: var(--color-main-accent)">Материалы</span></a>
                            <?php endif; ?>
                            <?php if($new_posts_count > 0) { ?>
                                <div class="account__count">
                                    <span><?php echo $new_posts_count; ?></span>
                                </div>
                            <?php } ?>
                        </div>
                        <nav class="account__pages">
                            <?php if ( wpml_get_current_language() == 'uk' ) : ?>
                                <ul>
                                    <li><a href="/profile">Профіль</a></li>
                                    <li><a href="/orders">Мої замовлення</a></li>
                                </ul>
                            <?php else: ?>
                                <ul>
                                    <li><a href="/ru/profile">Профиль</a></li>
                                    <li><a href="/ru/orders">Мои заказы</a></li>
                                </ul>
                            <?php endif; ?>
                            <div class="account__pages-toggle">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/svg-arrow-select.svg" alt="">
                            </div>
                        </nav>
                    </div>
                </div>
                <div class="article__wrapper">
                    <div class="article__title"><h1><?php echo $post->post_title; ?></h1></div>
                    <div class="article__date">
                        <div>
                            <p><?php echo $day; ?></p>
                            <span><?php echo $month; ?></span>
                            <span><?php echo $year; ?></span>
                        </div>
                    </div>
                    <?php if(!empty($post_img)) { ?>
                    <picture>
                        <img src="<?php echo $post_img; ?>" alt="">
                    </picture>
                    <?php }  ?>
                    <div class="article__text">
                        <?php echo $post_content; ?>
                    </div>

                    <?php if( !empty( $image_slider ) && empty( $files ) ) : ?>
                        <style> div:after {
                                visibility: hidden;
                            }</style>
                        <div class="article__slider">
                            <div class="article__slider-arrows">
                                <div class="article__slider-next">
                                    <svg width="22" height="63">
                                        <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-left.svg#arrow"></use>
                                    </svg>
                                </div>
                                <div class="article__slider-prev">
                                    <svg width="22" height="63">
                                        <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-right.svg#arrow"></use>
                                    </svg>
                                </div>
                            </div>
                            <ul>
                                <?php foreach( $image_slider as $item ) : ?>
                                    <li>
                                        <div class="product__media-item">
                                            <a href="<?php echo $item['image']; ?>" data-fancybox="product">
                                                <img src="<?php echo $item['image']; ?>" alt="">
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php elseif( !empty( $image_slider) ) : ?>
                        <div class="article__slider">
                            <div class="article__slider-arrows">
                                <div class="article__slider-next">
                                    <svg width="22" height="63">
                                        <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-left.svg#arrow"></use>
                                    </svg>
                                </div>
                                <div class="article__slider-prev">
                                    <svg width="22" height="63">
                                        <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-right.svg#arrow"></use>
                                    </svg>
                                </div>
                            </div>
                            <ul>
                                <?php foreach( $image_slider as $image_item ) : ?>
                                    <li href="<?php echo $image_item['image']; ?>">
                                        <div class="product__media-item">
                                            <a href="<?php echo $image_item['image']; ?>" data-fancybox="product">
                                                <img src="<?php echo $image_item['image']; ?>" alt="">
                                            </a>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                    <?php if( !empty( $files ) ) : ?>
                        <div class="article__files">
                            <ul>
                                <?php foreach( $files as $file_item ) : ?>
                                    <li>
                                        <a href="<?php echo $file_item['file']['url']; ?>"><?php echo $file_item['file']['title'] . ', '; echo_filesize($file_item['file']['url'], $file_item['file']['filesize']);  ?></a>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>

                    <?php if ( wpml_get_current_language() == 'uk' ) : ?>
                        <div class="article__back-link">
                            <a href="/materials">Повернутись до списку публікацій</a>
                        </div>
                    <?php else : ?>
                        <div class="article__back-link">
                            <a href="/ru/materials">Вернуться к списку публикаций</a>
                        </div>
                    <?php endif; ?>
                </div>
            </section>
        </section>
    </div>
</div>

<?php get_footer(); ?>
