<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php 
require_once __DIR__ . "/../../../classes/vg_user.php";
$vg_user = new VG_User();
?>
<?php do_action( 'wpo_wcpdf_before_document', $this->get_type(), $this->order ); ?>

<table class="head container">
	<tr>
		<td class="header">
		<?php
		if ( $this->has_header_logo() ) {
			do_action( 'wpo_wcpdf_before_shop_logo', $this->get_type(), $this->order );
			$this->header_logo();
			do_action( 'wpo_wcpdf_after_shop_logo', $this->get_type(), $this->order );
		} else {
			$this->title();
		}
		?>
		</td>
		<td class="shop-info">
			<?php do_action( 'wpo_wcpdf_before_shop_name', $this->get_type(), $this->order ); ?>
			<div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div>
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->get_type(), $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->get_type(), $this->order ); ?>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->get_type(), $this->order ); ?>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_document_label', $this->get_type(), $this->order ); ?>

<h1 class="document-type-label">
	Рахунок-фактура № <?php echo $this->order_number(); ?>
	<?php //if ( $this->has_header_logo() ) $this->title(); ?>
</h1>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->get_type(), $this->order ); ?>

<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<h3>Отримувач:</h3>
			<?php $user = $vg_user->get_user($this->order->get_user()->data->user_email);
			$display_name = $user->display_name;
			if($display_name !== "") {
				$display_name = explode(" ", $display_name);
				$fname = isset($display_name[0]) ? $display_name[0] : "";
				$name = isset($display_name[1]) ? $display_name[1] : "";
			} 
			?>
			<div class="billing-name"><?php echo $name. ' ' . $fname  ?></div>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<div class="billing-phone"><?php echo $user->phone; ?></div>
		</td>
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->get_type(), $this->order ); ?>
				<?php if ( isset( $this->settings['display_number'] ) ) : ?>
					<tr class="invoice-number">
						<th><?php echo $this->get_number_title(); ?></th>
						<td><?php $this->invoice_number(); ?></td>
					</tr>
				<?php endif; ?>
				<?php if ( isset( $this->settings['display_date'] ) ) : ?>
					<tr class="invoice-date">
						<th><?php echo $this->get_date_title(); ?></th>
						<td><?php $this->invoice_date(); ?></td>
					</tr>
				<?php endif; ?>
				<tr class="order-number">
					<th>Номер замовлення:</th>
					<td><?php $this->order_number(); ?></td>
				</tr>
				<tr class="order-date">
					<th>Дата замовлення:</th>
					<td><?php $this->order_date(); ?></td>
				</tr>
				<?php if ( $payment_method = $this->get_payment_method() ) : ?>
				<tr class="payment-method">
					<th>Спосіб оплати:</th>
					<td><?php echo $payment_method; ?></td>
				</tr>
				<?php endif; ?>
				<?php do_action( 'wpo_wcpdf_after_order_data', $this->get_type(), $this->order ); ?>
			</table>			
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->get_type(), $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product">Продукт</th>
			<th class="quantity">Кількість</th>
			<th class="price">Ціна</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach ( $this->get_order_items() as $item_id => $item ) : ?>
			<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, esc_attr( $this->get_type() ), $this->order, $item_id ); ?>">
				<td class="product">
					<?php $description_label = 'Опис'; ?>
					<span class="item-name"><?php echo $item['name']; ?></span>
					<?php do_action( 'wpo_wcpdf_before_item_meta', $this->get_type(), $item, $this->order  ); ?>
					<span class="item-meta"><?php echo $item['meta']; ?></span>
					<dl class="meta">
						<?php $description_label = 'SKU'; // registering alternate label translation ?>
						<?php if ( ! empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo esc_attr( $item['sku'] ); ?></dd><?php endif; ?>
						<?php if ( ! empty( $item['weight'] ) ) : ?><dt class="weight">Масса:</dt><dd class="weight"><?php echo esc_attr( $item['weight'] ); ?><?php echo esc_attr( get_option( 'woocommerce_weight_unit' ) ); ?></dd><?php endif; ?>
					</dl>
					<?php do_action( 'wpo_wcpdf_after_item_meta', $this->get_type(), $item, $this->order  ); ?>
				</td>
				<td class="quantity"><?php echo $item['quantity']; ?></td>
				<td class="price"><?php echo $item['order_price'] . ' грн.' ; ?></td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders">
				<div class="document-notes">
					<?php do_action( 'wpo_wcpdf_before_document_notes', $this->get_type(), $this->order ); ?>
					<?php if ( $this->get_document_notes() ) : ?>
						<h3>Примітки</h3>
						<?php $this->document_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_document_notes', $this->get_type(), $this->order ); ?>
				</div>
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->get_type(), $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3>Примітки клієнта</h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->get_type(), $this->order ); ?>
				</div>				
			</td>
			<td class="no-borders" colspan="2">
				<table class="totals">
					<tfoot>
						<?php foreach ( $this->get_woocommerce_totals() as $key => $total ) : ?>
							<tr class="<?php echo esc_attr( $key ); ?>">
						
								<?php if ( $total['label'] == 'Итого' ): ?>
									<th class="description">До сплати, <span style="font-weight: 50;">включно з ПДВ</span></th>
								<?php else : ?>
									<?php if ( $total['label'] == 'Подытог' ): ?>
										<th class="description">Підсумок</th>
									<?php else : ?>
										<th class="description"><?php echo $total['label']; ?></th>
									<?php endif; ?>
								<?php endif; ?>
								<?php if ( $total['label'] == 'Доставка' ) : ?>
									<?php if ( get_post_meta( $this->order->get_id(), 'delivery_method' )[0] == 'self' ) : ?>
										<td class="price"><span class="totals-price">Безкоштовно</span></td>
									<?php endif; ?>
									<?php if ( get_post_meta( $this->order->get_id(), 'delivery_method' )[0] == 'novaposhta' ) : ?>
										<td class="price"><span class="totals-price">В відділенні</span></td>
									<?php endif; ?>
									<?php if ( get_post_meta( $this->order->get_id(), 'delivery_method' )[0] == 'courier' ) : ?>
										<td class="price"><span class="totals-price"><?php echo $this->order->get_shipping_total() . '.00 грн.';?></span></td>
									<?php endif; ?>
								<?php else : ?>
									<td class="price"><span class="totals-price"><?php echo $total['value'] . ' грн.'; ?></span></td>
								<?php endif; ?>
							</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->get_type(), $this->order ); ?>

<?php if ( $this->get_footer() ) : ?>
	<div id="footer">
		<!-- hook available: wpo_wcpdf_before_footer -->
		<?php $this->footer(); ?>
		<!-- hook available: wpo_wcpdf_after_footer -->
	</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->get_type(), $this->order ); ?>
