<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$product = wc_get_product( get_the_ID() );

get_header( 'shop' ); ?>
    <div class="product content-area" id="primary" >
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <main id="main" class="site-main product__wrapper" role="main">
                        <?php if ( wp_get_current_user()->data->user_type == '1' ) : ?>
                            <div class="product__item-desktop" data-b2b="<?php echo get_field( 'b2b_sale', 'option' ); ?>">
                        <?php else : ?>
                            <div class="product__item-desktop">
                        <?php endif; ?>

                            <picture class="product__composition">
                                <div class="product__composition-image">
                                    <img src="<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" alt="">
                                </div>
                                <?php if ( !empty( get_field('pattern_image') ) ) : ?>
                                    <div class="product__composition-pattern">
                                        <img src="<?php echo get_field('pattern_image'); ?>" alt="">
                                    </div>
                                <?php endif; ?>
                            </picture>
                            <div class="product__orderinfo pa_czvet">
                                <div class="product__orderinfo-color" data-value=""></div>
                                <?php if ( !empty( $product->get_attribute( 'pa_czvet' ) ) ) :  ?>
                                    <?php $default_obem = $product->get_default_attributes()['pa_obem']; 
                                    $default_color = $product->get_default_attributes()['pa_czvet']; 
                                    if ( !empty( $default_color ) ) :
                                        $default_term_color = get_term_by('slug', $default_color, 'pa_czvet');
                                        ?>
                                        <div class="product__orderinfo-name"><?php echo $default_term_color->name; ?></div>
                                    <?php endif;
                                    if ( !empty( $default_obem ) ) :
                                        $default_term = get_term_by('slug', $default_obem, 'pa_obem');
                                        ?>
                                        <div class="product__orderinfo-size"><?php echo $default_term->name; ?></div>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <?php $default_obem = $product->get_default_attributes()['pa_obem']; 
                                    if ( !empty( $default_obem ) ) :
                                        $default_term = get_term_by('slug', $default_obem, 'pa_obem');
                                        ?>
                                        <div class="product__orderinfo-size"><?php echo $default_term->name; ?></div>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <i>
                                    <svg height="24px" width="24px">
                                        <use href="../static/svg-edit.svg#edit"></use>
                                    </svg>
                                </i>
                            </div>
                            <div class="product__order">
                                <div class="product__order-counter">
                                    <button class="counter-button" data-value="minus">
                                        <svg width="14px" height="2px">
                                            <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#minus"></use>
                                        </svg>
                                    </button>
                                    <input data-value="display" type="number" min="1" value="1">
                                    <button class="counter-button" data-value="plus">
                                        <svg width="14px" height="14px">
                                            <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#plus"></use>
                                        </svg>
                                    </button>
                                </div>
                                <div class="product__order-trade">
                                    <div class="product__order-discount">

                                    </div>
                                    <div class="product__order-price">

                                    </div>
                                    <div class="product__order-promo" style="display: none">
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                        <span>Акція</span>
                                    <?php else : ?>
                                        <span>Акция</span>
                                    <?php endif; ?>
                                    </div>
                                </div>
                                <div class="product__order-submit">
                                    <button class="btn btn-colored btn-small"><?php echo get_field('catalog','option')['tekst_v_korzinu']?></button>
                                </div>
                            </div>
                        </div>
                        <picture class="product__composition">
                            <div class="product__composition-image">
                                <img src="<?php echo wp_get_attachment_url( $product->get_image_id() ); ?>" alt="">
                            </div>
                            <?php if ( !empty( get_field('pattern_image') ) ) : ?>
                                <div class="product__composition-pattern">
                                    <img src="<?php echo get_field('pattern_image'); ?>" alt="">
                                </div>
                            <?php endif; ?>
                        </picture>
                        <div class="product__orderinfo">

                        </div>
                        <div class="product__order">
                            <div class="product__order-counter">
                                <button class="counter-button" data-value="minus">
                                    <svg width="14px" height="2px">
                                        <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#minus"></use>
                                    </svg>
                                </button>
                                <input data-value="display" type="number" min="1" value="1">
                                <button class="counter-button" data-value="plus">
                                    <svg width="14px" height="14px">
                                        <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#plus"></use>
                                    </svg>
                                </button>
                            </div>
                            <div class="product__order-trade">
                                <div class="product__order-discount">

                                </div>
                                <div class="product__order-price">

                                </div>
                                <div class="product__order-promo" style="display: none">
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                        <span>Акція</span>
                                    <?php else : ?>
                                        <span>Акция</span>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="product__order-submit">
                                <button class="btn btn-colored btn-small"><?php echo get_field('catalog','option')['tekst_v_korzinu']?></button>
                            </div>
                        </div>





                        <div class="product__links">
                            <ul>
                                <li class="active"><a href="#">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <span>Опис</span>
                                <?php else : ?>
                                    <span>Описание</span>
                                <?php endif; ?>
                                </a></li>
                                <?php if ( !empty( get_field('recomendation' ) ) ) : ?>
                                    <li><a href="#recommendations">
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                        <span>Рекомендації</span>
                                    <?php else : ?>
                                        <span>Рекомендации</span>
                                    <?php endif; ?>
                                    </a></li>
                                <?php endif; ?>
                                <?php if ( !empty( get_field('foto_and_video' )['video_link'] ) or !empty( get_field('foto_and_video' )['fotos'][0] ) ) : ?>
                                    <li><a href="#media">
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                        <span>Фото/відео</span>
                                    <?php else : ?>
                                        <span>Фото/видео</span>
                                    <?php endif; ?>
                                    </a></li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="product__title">
                            <h1><?php echo get_the_title(); ?></h1>
                        </div>
                        <?php if ( !empty( get_field( 'sale_text', get_the_ID() )) or  !empty( get_field( 'sale_flash', get_the_ID() ) ) ) : ?>
                            <div class="product__promo">
                                <?php if ( !empty( get_field( 'sale_flash', get_the_ID() ) )) : ?>
                                <div class="product__promo-label">
                                    <span><?php echo get_field( 'sale_flash', get_the_ID() ); ?></span>
                                </div>
                                <?php endif; ?>
                                <?php if ( !empty( get_field( 'sale_text', get_the_ID() )) ) : ?>
                                <div class="product__promo-info">
                                    <p>
                                    <?php echo get_field( 'sale_text', get_the_ID() ); ?>
                                    </p>
                                </div>
                            <?php endif; ?>
                            </div>
                        <?php endif; ?>

                        <div class="product__sizes">
                          <?php while ( have_posts() ) : ?>
                            <?php the_post(); ?>

                            <?php wc_get_template_part( 'content', 'single-product' ); ?>

                          <?php endwhile; ?>
                        </div>
                        <div class="product__colors">
                        </div>
                        <div class="product__colors-values">
                        </div>


                        <div class="product__info">
                            <?php echo get_the_content(); ?>
                        </div>
                        <?php
                        $tags = get_the_terms( get_the_ID(), 'product_tag' );
                        ?>
                        <?php if ( !empty( $tags[0] ) ) : ?>
                        <div class="product__use">
                            <h4>
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                <span>Застосування</span>
                            <?php else : ?>
                                <span>Применение</span>
                            <?php endif; ?>
                                
                            </h4>
                            <div class="product__use-list">
                                <ul>
                                <?php foreach ( $tags as $item ) : ?>
                                    <li><a href="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ).'?prod_tag='.$item->slug; ?>"><?php echo $item->name; ?></a></li>
                                <?php endforeach; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php if ( !empty( get_field('recomendation' ) ) ) : ?>
                        <div id="recommendations" class="product__rec-of-use">
                            <h3>
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                <span>Рекомендації до застосування</span>
                            <?php else : ?>
                                <span>Рекомендации к применению</span>
                            <?php endif; ?>
                            </h3>
                            <?php echo get_field( 'recomendation' ); ?>
                        </div>
                        <?php endif; ?>
                        <?php if ( !empty( get_field('foto_and_video' )['video_link'] ) or !empty( get_field('foto_and_video' )['fotos'][0] ) ) : ?>
                        <div id="media" class="product__media">
                            <h3>
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                Фото/відео
                            <?php else : ?>
                                Фото/видео
                            <?php endif; ?>
                            </h3>
                            <div class="product__media-items">
                                <?php if ( !empty(get_field('foto_and_video')['video_link']) ) :
                                preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', get_field('foto_and_video')['video_link'], $match);
                                $youtube_id = $match[1];
                                ?>
                                <div class="product__media-item">
                                <a href="<?php echo get_field('foto_and_video')['video_link']; ?>" data-type="iframe" data-fancybox="product" data-autoplay="false">
                                    <img src='https://i.ytimg.com/vi/<?php echo $youtube_id?>/hqdefault.jpg' alt="">
                                </a>
                                </div>
                                <?php endif; ?>
                                <?php foreach ( get_field('foto_and_video')['fotos'] as $item ) : ?>
                                <div class="product__media-item">
                                <a href="<?php echo $item['foto']; ?>" data-fancybox="product">
                                    <img src="<?php echo $item['foto']; ?>" alt="">
                                </a>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                    </main>
                </section>
            </section>
        </div>
    </div>
<?php
( new Blog_Section() )->render();
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
