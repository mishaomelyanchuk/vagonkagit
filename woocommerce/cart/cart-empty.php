<?php
/**
 * Empty cart page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cart-empty.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

defined( 'ABSPATH' ) || exit;

/*
 * @hooked wc_empty_cart_message - 10
 */

if ( wc_get_page_id( 'shop' ) > 0 ) : ?>
    <div class="cart">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="cart__wrapper">
                        <div class="cart__title">
                            <h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
                        </div>
                        <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            <p>В кошику поки немає товарів</p>
                        <?php else : ?>
                            <p>В корзине пока нет товаров</p>
                        <?php endif; ?>
                    </div>
                </section>
            </section>
        </div>
    </div>
<?php endif; ?>
