<?php 
$cart_item = WC()->cart->get_cart();
$cart_count = WC()->cart->get_cart_contents_count();
if ( ! WC()->cart->prices_include_tax ) {
  $cart_total = WC()->cart->cart_contents_total;
} else {
  $cart_total = WC()->cart->cart_contents_total + WC()->cart->tax_total;
}
$cart_total = explode('.' , number_format($cart_total, 2, ".", " "));
?>
<div class="cart">
	<div class="container">
	<section class="section__outer">
		<section class="section__inner">
            <div class="cart__wrapper">
                <div class="cart__title">
                    <h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
                </div>
                <?php if ( !empty( $cart_item ) ) : ?>
                    <ul class="cart__list">
                        <?php $count = 1; ?>
                        <?php foreach ( $cart_item as $item => $values ) :
                            $variation = wc_get_product( $values['data']->get_id() );
                            $product = wc_get_product( $variation->get_parent_id() );
                            $attributes =  $variation->get_variation_attributes() ;
                            $obem = get_term_by('slug',  $attributes['attribute_pa_obem'], 'pa_obem')->name;
                            $color = get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->name;
                            $line_price = number_format($values['line_total'], 2, ".", " ");
                            $price = explode('.' , $line_price);
                            ?>
                            <li class="cart__list-item">
                                <div class="cart__product">
                                <div class="cart__product-position">
                                <span><?php echo $count; ?></span>
                                </div>
                                <picture>
                                    <img src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" alt="">
                                </picture>
                                <div class="cart__product-title">
                                    <a href="<?php echo get_permalink( $product->get_id() ); ?>" target="_blank"><?php echo $product->get_title(); ?></a>
                                </div>
                                <div class="cart__product-info">
                                <div class="cart__product-info_size"><?php echo $obem; ?></div>
                                <div class="cart__product-info_color" style="background-color: <?php echo get_field( 'color', 'term_'.get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->term_id );?>"></div>
                                <div class="cart__product-info_name"><?php echo $color; ?></div>
                                <i>
                                <svg height="24px" width="24px">
                                    <use href="<?=TEMPLATE_PATH?>tatic/svg-edit.svg#edit"></use>
                                </svg>
                                </i>
                                </div>
                                <div class="cart__product-count">
                                <div class="product__order-counter">
                                <button class="counter-button" data-value="minus">
                                    <svg width="14px" height="2px">
                                    <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#minus"></use>
                                    </svg>
                                </button>
                                <input data-value="display" type="number" onkeyup="this.value = this.value.replace (/\D/, '')" min="0" value="<?php echo $values['quantity']?>">
                                <button class="counter-button" data-value="plus">
                                    <svg width="14px" height="14px">
                                    <use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#plus"></use>
                                    </svg>
                                </button>
                                </div>
                                </div>
                                <div class="cart__product-summary">
                                <div class="cart__product-summary_total">
                                <p>
                                    <?php echo $price[0]; ?><span>.<?php echo $price[1]; ?></span><span> </span><span class="currency">₴</span>
                                </p>
                                </div>
                                <div class="cart__product-summary_price">
                                <ul>
                                    <?php if ( $variation->is_on_sale() ) : ?>
                                        <li class="price-new">
                                        <?php
                                         if ( wp_get_current_user()->data->user_type == '1' ) : 
                                            $temp = $variation->get_sale_price();
                                            $new_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                         else : 
                                            $new_price = $variation->get_sale_price();
                                           endif; 
                                          $var_price = number_format($new_price, 2, ".", " ");
                                          echo $var_price;
                                        ?> <span class="currency">₴</span> за шт.
                                        </li>
                                        <li class="price-old">
                                          <?php
                                          if ( wp_get_current_user()->data->user_type == '1' ) : 
                                            $temp = $variation->get_regular_price();
                                            $old_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                         else : 
                                            $old_price = $variation->get_regular_price();
                                           endif; 
                                          $var_price = number_format($old_price, 2, ".", " ");
                                          echo $var_price;
                                          ?> <span class="currency"> ₴</span></li>
                                    <?php else : ?>
                                        <li class="price-new">
                                          <?php
                                          $discount_price = number_format($variation->get_price(), 2, ".", " ");
                                          echo $discount_price;
                                          ?> <span class="currency">₴</span> за шт.
                                        </li>
                                    <?php endif; ?>
                                </ul>
                                </div>
                                </div>
                                <div class="cart__product-remove" data-cartID="<?php echo $item; ?>">
                                <i>
                                <svg height="20px" width="18px">
                                    <use href="<?=TEMPLATE_PATH?>static/svg-remove.svg#remove"></use>
                                </svg>
                                </i>
                                </div>
                                </div>
                            </li>
                        <?php $count++; ?>
                        <?php endforeach; ?>

                    </ul>
                    <div class="cart__total">
                    <div class="cart__total-price">
                        <p>
                          <?php echo $cart_total[0]; ?><span>.<?php echo $cart_total[1]; ?> </span><span class="currency">₴ </span><span class="price-text">
                          <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            всього
                        <?php else : ?>
                            итого
                        <?php endif; ?>
                          </span>
                        </p>
                    </div>
                    <div class="cart__total-order">
                        <a href="<?php echo wc_get_checkout_url(); ?>" class="btn btn-colored">
                        <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            Оформити замовлення
                        <?php else : ?>
                            Оформить заказ
                        <?php endif; ?>
                        </a>
                    </div>
                </div>
                <?php else : ?>
                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                        <p>В кошику поки немає товарів</p>
                    <?php else : ?>
                        <p>В корзине пока нет товаров</p>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
		</section>
	</section>
	</div>
</div>
