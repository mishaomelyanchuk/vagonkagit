<?php
/*
 * Template name: Liqpay Success
 */
$order = wc_get_order(base64_decode($_GET['order']));
$order_status = $order->get_status();
if ( $order_status == 'processing' ) :
    if ( $_GET['new_user'] ) {
        $redirect_url = $order->get_checkout_order_received_url() . '?new_user=1';
    } else {
        $redirect_url = $order->get_checkout_order_received_url();
    }
    wp_redirect($redirect_url); die;
endif; 
?>
<?php get_header(); ?>
    <div class="order">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="order__wrapper">
                        <div class="order__title">
                        <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            <h1>Оформлення замовлення</h1>
                        <?php else : ?>
                            <h1>Оформление заказа</h1>
                        <?php endif; ?>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
<?php if(isset($_GET['order'])) { ?>
    <?php if ( $order_status == 'processing' ) :
    $order = wc_get_order( base64_decode($_GET['order']) );
    if ( $_GET['new_user'] ) {
        $redirect_url = $order->get_checkout_order_received_url() . '?new_user=1';
    } else {
        $redirect_url = $order->get_checkout_order_received_url();
    }
    ?>
    <script>
        window.location.href = '<?php echo $redirect_url; ?>';
    </script>
    <!-- <div class="order__details">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="order__details-wrapper">
                        <div class="order__products">
                            <div class="order__products-header">
                                <div>
                                    <p>
                                        <?php echo str_replace( '[order_id]', '№ ' . base64_decode($_GET['order']), get_field( 'checkout_settings', 'option' )['checkout_text']); ?>
                                    </p>
                                    <br><br><br>
                                    <?php if ( !empty( $_GET['new_user'] ) ) : ?>
                                        <p><?php echo get_field('success_order','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></p><br><br>
                                    <?php endif; ?> 
                                    <div class="order__checkout-submit">
                                        <button type="button" class="btn btn-colored btn-big" onclick="window.location.href='<?php echo get_field( 'checkout_settings', 'option' )['button']['url']; ?>';"><?php echo get_field( 'checkout_settings', 'option' )['button']['title']; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div> -->
    <?php else : ?>
        <div class="order__details">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="order__details-wrapper">
                        <div class="order__products">
                            <div class="order__products-header">
                                <div>
                                    <?php 
                                     require_once __DIR__ . "/classes/LiqPay.php";
                                     $settings = get_option('woocommerce_liqpay-webplus_settings');
                                     $liqp = array(
                                         'action' => 'pay',
                                         'amount' => $order->get_total(),
                                         'currency' => 'UAH',
                                         'description' => 'Заказ №' . base64_decode($_GET['order']),
                                         'order_id' => base64_decode($_GET['order']),
                                         'language' => 'ru',
                                         'version' => '3',
                                         'server_url' => str_replace( '/ru', '' , home_url() ) . '/wp-admin/admin-ajax.php?action=set_new_order_status' ,
                                         'result_url' => Page_Liqpay_Success::get_url() . '?order='. $_GET['order'] . '&new_user=' ,
                                         'type' => 'buy'
                                     );
                                     $liqPay = new LiqPay($settings["public_key"], $settings["private_key"]);
                                     $link = $liqPay->cnb_link($liqp);
                                    ?>
                                    <p>
                                    <?php echo get_field('success_order','option')['tekst_u_vas_est_chas_dlya_povtornoj_oplaty_zakaza_vy_mozhete_sdelat_eto_po_ssylke_v_nizu_ili_po_ssylka_v_moem_kabineta_na_danom_zakaze']; ?>
                                    </p>
                                    <br><br><br>
                                    <p>
                                        <a href="<?php echo $link?>"> <?php echo get_field('success_order','option')['tekst_ssylka_na_povtornuyu_oplatu']; ?></a>
                                    </p>
                                    <br><br>

                                    <?php if ( !empty( $_GET['new_user'] ) ) : ?>
                                        <p><?php echo get_field('success_order','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></p><br><br>
                                    <?php endif; ?> 
                                    <div class="order__checkout-submit">
                                        <button type="button" class="btn btn-colored btn-big" onclick="window.location.href='<?php echo get_field( 'checkout_settings', 'option' )['button']['url']; ?>';"><?php echo get_field( 'checkout_settings', 'option' )['button']['title']; ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
    <?php endif; ?>
<?php } ?>



<?php 

get_footer(); ?>

