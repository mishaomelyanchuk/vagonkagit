<?php
/**
 * Twenty Twenty-Two functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_Two
 * @since Twenty Twenty-Two 1.0
 */


define( 'TEMPLATE_PATH', get_template_directory_uri() . '/' );

require_once __DIR__ .'/includes/theme/autoloader.php';
Theme_AutoLoader::init();
add_theme_support( 'woocommerce' );

if ( ! function_exists( 'twentytwentytwo_support' ) ) :

	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function twentytwentytwo_support() {

		// Add support for block styles.
		add_theme_support( 'wp-block-styles' );

		// Enqueue editor styles.
		add_editor_style( 'style.css' );

	}

endif;

add_action( 'after_setup_theme', 'twentytwentytwo_support' );

if ( ! function_exists( 'twentytwentytwo_styles' ) ) :

	/**
	 * Enqueue styles.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function twentytwentytwo_styles() {
		// Register theme stylesheet.
		$theme_version = wp_get_theme()->get( 'Version' );

		$version_string = is_string( $theme_version ) ? $theme_version : false;
		wp_register_style(
			'twentytwentytwo-style',
			get_template_directory_uri() . '/style.css',
			array(),
			$version_string
		);

		// Add styles inline.
		wp_add_inline_style( 'twentytwentytwo-style', twentytwentytwo_get_font_face_styles() );

		// Enqueue theme stylesheet.
		wp_enqueue_style( 'twentytwentytwo-style' );

	}

endif;

add_action( 'wp_enqueue_scripts', 'twentytwentytwo_styles' );

if ( ! function_exists( 'twentytwentytwo_editor_styles' ) ) :

	/**
	 * Enqueue editor styles.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function twentytwentytwo_editor_styles() {

		// Add styles inline.
		wp_add_inline_style( 'wp-block-library', twentytwentytwo_get_font_face_styles() );

	}

endif;

add_action( 'admin_init', 'twentytwentytwo_editor_styles' );


if ( ! function_exists( 'twentytwentytwo_get_font_face_styles' ) ) :

	/**
	 * Get font face styles.
	 * Called by functions twentytwentytwo_styles() and twentytwentytwo_editor_styles() above.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return string
	 */
	function twentytwentytwo_get_font_face_styles() {

		return "
		@font-face{
			font-family: 'Source Serif Pro';
			font-weight: 200 900;
			font-style: normal;
			font-stretch: normal;
			font-display: swap;
			src: url('" . get_theme_file_uri( 'assets/fonts/SourceSerif4Variable-Roman.ttf.woff2' ) . "') format('woff2');
		}

		@font-face{
			font-family: 'Source Serif Pro';
			font-weight: 200 900;
			font-style: italic;
			font-stretch: normal;
			font-display: swap;
			src: url('" . get_theme_file_uri( 'assets/fonts/SourceSerif4Variable-Italic.ttf.woff2' ) . "') format('woff2');
		}
		";

	}

endif;

if ( ! function_exists( 'twentytwentytwo_preload_webfonts' ) ) :

	/**
	 * Preloads the main web font to improve performance.
	 *
	 * Only the main web font (font-style: normal) is preloaded here since that font is always relevant (it is used
	 * on every heading, for example). The other font is only needed if there is any applicable content in italic style,
	 * and therefore preloading it would in most cases regress performance when that font would otherwise not be loaded
	 * at all.
	 *
	 * @since Twenty Twenty-Two 1.0
	 *
	 * @return void
	 */
	function twentytwentytwo_preload_webfonts() {
		?>
		<link rel="preload" href="<?php echo esc_url( get_theme_file_uri( 'assets/fonts/SourceSerif4Variable-Roman.ttf.woff2' ) ); ?>" as="font" type="font/woff2" crossorigin>
		<?php
	}

endif;



add_action( 'wp_head', 'twentytwentytwo_preload_webfonts' );

// Add block patterns
require get_template_directory() . '/inc/block-patterns.php';


add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
function my_acf_google_map_api( $args ) {
    $args['key'] = 'AIzaSyBj70DjlmDfi-cP-IyRWVV7t-tLXHkNpGQ&amp';
    return $args;
}
// PAGINATION
function vagonka_paginate_links( $args = '' ) {
    global $wp_query, $wp_rewrite;

    // Setting up default values based on the current URL.
    $pagenum_link = html_entity_decode( get_pagenum_link() );
    $url_parts    = explode( '?', $pagenum_link );

    // Get max pages and current page out of the current query, if available.
    $total   = isset( $wp_query->max_num_pages ) ? $wp_query->max_num_pages : 1;
    $current = get_query_var( 'paged' ) ? (int) get_query_var( 'paged' ) : 1;

    // Append the format placeholder to the base URL.
    $pagenum_link = trailingslashit( $url_parts[0] ) . '%_%';

    // URL base depends on permalink settings.
    $format  = $wp_rewrite->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
    $format .= $wp_rewrite->using_permalinks() ? user_trailingslashit( $wp_rewrite->pagination_base . '/%#%', 'paged' ) : '?paged=%#%';

    $defaults = array(
        'base'               => $pagenum_link, // http://example.com/all_posts.php%_% : %_% is replaced by format (below).
        'format'             => $format, // ?page=%#% : %#% is replaced by the page number.
        'total'              => $total,
        'current'            => $current,
        'aria_current'       => 'page',
        'show_all'           => false,
        'prev_next'          => false,
        'prev_text'          => '',
        'next_text'          => '',
        'end_size'           => 1,
        'mid_size'           => 2,
        'type'               => 'plain',
        'add_args'           => array(), // Array of query args to add.
        'add_fragment'       => '',
        'before_page_number' => '',
        'after_page_number'  => '',
    );

    $args = wp_parse_args( $args, $defaults );

    if ( ! is_array( $args['add_args'] ) ) {
        $args['add_args'] = array();
    }

    // Merge additional query vars found in the original URL into 'add_args' array.
    if ( isset( $url_parts[1] ) ) {
        // Find the format argument.
        $format       = explode( '?', str_replace( '%_%', $args['format'], $args['base'] ) );
        $format_query = isset( $format[1] ) ? $format[1] : '';
        wp_parse_str( $format_query, $format_args );

        // Find the query args of the requested URL.
        wp_parse_str( $url_parts[1], $url_query_args );

        // Remove the format argument from the array of query arguments, to avoid overwriting custom format.
        foreach ( $format_args as $format_arg => $format_arg_value ) {
            unset( $url_query_args[ $format_arg ] );
        }

        $args['add_args'] = array_merge( $args['add_args'], urlencode_deep( $url_query_args ) );
    }

    // Who knows what else people pass in $args.
    $total = (int) $args['total'];
    if ( $total < 2 ) {
        return;
    }
    $current  = (int) $args['current'];
    $end_size = (int) $args['end_size']; // Out of bounds? Make it the default.
    if ( $end_size < 1 ) {
        $end_size = 1;
    }
    $mid_size = (int) $args['mid_size'];
    if ( $mid_size < 0 ) {
        $mid_size = 2;
    }

    $add_args   = $args['add_args'];
    $r          = '';
    $page_links = array();
    $dots       = false;

    if ( $args['prev_next'] && $current && 1 < $current ) :
        $link = str_replace( '%_%', 2 == $current ? '' : $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current - 1, $link );
        if ( $add_args ) {
            $link = add_query_arg( $add_args, $link );
        }
        $link .= $args['add_fragment'];

        $page_links[] = sprintf(
            '<a class="prev page-numbers" href="%s">%s</a>',
            /**
             * Filters the paginated links for the given archive pages.
             *
             * @since 3.0.0
             *
             * @param string $link The paginated link URL.
             */
            esc_url( apply_filters( 'paginate_links', $link ) ),
            $args['prev_text']
        );
    endif;

    for ( $n = 1; $n <= $total; $n++ ) :
        if ( $n == $current ) :
            $page_links[] = sprintf(
//                '<span aria-current="%s" class="page-numbers current">%s</span>'
                '<li class="active"><a %s >%s</a></li>',
                esc_attr( $args['aria_current'] ),
                $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number']
            );

            $dots = true;
        else :
            if ( $args['show_all'] || ( $n <= $end_size || ( $current && $n >= $current - $mid_size && $n <= $current + $mid_size ) || $n > $total - $end_size ) ) :
                $link = str_replace( '%_%', 1 == $n ? '' : $args['format'], $args['base'] );
                $link = str_replace( '%#%', $n, $link );
                if ( $add_args ) {
                    $link = add_query_arg( $add_args, $link );
                }
                $link .= $args['add_fragment'];

                $page_links[] = sprintf(
//                    '<a class="page-numbers" href="%s">%s</a>'
                    '<li><a href="%s">%s</a></li>',
                    /** This filter is documented in wp-includes/general-template.php */
                    esc_url( apply_filters( 'paginate_links', $link ) ),
                    $args['before_page_number'] . number_format_i18n( $n ) . $args['after_page_number']
                );

                $dots = true;
            elseif ( $dots && ! $args['show_all'] ) :
//                $page_links[] = '<span class="page-numbers dots">' . __( '&hellip;' ) . '</span>';
                $page_links[] = '<li><a>...</a>';

                $dots = false;
            endif;
        endif;
    endfor;

    if ( $args['prev_next'] && $current && $current < $total ) :
        $link = str_replace( '%_%', $args['format'], $args['base'] );
        $link = str_replace( '%#%', $current + 1, $link );
        if ( $add_args ) {
            $link = add_query_arg( $add_args, $link );
        }
        $link .= $args['add_fragment'];

        $page_links[] = sprintf(
            '<a class="next page-numbers" href="%s">%s</a>',
            /** This filter is documented in wp-includes/general-template.php */
            esc_url( apply_filters( 'paginate_links', $link ) ),
            $args['next_text']
        );
    endif;

    switch ( $args['type'] ) {
        case 'array':
            return $page_links;

        case 'list':
            $r .= "<ul class='page-numbers'>\n\t<li>";
            $r .= implode( "</li>\n\t<li>", $page_links );
            $r .= "</li>\n</ul>\n";
            break;

        default:
            $r = implode( "\n", $page_links );
            break;
    }

    /**
     * Filters the HTML output of paginated links for archives.
     *
     * @since 5.7.0
     *
     * @param string $r    HTML output.
     * @param array  $args An array of arguments. See paginate_links()
     *                     for information on accepted arguments.
     */
    $r = apply_filters( 'paginate_links_output', $r, $args );

    return $r;
}

// add_filter("the_content", "plugin_myContentFilter");

//   function plugin_myContentFilter($content)
//   {
//     return substr($content, 0, 250);
//   }
  
  add_filter( 'woocommerce_currencies', 'add_my_currency' );
  
function add_my_currency( $currencies ) {
$currencies['ukr'] = __( '₴', 'woocommerce' );
return $currencies;
}

add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
  
function add_my_currency_symbol( $currency_symbol, $currency ) {
switch( $currency ) {
case 'ukr': $currency_symbol = '₴'; break;
}
return $currency_symbol;
}

wp_enqueue_script('registration.js', get_template_directory_uri() . '/js/registration.js', array(), false, true);
wp_enqueue_script('cabinet.js', get_template_directory_uri() . '/js/cabinet.js', array(), false, true);
wp_enqueue_script('login.js', get_template_directory_uri() . '/js/login.js', array(), false, true);
wp_enqueue_script('order.js', get_template_directory_uri() . '/js/order.js', array(), false, true);
wp_enqueue_script('novaposhta.js', get_template_directory_uri() . '/js/novaposhta.js', array(), false, true);
wp_enqueue_script('liqpay.js', get_template_directory_uri() . '/js/liqpay.js', array(), false, true);
wp_enqueue_script('checkout.js', get_template_directory_uri() . '/js/checkout.js', array(), false, true);
wp_enqueue_script('validations.js', get_template_directory_uri() . '/js/validations.js', array(), false, true);
wp_enqueue_script('mdl-jquery-modal-dialog.js', get_template_directory_uri() . '/js/mdl-jquery-modal-dialog.js', array(), false, true);
wp_enqueue_style('registration.css', get_template_directory_uri() . '/css/registration.css');
wp_enqueue_style('cabinet.css', get_template_directory_uri() . '/css/cabinet.css');
wp_enqueue_style('mdl-jquery-modal-dialog.css', get_template_directory_uri() . '/css/mdl-jquery-modal-dialog.css');

//wp_enqueue_script('slick.js', get_template_directory_uri() . '/js/slick.js', array(), false, true);
//wp_enqueue_style('slick.css', get_template_directory_uri() . '/css/slick.css');

add_action("wp_ajax_login", "customer_login");

add_action( 'wp_ajax_nopriv_customer_login', 'customer_login' );
add_action( 'wp_ajax_customer_login', 'customer_login' );

add_action( 'wp_ajax_nopriv_checkout_login', 'checkout_login' );
add_action( 'wp_ajax_checkout_login', 'checkout_login' );

add_action( 'wp_ajax_nopriv_checkout_registration', 'checkout_registration' );
add_action( 'wp_ajax_checkout_registration', 'checkout_registration' );

add_action( 'wp_ajax_nopriv_reading_post', 'reading_post' );
add_action( 'wp_ajax_reading_post', 'reading_post' );

add_action( 'wp_ajax_nopriv_novaposhta_cities', 'novaposhta_cities' );
add_action( 'wp_ajax_novaposhta_cities', 'novaposhta_cities' );

add_action( 'wp_ajax_nopriv_novaposhta_houses', 'novaposhta_houses' );
add_action( 'wp_ajax_novaposhta_houses', 'novaposhta_houses' );

add_action( 'wp_ajax_nopriv_liq_pay', 'liq_pay' );
add_action( 'wp_ajax_liq_pay', 'liq_pay' );

add_action( 'wp_ajax_nopriv_promo_code', 'promo_code' );
add_action( 'wp_ajax_promo_code', 'promo_code' );

function customer_login() {
    $email = isset($_POST["email"]) ? $_POST["email"] : null;
    $pass = isset($_POST["pass"]) ? $_POST["pass"]: null;

    if(is_null($email) || is_null($pass)) {
        echo json_encode(array(
            "res" => -1,
            "message" => "Ошибка в параметрах запроса"
        ));
        die();
    }

    $creds = array(
        'user_login'    => $email,
        'user_password' => $pass,
        'remember'      => true
    );

    $user = wp_signon( $creds, false );

    if ( is_wp_error( $user ) ) {
        echo json_encode(array(
            "res" => -1,
            "message" => get_field('header','option')['tekst_nevernyj_email_ili_parol'],
        ));
        exit;
    }

    echo json_encode(array(
        "res" => 0,
        "user" => $user
    ));
    exit;
}

function checkout_login() {
    $email = isset($_POST["email"]) ? $_POST["email"] : null;
    $pass = isset($_POST["pass"]) ? $_POST["pass"]: null;

    if(is_null($email) || is_null($pass)) {
        echo json_encode(array(
            "res" => -1,
            "message" => "Ошибка в параметрах запроса"
        ));
        die();
    }

    $creds = array(
        'user_login'    => $email,
        'user_password' => $pass,
        'remember'      => true
    );

    $user = wp_signon( $creds, false );

    if ( is_wp_error( $user ) ) {
        echo json_encode(array(
            "res" => -1,
            "message" => get_field('header','option')['tekst_nevernyj_email_ili_parol']
        ));
        exit;
    }
   
    $phone = $user->data->phone;
    $display_name = $user->data->display_name;
    $id =  $user->data->ID;
    $user_email =  $user->data->user_email;
    $return_user = [];
    if($display_name !== "") {
        $display_name = explode(" ", $display_name);
        $return_user["id"] = isset($id) ? $id : "";
        $return_user["customer_fname"] = isset($display_name[0]) ? $display_name[0] : "";
        $return_user["customer_name"] = isset($display_name[1]) ? $display_name[1] : "";
        $return_user["phone"] = isset($phone) ? $phone : "";
        $return_user["user_email"] = isset($user_email) ? $user_email : "";
    }

    echo json_encode(array(
        "res" => 0,
        "redirect" => wc_get_checkout_url()
    ));
    exit;
}

function checkout_registration() {
    require_once __DIR__ . "/classes/vg_user.php";
    $vg_user = new VG_User();
    
    $fname = isset($_POST["fname"]) ? $_POST["fname"] : null;
    $name = isset($_POST["name"]) ? $_POST["name"]: null;
    $email = isset($_POST["email"]) ? $_POST["email"] : null;
    $phone = isset($_POST["phone"]) ? $_POST["phone"]: null;

   
  
   
    if(!empty($fname) && !empty($name) && !empty($email) && !empty($phone)) {
        $user_data = (array)$vg_user->get_user($email);
        if ( empty( $user_data  ) ) {
            $register_data = $vg_user->add_user($fname, $name, $email, $phone, 0, "", "", "", 0);
            
            $result = wp_signon(array(
                'user_login'    => $register_data["login"],
                'user_password' => $register_data["pass"],
                'remember'      => true
            ));
            $id =  $result->data->ID;
            if ( !empty( $result ) ) {
                $return_user = [];
        
                $display_name = explode(" ", $display_name);
                $return_user["id"] = isset($id) ? $id : "";
                $return_user["customer_fname"] = isset($fname) ? $fname : "";
                $return_user["customer_name"] = isset($name) ? $name : "";
                $return_user["phone"] = isset($phone) ? $phone : "";
                $return_user["user_email"] = isset($email) ? $email : "";
    
                echo json_encode(array(
                    "res" => 0,
                    "user" => $return_user
                ));
            }
            
        } else {
            echo json_encode(array(
                "res" => -1,
                "message" => "Ошибка"
            ));
            die();
        }
    }

    exit;
}

function reading_post() {
    require_once __DIR__ . "/classes/vg_user.php";
    $post_id = $_POST["post_id"];
    $vg_user = new VG_User();
    $vg_user->post_read($post_id);
    echo json_encode(array(
        "res" => 0
    ));
    exit;
}

function novaposhta_cities() {
    global $wpdb;
    $area_ref = isset($_POST["region_id"]) ? $_POST["region_id"] : null;
    $cities = $wpdb->get_results(sprintf("SELECT * FROM `wc_ukr_shipping_np_cities` WHERE `area_ref` = '%s' ORDER BY `description`", $area_ref));
    echo json_encode(array(
        "res" => 0,
        "cities" => $cities
    ));
    exit;
}

function novaposhta_houses() {
    global $wpdb;
    $city_ref = isset($_POST["city_id"]) ? $_POST["city_id"] : null;
    $houses = $wpdb->get_results(sprintf("SELECT * FROM `wc_ukr_shipping_np_warehouses` WHERE `city_ref` = '%s' ORDER BY `description`", $city_ref));
    echo json_encode(array(
        "res" => 0,
        "houses" => $houses
    ));
    exit;
}

function liq_pay() {
    $amount = isset($_POST["amount"]) ? $_POST["amount"] : null;
    require_once __DIR__ . "/classes/LiqPay.php";
    $settings = get_option('woocommerce_liqpay-webplus_settings');
    $liqp = array(
        'action' => 'pay',
        'amount' => $amount,
        'currency' => 'UAH',
        'description' => 'Оплата товаров',
        'order_id' => 'order_123',
        'language' => 'ru',
        'version' => '3',
        'server_url' => "http://" . $_SERVER['HTTP_HOST'] . "/checkout",
        'result_url' => 'http://vagonka.onmywaystudio.net/checkout/?action=saveLiqPay',
        'type' => 'buy'
    );
    if(is_null($amount)) {
        echo json_encode(array(
            "res" => -1,
            "link" => ""
        ));
        exit;
    }
    $liqPay = new LiqPay($settings["public_key"], $settings["private_key"]);
    $link = $liqPay->cnb_link($liqp);
    echo json_encode(array(
        "res" => 0,
        "link" => $link
    ));
    exit;
}

function promo_code() {
    global $wpdb;
    $promo_code = isset($_POST["promo_code"]) ? $_POST["promo_code"] : "";
    $sql = sprintf("SELECT p.`ID`, 
                               p.`post_title`   AS coupon_code, 
                               p.`post_excerpt` AS coupon_description, 
                               Max(CASE WHEN pm.meta_key = 'discount_type'      AND  p.`ID` = pm.`post_id` THEN pm.`meta_value` END) AS discount_type, 
                               Max(CASE WHEN pm.meta_key = 'coupon_amount'      AND  p.`ID` = pm.`post_id` THEN pm.`meta_value` END) AS coupon_amount
                        FROM   `wp_posts` AS p 
                               INNER JOIN `wp_postmeta` AS pm ON  p.`ID` = pm.`post_id` 
                        WHERE  p.`post_type` = 'shop_coupon' 
                               AND p.`post_status` = 'publish'
                               AND p.`post_title` = '%s'
                        GROUP  BY p.`ID` 
                        ORDER  BY p.`ID` ASC; ", $promo_code);
    $result = $wpdb->get_results($sql);
    if(count($result) === 0) {
        echo json_encode(array(
            "res" => -1,
            "promo" => null
        ));
        exit;
    }
    echo json_encode(array(
        "res" => 0,
        "promo" => $result[0]
    ));
    exit;
}

function get_account_name(int $t = 0) {
    $types = get_field("options_header", "option")[0]["account_types"];
    foreach ($types as $type) {
        if($t == $type["account_type"]["type"]) return $type["account_type"]["text"];
    }
}

function get_page_by_slug($slug) {
    $posts = get_posts(array("name" => $slug, "post_type" => "page", "post_status" => "publish"));
    $lang = wpml_get_current_language();
    foreach($posts as $post) {
        $lang_detail = apply_filters("wpml_post_language_details", null, $post->ID);
        if($lang_detail["language_code"] == $lang) return $post;
    }
    return null;
}

function acf_clear_stores() { acf_reset_local(); }
add_action( 'acf/input/admin_head', 'acf_clear_stores' );


function prefix_redirect_function() {
    if ( is_page(552,550,827,1438,826,570,569,1663,554) && !is_user_logged_in() ) { // 42 это ID
        // редирект на главную:
        wp_redirect( home_url() );
        // или так на url:
        // wp_redirect( 'http://www.example.com', 301 );
        exit;
    }
}
add_action( 'template_redirect', 'prefix_redirect_function', 9 );

// On functions.php of your child theme or in a code snippet:
add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
    if ( strstr( $_SERVER['HTTP_REFERER'], 'orders' ) or strstr( $_SERVER['HTTP_REFERER'], 'profile' ) ) {
        wp_redirect( home_url() );
    } else {
        wp_redirect( $_SERVER['HTTP_REFERER'] );
    }
    exit();
}

function new_excerpt_more($more) {
    global $post;
    return '...';
 }
add_filter('excerpt_more', 'new_excerpt_more');



function custom_price_by_account_type($price, $product) {

    $current_user = wp_get_current_user();
        
    if ( $current_user->data->user_type == '1' ) {
        if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
            $price = $price - ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
        }
    }
    return round($price,2);
}

add_filter('woocommerce_variation_prices_price', 'custom_variable_price', 99, 3 );
add_filter('woocommerce_variation_prices_regular_price', 'custom_variable_price', 99, 3 );
function custom_variable_price( $price, $variation, $product ) {
$price = custom_price_by_account_type( $price, $product );
  return $price;
}

add_filter('woocommerce_product_get_price', 'custom_price_role', 99, 2 );
add_filter('woocommerce_product_get_regular_price', 'custom_price_role', 99, 2 );
// add_filter('woocommerce_product_variation_get_regular_price', 'custom_price_role_reg', 99, 2 );
add_filter('woocommerce_product_variation_get_price', 'custom_price_role', 99, 2 );
function custom_price_role( $price, $product ) {
 $price = custom_price_by_account_type( $price, $product );
 return $price;
}

function zpd_add_wc_currency( $currencies ) {
    $currencies['UAH'] = __( 'GRN', 'woocommerce' );

    return $currencies;
}
add_filter( 'woocommerce_currencies', 'zpd_add_wc_currency' );


function zpd_add_wc_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'GRN': $currency_symbol = 'грн.';
        break;
    }

    return $currency_symbol;
}
add_filter('woocommerce_currency_symbol', 'zpd_add_wc_currency_symbol', 10, 2);

add_filter( 'woocommerce_structured_data_order', 'wp_kama_woocommerce_structured_data_order_filter', 10, 3 );


add_action( 'wp_ajax_nopriv_set_new_order_status', 'set_new_order_status' );
add_action( 'wp_ajax_set_new_order_status', 'set_new_order_status' );

function set_new_order_status() 
{

    $settings = get_option('woocommerce_liqpay-webplus_settings');
    $signature = $_POST['signature'];
    $data = $_POST['data'];

    $sign = base64_encode( sha1( 
    $settings["private_key"] .  
    $data . 
    $settings["private_key"] 
    , 1 ));

    if ( $sign == $signature ) {
        $order_id = json_decode( base64_decode($data), true )['order_id'];
        $order = wc_get_order($order_id);
        $order->update_status('processing');
    }
}


add_filter( 'woocommerce_payment_complete_order_status', 'snippetpress_payment_complete_status_payment_method', 10, 3 );
function snippetpress_payment_complete_status_payment_method( $status, $order_id, $order  ){
    $payment_method = $order->get_payment_method();
    if( $payment_method == 'cheque' ){
        $status = 'not pay';
    }     
    return $status;     
}

add_filter('logout_url', function($logout_url, $redirect) {
    $logout_url = str_replace('/ru','', $logout_url); 

    return $logout_url;
}, 10, 2);


add_filter( 'wp_redirect', 'wp_kama_redirect_filter', 10, 2 );

function wp_kama_redirect_filter( $location, $status ){
    if ( is_admin() and ( strstr( $location, '/ru/wp-login.php' ) )  ) {
        $location = str_replace('/ru','',$location);
    }
    return $location;
}


function translate_order_status($status) {

        switch ($status) {
            case 'pending':
                return get_field('orders_status','option')['pending'];
            case 'processing':
                return get_field('orders_status','option')['processing'];
            case 'on-hold':
                return get_field('orders_status','option')['on-hold'];
            case 'completed':
                return get_field('orders_status','option')['completed'];
            case 'cancelled':
                return get_field('orders_status','option')['cancelled'];
            case 'refunded':
                return get_field('orders_status','option')['refunded'];
            case 'failed':
                return get_field('orders_status','option')['failed'];
            default:
                return $status;
        }

}

add_filter( 'wp_mail_from_name', function ( $original_email_from ) {
    if ( wpml_get_current_language() == 'uk' ):
        return 'Дніпровська вагонка';
    else : 
        return 'Днепровская вагонка';
    endif; 
} );