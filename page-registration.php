<?php

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once __DIR__ . "/classes/vg_user.php";
    $customer_name = isset($_POST["customer-name"]) ? $_POST["customer-name"] : "";
    $customer_fname = isset($_POST["customer-fname"]) ? $_POST["customer-fname"] : "";
    $customer_phone = isset($_POST["customer-phone"]) ? $_POST["customer-phone"] : "";
    $customer_email = isset($_POST["customer-email"]) ? $_POST["customer-email"] : "";
    $is_commercial = isset($_POST["is_commercial"]) ? $_POST["is_commercial"] : "";

    $user_type = 0;
    $company_name = isset($_POST["company-name"]) ? $_POST["company-name"] : "";
    $company_code = isset($_POST["company-code"]) ? $_POST["company-code"] : "";
    $representative = isset($_POST["representative"]) ? $_POST["representative"] : "";

    $vg_user = new VG_User();

    $res = $vg_user->add_user($customer_fname, $customer_name, $customer_email, $customer_phone, ($is_commercial == "on" ? 1 : 0), $company_name, $company_code, $representative, 0);
    if($res["user_id"] != -1) {
        $user_id = $res["user_id"];
    }
}

if(isset($_GET["secure_key"]) && $_GET["secure_key"] !== "") {
    if(!class_exists("VG_User")) {
        require_once __DIR__ . "/classes/vg_user.php";
    }

    $vg_user = new VG_User();
    $activation = $vg_user->activate_account($_GET["secure_key"]);
    if($activation["result"] == 0) {
        $creds = array(
            'user_login'    => $activation["email"],
            'user_password' => $activation["pass"],
            'remember'      => true
        );

        $current_user = wp_signon( $creds, false );
        $user_id = $current_user->ID;
    }
}

?>

<?php get_header(); ?>
    <div class="container">
        <section class="section__outer">
            <section class="section__inner">
                <div class="signup__wrapper">
                    <div class="signup__title">
                        <h1><?php echo get_field('registration','option')['text_registration']; ?></h1>
                    </div>

                    <!-- Error create user -->
                    <?php if(isset($res) && $res["user_id"] == -1) { ?>
                    <span class="registration-error"><?php echo $res["message"]; ?></span>
                    <?php } ?>

                    <!-- Error activation account -->
                    <?php if(isset($activation) && $activation["result"] == -1) { ?>
                        <span class="registration-error"><?php echo $activation["message"]; ?></span>
                    <?php } ?>

                    <!-- Success activation -->
                    <?php if(isset($activation) && $activation["result"] == 0) { ?>
                        <span class="registration-success"><?php echo $activation["message"]; ?></span>
                    <?php } ?>

                    <!-- Success registration -->
                    <?php if(isset($user_id)) { ?>
                        <span class="registration-success"><?php echo get_field('registration','option')['tekst_polzovatel_uspeshno_zaregistrirovan']; ?></span>
                    <?php } ?>

                    <div class="signup__form">
                        <?php if(!isset($user_id) && !isset($activation)) { ?>
                        <form class="form" method="post" onsubmit="return registration.checkForm();">
                            <div class="form__section section-personal">
                                <h2><?php echo get_field('registration','option')['tekst_lichnye_dannye']; ?></h2>
                                <div class="form__input input-required">
                                    <input id="name" name="customer-name" value="<?php if(isset($customer_name)) echo $customer_name; ?>" type="text" placeholder="<?php echo get_field('registration','option')['test_imya']; ?>">
                                </div>
                                <div class="form__input input-required">
                                    <input id="fname" name="customer-fname" value="<?php if(isset($customer_fname)) echo $customer_fname; ?>" type="text" placeholder="<?php echo get_field('registration','option')['tekst_familiya']; ?>">
                                </div>
                                <div class="form__input input-required">
                                    <input id="email" name="customer-email" value="<?php if(isset($customer_email)) echo $customer_email; ?>" type="email" placeholder="<?php echo get_field('registration','option')['tekst_elektronnaya_pochta']; ?>" data-mail-input>
                                </div>
                                <div class="form__input input-required">
                                    <input id="phone" name="customer-phone" value="<?php if(isset($customer_phone)) echo $customer_phone; ?>" type="tel" placeholder="<?php echo get_field('registration','option')['tekst_telefon']; ?>" data-tel-input>
                                </div>
                            </div>
                            <div class="form__section section-company">
                                <h2><?php echo get_field('registration','option')['tekst_yuridicheskoe_liczo']; ?></h2>
                                <div class="form__checkbox">
                                    <label for="iscommercial">
                                        <input type="checkbox" <?php if(isset($company_code) && $company_code !== "") echo "checked"; ?> id="iscommercial">
                                        <div class="form__checkbox-custom"></div>
                                        <span><?php echo get_field('registration','option')['tekst_akkaunt_ispolzuetsya_yurliczom_organizacziej_ili_fop']; ?></span>
                                    </label>
                                </div>
                                <div class="form__input">
                                    <input id="companyname" name="company-name" value="<?php if(isset($company_name)) echo $company_name; ?>" <?php if(!isset($company_name) || $company_code == "") echo 'style="display:none;"'; ?> type="text" placeholder="<?php echo get_field('registration','option')['tekst_nazvanie']; ?>">
                                </div>
                                <div class="form__input">
                                    <input id="companycode" name="company-code" value="<?php if(isset($company_code)) echo $company_code; ?>" <?php if(!isset($company_name) || $company_code == "") echo 'style="display:none;"'; ?> type="text" placeholder="<?php echo get_field('registration','option')['tekst_kod_egrpouinn']; ?>" class="numeric">
                                </div>
                                <div class="form__input">
                                    <input id="representative" name="representative" value="<?php if(isset($representative)) echo $representative; ?>" <?php if(!isset($company_name) || $company_code == "") echo 'style="display:none;"'; ?> type="text" placeholder="<?php echo get_field('registration','option')['tekst_fio_otvetstvennogo_licza']; ?>">
                                </div>
                            </div>
                            <div class="form__submit">
                                <button type="submit" class="btn btn-colored"><?php echo get_field('registration','option')['tekst_zaregistrirovatsya']; ?></button>
                            </div>
                        </form>
                        <?php } elseif(!isset($activation)) { ?>
                            <span><?php echo get_field('registration','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></span>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </section>
    </div>
<?php get_footer(); ?>