<?php
class VG_User {

    public $id;

    public $fname;

    public $name;

    public $email;

    public $phone;

    public $status;

    public $is_commercial;

    public $company_name;

    public $company_code;

    public $representative;

    public $user_type;

    public $pass;

    public $secure_key;


    function __construct()
    {
        $this->init();
    }

    public function init() {
        if ( is_user_logged_in() ) : 
            $data = $this->get_current_user();

            if(is_null($data)) return $this;

            $meta = $this->get_user_meta($data["ID"]);

            $this->id             = $data["ID"];

            $this->fname          = $meta["first_name"];
            $this->name           = $meta["last_name"];

            $this->email          = $data["user_email"];
            $this->phone          = $data["phone"];
            $this->status         = $data["user_status"];
            $this->is_commercial  = $data["is_commercial"];
            $this->company_name   = $data["company_name"];
            $this->company_code   = $data["company_code"];
            $this->representative = $data["representative"];
            $this->user_type      = $data["user_type"];
            $this->pass           = $data["user_pass"];
            $this->secure_key     = $data["secure_key"];
        endif; 
    }

    public function is_auth() {
        return (int)$this->id > 0;
    }

    public function get_user_meta($user_id) {
        global $wpdb;

        $f_sql = sprintf("SELECT `meta_value` FROM `wp_usermeta` WHERE `meta_key`='first_name' AND `user_id` = %s", $user_id);
        $l_sql = sprintf("SELECT `meta_value` FROM `wp_usermeta` WHERE `meta_key`='last_name' AND `user_id` = %s", $user_id);

        $f_res = $wpdb->get_results($f_sql)[0];
        $l_res = $wpdb->get_results($l_sql)[0];

        return [
            "first_name" => $f_res->meta_value,
            "last_name"  => $l_res->meta_value,
        ];
    }

    public function add_user($fname, $name, $email, $phone, $is_commercial, $company_name, $company_code, $representative, $user_type = 0, $status = -1) {
        $this->fname = $fname;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;
        $this->status = $status;
        $this->is_commercial = $is_commercial;
        $this->company_name = $company_name;
        $this->company_code = $company_code;
        $this->representative = $representative;
        $this->user_type = $user_type;
        $this->pass = $this->generate_password();
        $this->secure_key = $this->generate_password(18);

        $res = wp_create_user($this->email, $this->pass, $this->email);
        if(is_object($res)) {
            $error = $this->get_error($res);
            return array(
                "user_id" => -1,
                "message" => $error
            );
        }

        $this->id = $res;
        $this->set_additional();
        $this->send_registration_email();
        //$this->init();
        return array(
            "user_id" => $this->id,
            "login" => $this->email,
            "pass" => $this->pass,
            "message" => "User create success"
        );
    }

    public function update_user($id, $fname, $name, $email, $phone) {
        global $wpdb;
        $this->id = $id;
        $this->fname = $fname;
        $this->name = $name;
        $this->email = $email;
        $this->phone = $phone;

        $query = sprintf("UPDATE `wp_users` SET `user_nicename` = '%s', `display_name` = '%s', `phone` = '%s' WHERE `ID` = %s ",
            $this->fname . " " . $this->name,
            $this->fname . " " . $this->name,
            $this->phone,
            $this->id);

        $wpdb->query($query);

        $query = sprintf("UPDATE `wp_usermeta` SET `meta_value`='%s' WHERE `user_id` = %s AND `meta_key` = 'first_name'", $this->name, $this->id);
        $wpdb->query($query);
        $query = sprintf("UPDATE `wp_usermeta` SET `meta_value`='%s' WHERE `user_id` = %s AND `meta_key` = 'last_name'", $this->fname, $this->id);
        $wpdb->query($query);
        $this->init();
        return array(
            "res" => 0
        );
    }

    public function update_company($id, $is_commercial, $company_name, $company_code, $representative) {
        global $wpdb;
        $this->id = $id;
        $this->is_commercial = $is_commercial;
        $this->company_name = $company_name;
        $this->company_code = $company_code;
        $this->representative = $representative;

        $query = sprintf("UPDATE `wp_users` SET `is_commercial` = %s, `company_name` = '%s', `company_code` = '%s', `representative` = '%s' WHERE `ID` = %s",
            $this->is_commercial,
            $this->company_name,
            $this->company_code,
            $this->representative,
            $this->id);

        $wpdb->query($query);
        $this->init();
        return array(
            "res" => 0
        );
    }

    public function change_password($user_id, $login, $old_password, $new_password) {
        global $wpdb;
        $user_data = $wpdb->get_results(sprintf("SELECT * FROM `wp_users` WHERE `ID`=%s", $user_id));
        $u = wp_login($login, $old_password);

        if($u === false) {
            return array(
                "res" => -1,
                "message" => get_field('header','option')['tekst_nevernyj_email_ili_parol']
            );
        }

        $new_pass = wp_hash_password($new_password);
        $wpdb->query(sprintf("UPDATE `wp_users` SET `user_pass`='%s' WHERE `ID`=%s", $new_pass, $user_id));
        return array(
            "res" => 0,
            "message" => get_field('header','option')['tekst_parol_uspeshno_izmenen']
        );
    }

    public function activate_account($key) {
        global $wpdb;

        $key_data = base64_decode($key);
        $key_data = json_decode($key_data);
        $secure_key = $key_data->secure_key;
        $email = $key_data->email;
        $pass = $key_data->pass;

        $res = $wpdb->get_results(sprintf("SELECT * FROM `wp_users` WHERE `secure_key` = '%s'", $secure_key));
        if(is_null($res) || count($res) == 0) {
            return array(
                "result" => -1,
                "message" => get_field('header','option')['tekst_nevernyj_email_ili_parol']
            );
        }

        $wpdb->query(sprintf("UPDATE `wp_users` SET `user_status` = 0 WHERE `ID` = %s", $res[0]->ID));
        return array(
            "result" => 0,
            "user_id" => $res[0]->ID,
            "email" => $email,
            "pass" => $pass,
            "message" => get_field('header','option')['tekst_vash_akkaunt_uspeshno_aktivirovan_rekomenduem_smenit_parol_v_lichnom_kabinete']
        );
    }

    public function get_user($email) {
        global $wpdb;
        $sql = sprintf("SELECT * FROM `wp_users` WHERE `user_login` = '%s'", $email);
        $user = $wpdb->get_results($sql);
        return isset($user[0]) ? $user[0] : null;
    }

    public function repair($email) {
        global $wpdb;
        $password = $this->generate_password();
        $pass = wp_hash_password($password);
        $sql = sprintf("SELECT * FROM `wp_users` WHERE `user_login` = '%s'", $email);
        $user = $wpdb->get_results($sql);
        if(count($user) == 0) {
            return array(
                "res" => -1,
                "message" => get_field('header','option')['tekst_polzovatel_ne_najden']
            );
        }

        $sql = sprintf("UPDATE `wp_users` SET `user_pass` = '%s' WHERE `user_login` = '%s'", $pass, $email);
        $wpdb->query($sql);

        $this->send_repair_email($email, $password);

        return array(
            "res" => 0,
            "message" => get_field('header','option')['tekst_parol_uspeshno_izmenen_novyj_parol_vyslan_pismom_na_vashu_elektronnuyu_pochtu']
        );
    }

    public function get_current_user() {
        global $wpdb;
        $id = get_current_user_id();
        $sql = sprintf("SELECT * FROM `wp_users` WHERE `ID` = %s", $id);
        $user = $wpdb->get_results($sql, 'ARRAY_A');
        $user = isset($user[0]) ? $user[0] : null;
        $display_name = $user["display_name"];
        if($display_name !== "") {
            $display_name = explode(" ", $display_name);
            $user["customer-fname"] = isset($display_name[0]) ? $display_name[0] : "";
            $user["customer-name"] = isset($display_name[1]) ? $display_name[1] : "";
        }
        return $user;
    }

    public function post_read($post_id) {
        global $wpdb;
        $user = $this->get_current_user();
        if(!$this->is_post_read($post_id)) {
            $sql = sprintf("INSERT INTO `wp_posts_reading` (`user_id`, `post_id`, `date`) VALUES (%s, %s, NOW())", $user["ID"], $post_id);
            $wpdb->query($sql);
        }
    }

    public function get_count_new_materials() {
        global $wpdb;
        $user = $this->get_current_user();
        if(!isset($user["ID"])) return 0;
        $sql = sprintf("SELECT ID FROM `wp_posts` WHERE `post_type`='b2b_materials'
            AND `post_status` = 'publish'
            AND ID NOT IN (SELECT `post_id` FROM `wp_posts_reading` WHERE `user_id` = %s);", $user["ID"]);
        $rows = $wpdb->get_results($sql);
        $new_posts = [];

        foreach ( $rows as $post ) {
            $my_post_language_details = apply_filters( 'wpml_post_language_details', NULL, $post->ID );
            if ( $my_post_language_details['language_code'] ==  wpml_get_current_language() ) {
                $new_posts[] = $post->ID;
            } 
        }
       
        return (int)count( $new_posts );
    }

    private function is_post_read($post_id) {
        global $wpdb;
        $user = $this->get_current_user();
        $sql = sprintf("SELECT count(`ID`) as `reading` FROM `wp_posts_reading` WHERE `user_id` = %s AND `post_id` = %s", $user["ID"], $post_id);
        $rows = $wpdb->get_results($sql);
        return (int)$rows[0]->reading > 0;
    }

    private function generate_password($length = 8) {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < $length; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    private function send_registration_email() {
        if ( wpml_get_current_language() == 'uk' ):
            $subject = "Реєстрація на сайті";
        else :
            $subject = "Регистрация на сайте";
        endif; 

        $key_data = json_encode(array(
            "email" => $this->email,
            "pass" => $this->pass,
            "secure_key" => $this->secure_key
        ));

        $key64 = base64_encode($key_data);

        if ( wpml_get_current_language() == 'uk' ):
            $body = "Ви успішно зареєструвалися на сайті<br>" .
            get_site_url() . '<br><br>Ваш логін та пароль: <br> Логін: ' .
            $this->email . '<br> Пароль: ' . $this->pass . '<br><br>Щоб активувати Ваш обліковий запис, перейдіть за посиланням:<br>' .
            get_site_url() . '/registration/?secure_key=' . $key64;
        else :
            $body = "Вы успешно зарегистрировались на сайте<br>" .
            get_site_url() . '<br><br>Ваш логин и пароль: <br> Логин: ' .
            $this->email . '<br> Пароль: ' . $this->pass . '<br><br>Для активации Вашего аккаунта перейдите по ссылке:<br>' .
            get_site_url() . '/registration/?secure_key=' . $key64;
        endif; 
       
        wp_mail($this->email, $subject, $body,"Content-Type: text/html; charset=UTF-8");
    }

    private function send_repair_email($email, $pass) {
        $subject = "Восстановление пароля на сайте " . get_site_url();

        $body = "Ваш пароль успешно изменен " .
            get_site_url() . '<br> Ваш логин и пароль: <br> Логин: ' .
            $email . '<br> Пароль: ' . $pass;
        wp_mail($email, $subject, $body,"Content-Type: text/html; charset=UTF-8");
    }

    private function get_error(WP_Error $error) {
        return $error->get_error_message();
    }

    private function set_additional() {
        global $wpdb;

        $query = sprintf("UPDATE `wp_users` SET `user_nicename` = '%s', `display_name` = '%s', `phone` = '%s', `secure_key` = '%s', `user_status` = %s , `is_commercial` = %s , `company_name` = '%s', `company_code` = '%s', `representative` = '%s', `user_type` = %s WHERE `ID` = %s ",
            $this->fname . " " . $this->name,
                    $this->fname . " " . $this->name,
                    $this->phone,
                    $this->secure_key,
                    $this->status,
                    $this->is_commercial,
                    $this->company_name,
                    $this->company_code,
                    $this->representative,
                    0,
                    $this->id);

        $wpdb->query($query);

        $query = sprintf("UPDATE `wp_usermeta` SET `meta_value`='%s' WHERE `user_id` = %s AND `meta_key` = 'first_name'", $this->name, $this->id);
        $wpdb->query($query);
        $query = sprintf("UPDATE `wp_usermeta` SET `meta_value`='%s' WHERE `user_id` = %s AND `meta_key` = 'last_name'", $this->fname, $this->id);
        $wpdb->query($query);
    }

}