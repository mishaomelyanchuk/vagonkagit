<?php
class VG_Order {

    public function create($data) {
        $cart = WC()->cart;

        $checkout = WC()->checkout();

        $order_id = $checkout->create_order(array());
        $order = wc_get_order($order_id);
        $order->set_billing_address_1( wp_get_current_user()->phone );
        update_post_meta($order_id, '_customer_user', get_current_user_id());
        $shipping_rate = new WC_Shipping_Rate(1);;
        if($data["delivery_method"] === "courier") {
            update_post_meta($order_id, 'delivery_method', 'courier');
            $shipping_rate = new WC_Shipping_Rate(6);
            $shipping_rate->set_cost($data["delivery_cost"]);
        } elseif($data["delivery_method"] === "novaposhta") {
            update_post_meta($order_id, 'delivery_method', 'novaposhta');
            $shipping_rate = new WC_Shipping_Rate(7);
        } elseif($data["delivery_method"] === "self") {
            update_post_meta($order_id, 'delivery_method', 'self');
        }

        $payment_gateways = WC()->payment_gateways->payment_gateways();

        if ( $data["payment_method"] == 'liqpay' ) {
            $order->set_payment_method($payment_gateways['liqpay-webplus']);
        }

        if ( $data["payment_method"] == 'on_receive' ) {
            $order->set_payment_method($payment_gateways['cod']);
        }

        if ( $data["payment_method"] == 'invoice' ) {
            $order->set_payment_method($payment_gateways['cheque']);
        }  

        $order->set_shipping_address_1($data["delivery_address"]);
        $order->add_shipping($shipping_rate);

        $order->calculate_totals();
        $final_discount = 0;
        if(!is_null($data["promo_code"]) && $data["promo_code"] !== "") {
            $order_total = $order->get_total();
            $coupon = new WC_Coupon( $data["promo_code"] );
            $coupon_type = $coupon->get_discount_type();
            $coupon_amount = $coupon->get_amount();
            if($coupon_type === "fixed_cart") {
                $order_total = $order_total - $coupon_amount;
                $final_discount = $coupon_amount;
            } else {
                $final_discount = $coupon_amount * ( $order_total / 100 );
            }
            //$order->add_coupon($data["promo_code"], $final_discount, 0);
            $order->apply_coupon($coupon);
            //$order->set_total($order_total - $final_discount);
        }
        $order->calculate_totals();
        //$order->payment_complete();
     

        if ( $data["payment_method"] != 'liqpay' ) {
            $order->update_status('processing');
        }
        $cart->empty_cart();

        return array(
            "order_id" => $order_id
        );
    }


}