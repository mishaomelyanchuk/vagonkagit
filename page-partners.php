<?php
/*
 * Template name: Partners
 */

?>

<?php
$contacts_section = get_field( 'contacts_section' );
$from = isset($_POST["f_email"]) ? $_POST["f_email"] : null;
$to = !empty($contacts_section['email_real']) ? $contacts_section['email_real'] : get_option('admin_email');
$name = isset($_POST["f_name"]) ? $_POST["f_name"] : null;
$offer = isset($_POST["f_offer"]) ? $_POST["f_offer"] : null;

if(!is_null($from) &&
    !is_null($to) &&
    !is_null($name) &&
    !is_null($from) &&
    !is_null($offer)) {
    wp_mail($to, "Обратная связь с сайта Вагонка страница Партнерам", "Имя:" . $name . "<br>Email: " . $from . "<br>Предложение: " . $offer,"Content-Type: text/html; charset=UTF-8");

    $rec_id = wp_insert_post(array(
        'post_title' => $name . ' - ' . $from,
        'post_type' => 'partners_request',
        'post_content' => $offer,
        'post_status' => 'publish',
        'post_author' => get_current_user_id(),
    ));

    update_field('contact_name', $name, $rec_id);
    update_field('contact_email', $from, $rec_id);
    update_field('contact_offer', $offer, $rec_id);

    $sending = true;
}

?>


<?php get_header(); ?>
    <?php $first_section = new Page_Partners_First_Section(); ?>
    <?php $features_section = new Page_Partners_Features_Section(); ?>
    <?php $contact_form = new Page_Partners_Contacts_Section(); ?>

    <?php if(isset($sending)) { ?>
       <?php  $contact_form->is_sending = true; ?>
    <?php } ?>

    <?php $first_section->render(); ?>
    <?php $features_section->render(); ?>
    <?php $contact_form->render(); ?>

<?php get_footer(); ?>
