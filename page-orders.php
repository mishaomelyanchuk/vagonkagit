<?php
/*
 * Template name: Orders
 * */


require_once __DIR__ . "/classes/vg_user.php";
$vg_user = new VG_User();
$user = $vg_user->get_current_user();
$new_posts_count = $vg_user->get_count_new_materials();

$lang = wpml_get_current_language();

// Get all customerorders
$customer = wp_get_current_user();
$customer_orders = get_posts(array(
    'numberposts' => -1,
    'meta_key' => '_customer_user',
    'orderby' => 'date',
    'order' => 'DESC',
    'meta_value' => get_current_user_id(),
    'post_type' => wc_get_order_types(),
    'post_status' => array_keys(wc_get_order_statuses()),
));

$orders = [];
foreach ($customer_orders as $customer_order) {
    $order = wc_get_order($customer_order);
    $orders[] = [
        "ID" => $order->get_id(),
        "customer_id" => $order->get_customer_id(),
        "total" => $order->get_total(),
        "date" => $order->get_date_created()->date_i18n('d F Y'),
        "status" => $order->get_status(),
        "shipping" => $order->get_shipping_method(),
        "payment" => $order->get_payment_method(),
        "items" => $order->get_items(),
        "address" => $order->get_shipping_address_1(),
        'status' => $order->get_status()

    ];
}

function get_order_product_attribute(WC_Order_Item_Product $product, $attr_name) {
    $slug = $product->get_meta("pa_" . $attr_name);
    return get_term_by('slug', $slug, "pa_" . $attr_name);
}

?>

<?php get_header(); ?>
<div class="account">
    <div class="container">
        <section class="secton__outer">
            <section class="section__inner">
                <div class="account__wrapper">
                    <div class="account__header">
                        <div class="account__type">
                            <?php if($user["user_type"] == 0) { ?>
                                <p class="standart_account"><?php echo get_account_name(); ?></p>
                            <?php } else { ?>
                                <p class="b2b_account"><?php echo get_account_name(1); ?></p>
                            <?php } ?>
                        </div>
                        <div class="account__title">
                            <h1><?php echo get_post()->post_title; ?></h1>
                        </div>
                        <div class="account__pages">
                            <ul>
                                <li><a href="<?php echo ($lang == 'ru' ? '/ru' : ''); ?>/profile"><?php echo get_page_by_slug("profile")->post_title; ?></a></li>
                                <?php if((int)$user["user_type"] !== 0) { ?>
                                    <li>
                                        <a href="<?php echo ($lang == 'ru' ? '/ru' : ''); ?>/materials"><?php echo get_page_by_slug("materials")->post_title; ?></a>
                                        <?php if($new_posts_count > 0) { ?>
                                            <div class="account__count">
                                                <span><?php echo $new_posts_count; ?></span>
                                            </div>
                                        <?php } ?>
                                    </li>
                                <?php } ?>
                            </ul>
                            <div class="account__pages-toggle">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/svg-arrow-select.svg" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="account__orders">
                        <?php if(count($orders) == 0) { ?>
                            <p><?php echo get_field('orders','option')['tekst_u_vas_poka_net_zakazov']; ?></p>
                        <?php } else { ?>
                        <?php foreach($orders as $order) { ?>
                            <?php
                                $status = translate_order_status($order["status"]);
                                $status_class = "status--processing";

                                switch (strtolower($order["status"])) {
                                    case "pending":
                                      $status_class = "status--pending";
                                      break;
                                    case "processing":
                                      $status_class = "status--processing";
                                      break;
                                    case "on-hold":
                                      $status_class = "status--hold";
                                      break;
                                    case "completed":
                                      $status_class = "status--done";
                                      break;
                                    case "cancelled":
                                      $status_class = "status--canceled";
                                      break;
                                    case "refunded":
                                      $status_class = "status--refunded";
                                      break;
                                    case "failed":
                                      $status_class = "status--failed";
                                      break;
                                    default:
                                        $status_class = "status--processing";
                                        break;
                                }

                                
                                
                                
                                if ( wpml_get_current_language() == 'uk' ):
                                    $count_text = "позиція";
                                if(count($order["items"]) == 1) {
                                    $count_text = "позиція";
                                } elseif(count($order["items"]) > 1 && count($order["items"]) < 5) {
                                    $count_text = "позиції";
                                } else {
                                    $count_text = "позицій";
                                }
                                else :
                                    $count_text = "позиция";
                                    if(count($order["items"]) == 1) {
                                        $count_text = "позиция";
                                    } elseif(count($order["items"]) > 1 && count($order["items"]) < 5) {
                                        $count_text = "позиции";
                                    } else {
                                        $count_text = "позиций";
                                    }
                                endif; 
                                switch ($order["payment"]) {
                                    case "cod":
                                        if ( wpml_get_current_language() == 'uk' ):
                                            $payment = "Оплата при доставці";
                                        else :
                                            $payment = "Оплата при доставке";
                                        endif; 
                                        break;
                                    case "liqpay-webplus":
                                        if ( wpml_get_current_language() == 'uk' ):
                                            $payment = "Оплата онлайн (LiqPay)";
                                        else :
                                            $payment = "Оплата онлайн (LiqPay)";
                                        endif; 
                                        break;
                                    default:
                                        if ( wpml_get_current_language() == 'uk' ):
                                            $payment = "Оплата рахунком";
                                        else :
                                            $payment = "Оплата счетом";
                                        endif; 
                                        break;
                                }
                            ?>
                            <div class="account__order <?php echo $status_class; ?>">
                            <div class="account__order-title">
                                <div class="account__order-name">
                                    <h4><?php echo get_field('orders','option')['tekst_zakaz']; ?> №<?php echo $order["ID"]; ?></h4>
                                </div>
                                <div class="account__order-date">
                                    <?php
                                    if ( wpml_get_current_language() == 'uk' ):
                                        $months = array(
                                            'January' => 'січня',
                                            'February' => 'лютого',
                                            'March' => 'березня',
                                            'April' => 'квітня',
                                            'May' => 'травня',
                                            'June' => 'червня',
                                            'July' => 'липня',
                                            'August' => 'серпня',
                                            'September' => 'вересня',
                                            'October' => 'жовтня',
                                            'November' => 'листопада',
                                            'December' => 'грудня'
                                        );
                                    else : 
                                        $months = array(
                                            'January' => 'января',
                                            'February' => 'февраля',
                                            'March' => 'марта',
                                            'April' => 'апреля',
                                            'May' => 'мая',
                                            'June' => 'июня',
                                            'July' => 'июля',
                                            'August' => 'августа',
                                            'September' => 'сентября',
                                            'October' => 'октября',
                                            'November' => 'ноября',
                                            'December' => 'декабря'
                                        );
                                    endif; 
                                    
                                    $orderDate = $order["date"];
                                    $orderMonth = date('F', strtotime($orderDate)); 
                                    $translatedMonth = $months[$orderMonth]; 
                                    $translatedDate = str_replace($orderMonth, $translatedMonth, $orderDate); 
                                    ?>
                                    <p><?php echo $translatedDate; ?></p>
                                </div>
                                <div class="account__order-status">
                                    <i></i>
                                    <p><?php echo $status; ?></p>
                                </div>
                            </div>
                            <div class="account__order-total">
                                <p><?php echo get_field('orders','option')['tekst_summa']; ?></p>
                                <p class="account__order-price">
                                    <?php $price = explode( '.', $order["total"] )?>
                                    <?php echo $price[0]; ?><span>.<?php echo $price[1]; ?></span><span> </span><span class="currency">₴</span>
                                </p>
                            </div>
                            <div class="account__order-short">
                                <div class="account__order-count">
                                    <p><?php echo count($order["items"]); ?><span> <?php echo $count_text; ?></span></p>
                                </div>
                                <div class="account__order-info">
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'self' ) : ?>
                                            <h5>Самовивіз</h5>
                                            <?php if ( !empty( $order["address"] ) ) : ?>
                                                <?php $address = str_replace( 'Самовывоз', 'Самовивіз', $order["address"] ); ?>
                                            <?php endif;  ?>
                                        <?php endif; ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'courier' ) : ?>
                                            <h5>Доставка кур`єром</h5>
                                            <?php if ( !empty( $order["address"] ) ) : ?>
                                                <?php $address = str_replace( 'Доставка курьером', 'Доставка кур`єром', $order["address"] ); ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'novaposhta' ) : ?>
                                            <h5>Нова Пошта</h5>
                                            <?php if ( !empty( $order["address"] ) ) : ?>
                                                <?php $address = str_replace( 'Новая почта', 'Нова пошта', $order["address"] ); ?>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php else : ?>
                                        <?php $address = $order["address"]; ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'self' ) : ?>
                                            <h5>Самовывоз</h5>
                                        <?php endif; ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'courier' ) : ?>
                                            <h5>Доставка курьером</h5>
                                        <?php endif; ?>
                                        <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'novaposhta' ) : ?>
                                            <h5>Новая Почта</h5>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                  <span style="display: none"><?php echo $address; ?>
                                    <!-- <a href="#"> На карте</a> -->
                                  </span>
                                  <?php if ( get_post_meta( $order["ID"], 'delivery_method' )[0] == 'courier' ) : ?>
                                    <?php $flat_rate = new WC_Shipping_Flat_Rate(6);
                                    $courier_cost = $flat_rate->instance_settings["cost"];?>
                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                      <p>Ціна доставки: <?php echo $courier_cost; ?> ₴</p>
                                    <?php else : ?>
                                      <p>Ціна доставки: <?php echo $courier_cost; ?> ₴</p>
                                    <?php endif; ?>
                                  <?php endif; ?>
                                </div>
                                <div class="account__order-pay">
                                    <p><?php echo $payment; ?></p>
                                    <?php if ( !empty( get_post_meta( $order["ID"], 'invoce_generete' )[0] ) ) : ?>
                                      <span style="display: none"><?php echo do_shortcode('[wcpdf_download_invoice link_text="'. get_field( 'checkout_settings', 'option' )['dow_link'] .'" order_id="'.$order["ID"] .'"]'); ?></span>
                                    <?php endif; ?>
                                  <?php if ( $order['status'] == 'pending' ) :
                                    require_once __DIR__ . "/classes/LiqPay.php";
                                    $settings = get_option('woocommerce_liqpay-webplus_settings');
                                    $liqp = array(
                                      'action' => 'pay',
                                      'amount' => $order['total'],
                                      'currency' => 'UAH',
                                      'description' => 'Заказ №' . $order["ID"],
                                      'order_id' => $order["ID"],
                                      'language' => 'ru',
                                      'version' => '3',
                                      'server_url' => home_url() . '/wp-admin/admin-ajax.php?action=set_new_order_status' ,
                                      'result_url' => Page_Liqpay_Success::get_url() . '?order='. base64_encode($order["ID"]) . '&new_user=' ,
                                      'type' => 'buy'
                                    );
                                    $liqPay = new LiqPay($settings["public_key"], $settings["private_key"]);
                                    $link = $liqPay->cnb_link($liqp);
                                    ?>
                                    <span style="display: none"><p><a href="<?php echo $link?>"><?php echo get_field('orders','option')['tekst_ssylka_na_povtornuyu_oplatu']; ?></a></p></span>
                                  <?php endif; ?>
                                  
                                </div>
                            </div>
                            <div class="account__order-wrapper" id="order_wrapper_<?php echo $order["ID"]; ?>">
                                
                                <!-- <p><?php echo $user["company_name"] !== "" ? $user["company_name"] :$user["display_name"];  ?></p> -->
                                
                                <ul class="account__order-products">
                                <?php $i = 1; ?>
                                <?php foreach($order["items"] as $product) { ?>
                                    <?php
                                    $product_id = $product->get_product_id();
                                    $p = $product->get_product($product_id);
                                    if ( empty( $p ) ) {
                                        continue;
                                    }
                                    $slug = $p->get_slug();

                                    $obem = get_order_product_attribute($product, "obem");
                                    $czvet = get_order_product_attribute($product, "czvet");

                                    // $color = "#fff";
                                    // switch ($czvet->term_id) {
                                    //     case 28:
                                    //         $color = "#fff";
                                    //         break;
                                    //     case 29:
                                    //         $color = "#0f0f0f";
                                    //         break;
                                    //     case 30:
                                    //         $color = "#757575";
                                    //         break;
                                    //     case 31:
                                    //         $color = "#757575";
                                    //         break;
                                    //     case 32:
                                    //         $color = "rgb(251, 213, 13)";
                                    //         break;
                                    // }

                                    ?>
                                    <li class="order__products-item">
                                        <div class="order__products-position"><span> <?php echo $i; $i++;?> </span></div>
                                        <picture>
                                            <?php $image = wp_get_attachment_image_src( get_post_thumbnail_id($product_id), 'single-post-thumbnail' );?>
                                            <img src="<?php echo isset($image[0]) && $image[0] != "" ? $image[0] : get_template_directory_uri() . "/static/svg-empty-file.svg"; ?>" alt="">
                                        </picture>
                                        <div class="order__products-title">
                                            <a target="_blank" href="<?php echo get_the_permalink($product_id); ?>"><?php echo get_the_title($product_id); ?></a>
                                        </div>
                                        <div class="order__products-info">
                                            <div class="order__products-info_size"><?php echo $obem->name; ?></div>
                                            <div class="order__products-info_color"
                                            style="background-color: <?php echo get_field( 'color', 'term_'.$czvet->term_id ); ?>"></div>
                                            <div class="order__products-info_name"><?php echo $czvet->name; ?></div>
                                        </div>
                                        <ul class="order__products-price">
                                            <li>
                                                <p><?php echo get_field('orders','option')['tekst_czena']; ?></p>
                                                <?php $p_price = explode( '.', $p->get_price() ); ?>
                                                <span>
                                                <?php echo $p_price[0] . '.';
                                                if ( empty( $p_price[1] ) ) {
                                                    echo '00';
                                                } else {
                                                    if ( strlen( $p_price[1] ) == 1 ) {
                                                        echo $p_price[1].'0';
                                                    }
                                                    if ( strlen( $p_price[1] ) == 2 or strlen( $p_price[1] ) > 2 ) {
                                                        echo $p_price[1];
                                                    }
                                                } ?><span> </span><span class="currency">₴</span></span>
                                            </li>
                                            <li>
                                                <p><?php echo get_field('orders','option')['tekst_kol-vo']; ?></p>
                                                <span><?php echo $product->get_quantity(); ?></span>
                                            </li>
                                            <li>
                                                <p><?php echo get_field('orders','option')['tekst_summa']; ?></p>
                                                <?php $subtotal = explode( '.', $product->get_subtotal() ); ?>
                                                <span>
                                                <?php echo $subtotal[0] . '.';
                                                if ( empty( $subtotal[1] ) ) {
                                                    echo '00';
                                                } else {
                                                    if ( strlen( $subtotal[1] ) == 1 ) {
                                                        echo $subtotal[1].'0';
                                                    }
                                                    if ( strlen( $subtotal[1] ) == 2 ) {
                                                        echo $subtotal[1];
                                                    }
                                                } ?>
                                                <span> </span><span class="currency">₴</span></span>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>
                                </ul>
                            </div>
                            <div class="account__order-toggle">
                                <div class="account__order-toggle_line"></div>
                             
                                <div class="account__order-show">
                                  <p><?php echo get_field('orders','option')['tekst_podrobnee']; ?></p>
                                  <p><?php echo get_field('orders','option')['tekst_svernut']; ?></p>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </section>
        </section>
    </div>
</div>
<?php get_footer(); ?>

