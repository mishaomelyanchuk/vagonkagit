<?php

if($_SERVER['REQUEST_METHOD'] === 'POST') {
    require_once __DIR__ . "/classes/vg_user.php";
    $email = isset($_POST['repair-email']) ? $_POST['repair-email'] : "";

    $vg_user = new VG_User();
    $result = $vg_user->repair($email);
}

?>

<?php get_header(); ?>
<div class="container">
    <section class="section__outer">
        <section class="section__inner">
            <div class="signup__wrapper">
                <div class="signup__title">
                    <h1><?php echo get_field('password_repair','option')['tekst_vosstanovlenie']; ?></h1>
                </div>

                <!-- Error repair password -->
                <?php if(isset($result) && $result["res"] == -1) { ?>
                    <span class="registration-error"><?php echo $result["message"]; ?></span>
                <?php } ?>

                <!-- Success repair -->
                <?php if(isset($result) && $result["res"] == 0) { ?>
                    <span class="registration-success"><?php echo $result["message"]; ?></span>
                <?php } ?>

                <?php if(!isset($result) || $result["res"] !== 0) { ?>
                <div class="signup__form">
                    <form class="form" method="post" onsubmit="return registration.checkRepairForm();">
                        <div class="form__section section-personal">
                            <h2><?php echo get_field('password_repair','option')['tekst_vvedite_svoj_email']; ?></h2>
                            <div class="form__input input-required repair_email">
                                <input id="repair-email" name="repair-email" value="<?php if(isset($repair_email)) echo $repair_email; ?>" type="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form__submit">
                            <button type="submit" class="btn btn-colored"><?php echo get_field('password_repair','option')['tekst_sbrosit_parol']; ?></button>
                        </div>
                    </form>
                </div>
                <?php } ?>
            </div>
        </section>
    </section>
</div>
<?php get_footer(); ?>