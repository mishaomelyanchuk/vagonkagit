<?php
/*
 * Template name: Contacts
 */
?>

<?php
global $post;

$form_section = get_field('form_section');
$to = !empty($form_section["email_to"]) ? $form_section["email_to"] : get_option('admin_email');

$from = isset($_POST["f_email"]) ? $_POST["f_email"] : null;
$name = isset($_POST["f_name"]) ? $_POST["f_name"] : null;
$offer = isset($_POST["f_offer"]) ? $_POST["f_offer"] : null;

if(!is_null($from) &&
    !is_null($to) &&
    !is_null($name) &&
    !is_null($offer)) {
    wp_mail($to, "Запрос с сайта Вагонка страница Контакты", "Имя:" . $name . "<br>Email: " . $from . "<br>Сообщение: " . $offer,"Content-Type: text/html; charset=UTF-8");
    $rec_id = wp_insert_post(array(
        'post_title' => $name . ' - ' . $from,
        'post_type' => 'contacts_request',
        'post_content' => $offer,
        'post_status' => 'publish',
        'post_author' => get_current_user_id(),
    ));

    update_field('contact_name', $name, $rec_id);
    update_field('contact_email', $from, $rec_id);
    update_field('contact_offer', $offer, $rec_id);

    $sending = true;
}

$page_content = new Page_Contacts_Content();


$map_setting = get_field("constacts_map_settings");

$page_content->center_lat = $map_setting["center"]["lat"];
$page_content->center_lng = $map_setting["center"]["lng"];
$page_content->zoom = $map_setting["zoom"];
$page_content->map = $map_setting["map"];
?>



<?php if(isset($sending)) { ?>
    <?php  $page_content->is_sending = true; ?>
<?php } ?>
<?php get_header(); ?>
<?php $page_content->render(); ?>
<?php ( new Blog_Section() )->render(); ?>
<?php get_footer(); ?>

