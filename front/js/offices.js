$(document).ready(function () {
  let map = new google.maps.Map(document.getElementById("map"), {
    center: new google.maps.LatLng(-33.91722, 151.23064),
    zoom: 17,
    styles: [
      {
        featureType: "administrative",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative.country",
        elementType: "geometry.stroke",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "administrative.province",
        elementType: "geometry.stroke",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [
          {
            visibility: "on",
          },
          {
            color: "#F5F5F5",
          },
        ],
      },
      {
        featureType: "landscape.natural",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            color: "#C9C9C9",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.line",
        elementType: "geometry",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.line",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.station.airport",
        elementType: "geometry",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.station.airport",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [
          {
            color: "#C9C9C9",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
    ],
  });
  const icons = {
    base: location.origin + "/static/map_marker.png",
    small: location.origin + "/static/map_marker-small.png",
  };
  const features = [
    {
      position: new google.maps.LatLng(-33.91721, 151.2263),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91539, 151.2282),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91747, 151.22912),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.9191, 151.22907),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91725, 151.23011),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91872, 151.23089),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91784, 151.23094),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91682, 151.23149),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.9179, 151.23463),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91666, 151.23468),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.916988, 151.23364),
      type: "base",
    },
    {
      position: new google.maps.LatLng(-33.91662347903106, 151.22879464019775),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.916365282092855, 151.22937399734496),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.91665018901448, 151.2282474695587),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.919543720969806, 151.23112279762267),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.91608037421864, 151.23288232673644),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.91851096391805, 151.2344058214569),
      type: "small",
    },
    {
      position: new google.maps.LatLng(-33.91818154739766, 151.2346203981781),
      type: "small",
    },
  ];
  // Create markers.
  for (let i = 0; i < features.length; i++) {
    console.log();
    const marker = new google.maps.Marker({
      position: features[i].position,
      icon: icons[features[i].type],
      map: map,
    });
  }

  function resizeMap() {
    const { clientWidth } = document.querySelector("body");
    const blocksGap = 25;
    const containerPaddings = 120;
    const maxContainerWidth = 1600;
    const insideMapWidth =
      ((Math.min(clientWidth, maxContainerWidth) - containerPaddings) / 3) * 2 -
      blocksGap;

    const outWidth = Math.max(0, (clientWidth - maxContainerWidth) / 2);
    const setWidth = outWidth + insideMapWidth + containerPaddings / 2;

    document.querySelector("#map").style.width = setWidth + "px";
  }
  resizeMap();

  window.onresize = resizeMap;
});
