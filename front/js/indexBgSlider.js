$(document).ready(function () {
  const wrapper = $(".bg-slider__wrapper");
  const options = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 3000,
    swipeToSlide: false,
    swipe: false,
  };
  wrapper.slick(options);
});
