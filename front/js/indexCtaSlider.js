$(document).ready(function () {
  const cta = $(".cta");
  const wrapper = cta.find(".cta__scroll-wrapper");
  const tabs = cta.find(".cta__scroll-item");
  const variants = cta.find(".cta__composition-variant");
  const options = {
    vertical: true,
    slidesToShow: 10,
    slidesToScroll: 1,
    infinite: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 1500,
    centerMode: true,
  };

  tabs.click(function () {
    const that = $(this);
    const { slickIndex, variant } = $(this).data();
    if (wrapper[0].hasOwnProperty("slick") && !wrapper[0].slick.unslicked) {
      $(wrapper).slick("slickGoTo", slickIndex);
    } else {
      tabs.each(function () {
        if ($(this).is(that)) {
          $(this).addClass("active");
        } else {
          $(this).removeClass("active");
        }
      });
    }
    setVariantByUid(variants, variant);
  });

  wrapper.on("beforeChange", function (e, slick, currentSlide, nextSlide) {
    cta.find(".cta__scroll-item").each(function () {
      const { slickIndex, variant } = $(this).data();
      if (slickIndex === nextSlide) {
        $(this).addClass("active");
        setVariantByUid(variants, variant);
      } else {
        $(this).removeClass("active");
      }
    });
  });

  if ($(window).width() >= 1100) {
    wrapper.slick(options);
  }
  $(window).resize(function () {
    if ($(this).width() >= 1100) {
      if (wrapper[0].hasOwnProperty("slick") && wrapper[0].slick.unslicked) {
        wrapper.slick(options);
      } else if (!wrapper[0].hasOwnProperty("slick")) {
        wrapper.slick(options);
      }
    } else {
      if (wrapper[0].hasOwnProperty("slick")) {
        wrapper.slick("unslick");
      }
    }
  });

  function setVariantByUid(nodeList, uid) {
    nodeList.each(function () {
      if ($(this).data().variant === uid) {
        $(this).addClass("show");
      } else {
        $(this).removeClass("show");
      }
    });
  }
});
