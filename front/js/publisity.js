$(document).ready(function () {
  const wrapper = $(".publicity__items ul");
  const options = {
    nextArrow: $(".publicity__controls-prev"),
    prevArrow: $(".publicity__controls-next"),
    slidesToShow: 3,
    slidesToScroll: 1,
  };

  if ($(window).width() >= 1100) {
    wrapper.slick(options);
  }
  $(window).resize(function () {
    if ($(this).width() >= 1100) {
      if (wrapper[0].hasOwnProperty("slick") && wrapper[0].slick.unslicked) {
        wrapper.slick(options);
      } else if (!wrapper[0].hasOwnProperty("slick")) {
        wrapper.slick(options);
      }
    } else {
      if (wrapper[0].hasOwnProperty("slick")) {
        wrapper.slick("unslick");
      }
    }
  });
});
