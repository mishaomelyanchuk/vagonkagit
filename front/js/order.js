$(document).ready(function () {
  const clientToggle = $(".order__client-toggle li");
  const clientFilds = $(".order__client-fields");
  const deliveryType = $(".order__delivery-type");
  const deliveryTypeInputs = deliveryType.find("input[type=radio]");
  const deliveryTypeDescriptions = deliveryType
    .find(".order__delivery-descriptions")
    .children();
  const codeSection = $(".order__checkout-code");
  const codeToggle = codeSection.find(".code-toggle");

  clientToggle.click(function () {
    if ($(this).hasClass("active")) return;

    const { field: toggleField } = $(this).data();

    clientToggle.removeClass("active");
    clientFilds.removeClass("active");

    $(this).addClass("active");
    clientFilds.each(function () {
      const { field } = $(this).data();
      if (field === toggleField) $(this).addClass("active");
    });
  });
  codeToggle.click(function () {
    codeSection.addClass("open-code");
  });
  deliveryTypeInputs.change(function () {
    const type = $(this).attr("id");
    deliveryTypeDescriptions.each(function () {
      const { delivery } = $(this).data();
      if (delivery === type) {
        $(this).addClass("show");
      } else {
        $(this).removeClass("show");
      }
    });
  });
});
