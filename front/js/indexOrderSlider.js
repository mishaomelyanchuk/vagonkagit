$(document).ready(function () {
  const linksClass = "text-link";
  const activeLinkClass = "text-active";
  const activeVariantClass = "variant-active";
  const wrapper = $(".offer-slider");
  const links = $(`.${linksClass}`);
  const variantsWrapper = $(".offer__variant");

  wrapper.click(function (e) {
    if ($(window).width() < 760) return;
    const { target } = e;

    if (target.nodeName === "SPAN") {
      if ($(target).hasClass(activeLinkClass)) return;

      const { variants } = $(target).data();

      links.each(function () {
        if ($(this).data().variants === variants) {
          $(this).addClass(activeLinkClass);
        } else {
          $(this).removeClass(activeLinkClass);
        }
      });

      variantsWrapper.each(function () {
        if ($(this).data().variants === variants) {
          $(this).addClass(activeVariantClass);
        } else {
          $(this).removeClass(activeVariantClass);
        }
      });
    }
  });
});
