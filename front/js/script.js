$(document).ready(function () {
  const body = $("body");
  const mobileMenuToggle = $(".header__mobile-toggle");
  const header = $(".header");
  const headerLang = $(".header__lang");
  const isHeaderTop = body.hasClass("header-top");
  const selects = $("select");
  const popupLinks = $(".header__actions a[data-link]");
  const lang = $(".header__lang-display");
  const langPopup = $(".header__lang-popup");

  mobileMenuToggle.click(function () {
    body.toggleClass("mobile-menu");
  });
  lang.click(function () {
    langPopup.toggleClass("show");
  });

  $(document).click(function (e) {
    var path =
      e.originalEvent.path ||
      (e.originalEvent.composedPath && e.originalEvent.composedPath());

    if (!$(path).hasClass("header__actions")) {
      popupLinks.each(function () {
        $(this).siblings("section").removeClass("show");
      });
    }

    if (!$(path).hasClass("header__lang")) {
      headerLang.find(".header__lang-popup").removeClass("show");
    }
  });

  popupLinks.click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    const that = $(this);
    const { link } = that.data();
    const popup = that.siblings("section");
    var path =
      e.originalEvent.path ||
      (e.originalEvent.composedPath && e.originalEvent.composedPath());

    headerLang.find(".header__lang-popup").removeClass("show");

    if (!path || ($(window).width() <= 760 && link !== "account")) {
      location.pathname = that[0].pathname;
    } else {
      if (popup.hasClass("show")) {
        popup.toggleClass("show");
      } else {
        popupLinks.each(function () {
          $(this).siblings("section").removeClass("show");
        });
        popup.addClass("show");
      }
    }
  });
  $(window).scroll(function () {
    const height = $(window).height();
    const scrollPosition = $(window).scrollTop();

    if (scrollPosition >= height / 4) {
      if (isHeaderTop) body.removeClass("header-top");
      body.addClass("header-fixed");
    } else {
      if (isHeaderTop) body.addClass("header-top");
      body.removeClass("header-fixed");
    }
  });

  selects.each(function () {
    const that = $(this);
    const numberOfOptions = $(this).children("option").length;
    const isColorSelect = that.attr("name") === "color";

    that.addClass("select-hidden");
    isColorSelect
      ? that.wrap('<div class="select select-color"></div>')
      : that.wrap('<div class="select"></div>');

    that.after('<div class="select-styled"><div></div></div>');

    const $styledSelect = that.next("div.select-styled");

    $styledSelect.children("div").text(that.children("option").eq(0).text());

    const $list = $("<ul />", {
      class: "select-options",
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
      const color =
        that.children("option").eq(i).attr("data-color") || "transparent";
      $("<div />", {
        css: {
          background: color,
        },
        text: that.children("option").eq(i).text(),
      }).appendTo(
        $("<li />", {
          "data-color": color,
          rel: that.children("option").eq(i).val(),
        }).appendTo($list)
      );
    }
    const $listItems = $list.children("li");

    $styledSelect.click(function (e) {
      e.stopPropagation();
      const { currentTarget } = e;

      if ($(currentTarget).hasClass("active")) {
        $(currentTarget).removeClass("active").next("ul.select-options").hide();
      } else {
        $("div.select-styled").each(function () {
          if ($(this).is($(currentTarget))) {
            $(this).addClass("active").next("ul.select-options").toggle();
          } else {
            $(this).removeClass("active").next("ul.select-options").hide();
          }
        });
      }
    });
    $listItems.click(function (e) {
      e.stopPropagation();
      $styledSelect.children("div").css("background", $(this).data().color);
      $styledSelect.children("div").text($(this).text());
      console.log($styledSelect)
      $styledSelect.removeClass("active");
      that.val($(this).attr("rel"));
      that.trigger("change");
      $list.hide();
    });
    $(document).click(function () {
      $styledSelect.removeClass("active");
      $list.hide();
    });
  });
});
