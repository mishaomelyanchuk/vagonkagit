$(document).ready(function () {
  const toggle = $(".catalog__filters-toggle");
  const filters = $(".catalog__filters-wrapper");
  const form = $("#filters");
  const formResetBtn = $("#form-reset");
  const selects = $("select");

  const product = $(".catalog-item");
  const colors = $(".catalog-item__color ul li");
  const colorsVideo = $(".catalog-item__video video");

  toggle.click(function () {
    toggle.hide();
    filters.show();
  });
  formResetBtn.click(function () {
    form[0].reset();
    $(".select-options").each(function () {
      $(this).children("li").first()[0].click();
    });
    $(this).hide();
  });
  selects.change(function () {
    if ($(this).val()) formResetBtn.show();
  });
  form.on("submit", function (e) {});
  //show color video on hover
  product.mousemove(function (e) {
    const { target } = e;
    if (target.nodeName == "LI") {
      const { color } = target.dataset;
      const videos = $(this).find("video");

      videos.each(function () {
        const { colorVideo } = $(this).data();
        colorVideo === color ? $(this).show() : $(this).hide();
      });
    } else {
      $(this).find("video").hide();
    }
  });
  //redirect to product
  product.click(function (e) {
    var path =
      e.originalEvent.path ||
      (e.originalEvent.composedPath && e.originalEvent.composedPath());

    for (let i = 0; i < path.length; i++) {
      if (path[i].nodeName === "PICTURE") {
        const { productPath } = $(this).data();
        const { origin } = window.location;

        window.location.href = `${origin}${productPath}`;
        return;
      }
    }
  });
});
