$(document).ready(function () {
  const offerBg = $(".offer__bg");
  const offerPattern = $(".offer__pattern");

  $(window).scroll(function () {
    const scrollTop = $(this).scrollTop();
    const screenHeigh = $(window).height();

    if ($(window).width() >= 1100) {
      offerBg.each(function () {
        const { top } = $(this).offset();

        if (scrollTop + screenHeigh > top) {
          const translate = Math.min(
            0,
            Math.max((top - screenHeigh - scrollTop) / 12, -120)
          );
          $(this).css({
            transform: `translateY(${translate}px)`,
          });
        }
      });

      offerPattern.each(function (index) {
        const { top } = $(this).offset();

        if (scrollTop + screenHeigh > top) {
          console.log();
          const translate = Math.min(
            0,
            Math.max((top - screenHeigh - scrollTop) / 6, -150)
          );
          $(this)
            .find("img")
            .css({
              transform: `translateY(${translate}px)`,
            });
        }
      });
    }
  });
  $(window).resize(function () {
    if ($(this).width() < 1100) {
      offerBg.each(function () {
        $(this).css({ transform: "" });
      });
      offerPattern.each(function () {
        $(this).find("img").css({ transform: "" });
      });
    }
  });
});
