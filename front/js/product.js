$(function () {
  $(document).ready(function () {
    const counterButtons = $(".counter-button");
    const header = $(".header");
    const anchoreLinks = $(".product__links a");
    const colorInputs = $(".product__colors-values input[name=color]");
    const colorInputslabels = $(".product__colors-values label");
    const colorSamples = $(".product__colors-sample ul li");
    const pattern = $(".product__composition-pattern");

    let lastScrollTop = 0;

    $(window).scroll(function (e) {
      const scrollTop = $(this).scrollTop();

      if ($(this).width() >= 1100) {
        scrollTop > lastScrollTop
          ? pattern.css("transform", "translateY(-75px)")
          : pattern.css("transform", "translateY(75px)");

        setTimeout(function () {
          pattern.css("transform", "");
        }, 550);
      }
      lastScrollTop = scrollTop;
    });

    counterButtons.click(function (e) {
      const { value } = $(this).data();

      if (value === "minus") {
        $(this).siblings("input")[0].stepDown();
      } else if (value === "plus") {
        $(this).siblings("input")[0].stepUp();
      }
    });
    anchoreLinks.click(function (e) {
      e.preventDefault();
      $("html, body").animate(
        {
          scrollTop:
            $($.attr(this, "href")).offset().top - header.height() - 30,
        },
        500
      );
    });

    colorInputs.change(function () {
      const value = $(this).val();

      colorSamples.each(function () {
        const { color } = $(this).data();

        if (color == value) {
          $(this).show();
          $(this).find("video")[0].load();
        } else {
          $(this).hide();
          $(this).find("video")[0].pause();
        }
      });
    });

    colorInputslabels.mouseenter(function () {
      const labelInput = $(this).find("input[type=radio]");

      if (labelInput.is(":checked")) {
        colorSamples.each(function () {
          const { color } = $(this).data();
          if (color == labelInput.val()) {
            $(this).find("video").trigger("play");
          }
        });
      }
    });
  });
});
