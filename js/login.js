var Login = {

    request: function(email, pass, redirect) {
        var admin_url = $('#admin-ajax-url').text();
        email = typeof(email) == 'undefined' ? $("#login_email").val() : email;
        pass = typeof(pass) == 'undefined' ? $("#login_pass").val() : pass;

        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'customer_login',
                email: email,
                pass: pass
            },
            success: function(result) {
                if(result.res === -1) {
                    var login_error_el = $('#login_error_message');
                    $(login_error_el).html(result.message);
                    $(login_error_el).show();
                    return;
                }
                if(typeof(redirect) == 'undefined') {
                    window.location.reload();
                } else {
                    window.location.href = redirect;
                }

            }
        });
    }

}

var Login_Mobile = {
    
    request: function(lang) {
        var admin_url = $('#admin-ajax-url').text();
        email = typeof(email) == 'undefined' ? $("#login_email_mobile").val() : email;
        pass = typeof(pass) == 'undefined' ? $("#login_pass_mobile").val() : pass;

        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'customer_login',
                email: email,
                pass: pass
            },
            success: function(result) {
                if(result.res === -1) {
                    var login_error_el = $('#login_error_message_mobile');
                    $(login_error_el).html(result.message);
                    $(login_error_el).show();
                    return;
                }
                if((lang) == 'uk') {
                    window.location.href = '/profile';
                } else {
                    window.location.href = '/ru/profile';
                }

            }
        });
    }

}

var Login_Checkout = {

    request: function(email, pass) {
        var admin_url = $('#admin-ajax-url').text();
        email = typeof(email) == 'undefined' ? $("#checkout_login").val() : email;
        pass = typeof(pass) == 'undefined' ? $("#checkout_password").val() : pass;

        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'checkout_login',
                email: email,
                pass: pass
            },
            success: function(result) {
                if(result.res === -1) {
                    $('#error_log').css("display","block");
                    return;
                }
                if(result.redirect != '') {
                    window.location.reload(result.redirect);
                    // $('.order__client').empty();
                    // $('.order__client').html('<div class="order__client-title"><h2>Контактные данные</h2></div><input type="hidden" name="client_id" id="order__client-id" value="'+result.user.id+'"/><p>'+result.user.customer_name+' ' +result.user.customer_fname+'<br>'+result.user.phone+'</br>'+result.user.user_email+'</p>');
                }
            }
        });
    }

}

var Registration_Checkout = {

    request: function(fname, name, email, phone) {
        var admin_url = $('#admin-ajax-url').text();
        fname = typeof(fname) == 'undefined' ? $("#fname").val() : fname;
        name = typeof(name) == 'undefined' ? $("#name").val() : name;
        email = typeof(email) == 'undefined' ? $("#email").val() : email;
        phone = typeof(phone) == 'undefined' ? $("#phone").val() : pass;

        if(!registration.checkForm()) return;
        
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'checkout_registration',
                fname: fname,
                name: name,
                email: email,
                phone: phone
            },
            success: function(result) {
                if(result.res === -1) {
                    $('#error_reg').css("display","block");
                    return;
                }
                if(result.user != '') {
                    $('.order__client').empty();
                    if ( currentLanguage == 'uk' ) {
                        $('.order__client').html('<div class="order__client-title"><h2>Контактні дані</h2></div><input type="hidden" name="client_id" id="order__client-id" value="'+result.user.id+'"/><p>Дякую! Обліковий запис створено:</br>'+result.user.customer_name+' ' +result.user.customer_fname+'<br>'+result.user.phone+'</br>'+result.user.user_email+'</p>');
                    } else {
                        $('.order__client').html('<div class="order__client-title"><h2>Контактные данные</h2></div><input type="hidden" name="client_id" id="order__client-id" value="'+result.user.id+'"/><p>Спасибо! Учетная запись создана:</br>'+result.user.customer_name+' ' +result.user.customer_fname+'<br>'+result.user.phone+'</br>'+result.user.user_email+'</p>');
                    }
                    $('#new_user_stat').val(result.user.id);
                }
            }
        });
    }

}
