document.addEventListener("DOMContentLoaded", function () {
  var phoneInputs = document.querySelectorAll("input[data-tel-input]");
  var getInputNumbersValue = function (input) {
    return input.value.replace(/\D/g, "");
  };

  var onPhonePaste = function (e) {
    var input = e.target,
      inputNumbersValue = getInputNumbersValue(input);
    var pasted = e.clipboardData || window.clipboardData;
    if (pasted) {
      var pastedText = pasted.getData("Text");
      if (/\D/g.test(pastedText)) {
        input.value = inputNumbersValue;
        return;
      }
    }
  };
  var onPhoneInput = function (e) {
    var input = e.target,
      inputNumbersValue = getInputNumbersValue(input),
      selectionStart = input.selectionStart,
      formattedInputValue = "";

    if (!inputNumbersValue) {
      return (input.value = "");
    }

    if (input.value.length != selectionStart) {
      if (e.data && /\D/g.test(e.data)) {
        input.value = inputNumbersValue;
      }
      return;
    }

    var firstSymbols = (inputNumbersValue[0] = "+38");
    formattedInputValue = input.value = firstSymbols + " ";
    if (inputNumbersValue.length > 1) {
      formattedInputValue += "(" + inputNumbersValue.substring(2, 5);
    }
    if (inputNumbersValue.length >= 6) {
      formattedInputValue += ") " + inputNumbersValue.substring(5, 8);
    }
    if (inputNumbersValue.length >= 9) {
      formattedInputValue += "-" + inputNumbersValue.substring(8, 10);
    }
    if (inputNumbersValue.length >= 11) {
      formattedInputValue += "-" + inputNumbersValue.substring(10, 12);
    }

    input.value = formattedInputValue;
  };
  var onPhoneKeyDown = function (e) {
    // Clear input after remove last symbol
    var inputValue = e.target.value.replace(/\D/g, "");
    if (e.keyCode == 8 && inputValue.length == 2) {
      e.target.value = "";
    }
  };
  for (var phoneInput of phoneInputs) {
    phoneInput.addEventListener("keydown", onPhoneKeyDown);
    phoneInput.addEventListener("input", onPhoneInput, false);
    phoneInput.addEventListener("paste", onPhonePaste, false);
  }

  validationForms.init();
});


var ValidationForms = function() {

  this.init = function() {

  }

  this.onSubmitPartners = function() {
    let name = $('#name');
    let email = $('#email');
    let text = $('#text');

    name.css({
      'border-color': '#e0e0e0'
    });

    email.css({
      'border-color': '#e0e0e0'
    });

    text.css({
      'border-color': '#e0e0e0'
    });

    if(name.val() === '') {
      name.css({
        'border-color': 'red'
      });

      return false;
    }

    if(email.val() === '') {
      email.css({
        'border-color': 'red'
      });

      return false;
    }

    if(text.val() === '') {
      text.css({
        'border-color': 'red'
      });

      return false;
    }

    return true;
  }

  this.onSubmitContacts = function() {
    let name = $('#p_name');
    let email = $('#p_email');
    let text = $('#p_text');

    name.css({
      'border-color': '#e0e0e0'
    });

    email.css({
      'border-color': '#e0e0e0'
    });

    text.css({
      'border-color': '#e0e0e0'
    });

    if(name.val() === '') {
      name.css({
        'border-color': 'red'
      });

      return false;
    }

    if(email.val() === '') {
      email.css({
        'border-color': 'red'
      });

      return false;
    }

    if(text.val() === '') {
      text.css({
        'border-color': 'red'
      });

      return false;
    }

    return true;
  }


}

let validationForms = new ValidationForms();
