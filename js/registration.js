var Registration = function($) {

    this.init = function() {
        var _this = this;
        $('#iscommercial').click(function() {
            _this.handleCommercialCheckbox();
        });

        $('#fname').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#name').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#email').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#phone').focus(function() {
            $(this).removeClass('invalid');
        });
    }

    this.handleCommercialCheckbox = function() {
        $('#companyname').toggle();
        $('#companycode').toggle();
        $('#representative').toggle();
    }

    this.checkForm = function() {
        var valid = true;
        var fname = $('#fname');
        var name = $('#name');
        var email = $('#email');
        var phone = $('#phone');

        if($(fname).val() === '') {
            valid = false;
            $(fname).addClass('invalid');
        }

        if($(name).val() === '') {
            valid = false;
            $(name).addClass('invalid');
        }

        if($(email).val() === '') {
            valid = false;
            $(email).addClass('invalid');
        }

        if($(phone).val() === '') {
            valid = false;
            $(phone).addClass('invalid');
        }

        return valid;
    }

    this.checkRepairForm = function() {
        var email_el = $('#repair-email');
        var email = $(email_el).val();
        if(email === '') {
            $(email_el).addClass('invalid');
            return false;
        }
        return true;
    }

}

var registration = new Registration(jQuery);

jQuery(function() {
    registration.init();
});