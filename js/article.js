$(document).ready(function () {
  const sliderWrapper = $(".article__slider ul");

  sliderWrapper.slick({
    nextArrow: $(".article__slider-prev"),
    prevArrow: $(".article__slider-next"),
    slidesToShow: 3,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1099,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 760,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  });

});
