const cartContainer = $(".cart__wrapper");
const cartDeleteBtn = '.cart__product-remove';
const cartCounterBtn = '.counter-button';
const cartCounterInput = '.product__order-counter input';



let quanTimeout = null;

function updateCartElements() {
  let countContainer = $('.cart-count')
  let countContainerParent = countContainer.parent()
  let cartPopupContainer = $('.cart-popup__wrapper')
  $.ajax({
    url: '/wp-admin/admin-ajax.php',
    method: 'post',
    data: {
      action: 'count_update'
    },
    success: function (response) {
      countContainer.remove();
      if(response)
        countContainerParent.append(response)
    }
  })
  $.ajax({
    url: '/wp-admin/admin-ajax.php',
    method: 'post',
    data: {
      action: 'mini_cart_update'
    },
    success: function (response) {
      cartPopupContainer.empty();
      cartPopupContainer.append(response)
    }
  })
}

function cartDeleteItem() {
  const container = $(".cart__wrapper");
  let cartID = $(this).attr('data-cartID');

  $.ajax({
    url: '/wp-admin/admin-ajax.php',
    method: 'post',
    data: {
      action: 'delete_cart_item',
      cardID: cartID
    },
    success: function (response) {
      container.empty();
      container.append(response)

      updateCartElements()
    }
  })
}

function quantitySelector() {
  function isValid(min, max, value) {
    if(isNaN(value)) return false;
    if(parseInt(min) === 0 && (value <= parseInt(min))){
      return false;
    } else if(parseInt(min) !== 0 && (value < parseInt(min))){
      return false;
    } else if(max && value > parseInt(max)) {
      return false;
    }

    return true;
  }
  function btnClick() {
    let thisBtn = $(this)
    let btnAction = thisBtn.attr('data-value')
    let cartId = thisBtn.closest('.cart__product').find('.cart__product-remove').attr('data-cartid')
    let input = thisBtn.siblings("input");
    let inputVal = parseInt(input.val())
    let inputMin = input.attr('min') ? ''+input.attr('min') : '0'
    let inputMax = input.attr('max') ? ''+input.attr('max') : false

    if (btnAction === "minus") {
      if(!isValid(inputMin, inputMax, inputVal-1)) return false;
      input.val(inputVal-1)
    } else if (btnAction === "plus") {
      if(!isValid(inputMin, inputMax, inputVal+1)) return false;
      input.val(inputVal+1)
    }

    sendResult(parseInt(input.val()), cartId)
  }
  function inputEvent(){
    let input = $(this)
    let inputVal = parseInt(input.val())
    let inputMin = input.attr('min') ? ''+input.attr('min') : '0'
    let inputMax = input.attr('max') ? ''+input.attr('max') : false
    let cartId = input.closest('.cart__product').find('.cart__product-remove').attr('data-cartid')

    if(!isValid(inputMin, inputMax, inputVal)) return false;

    sendResult(inputVal, cartId)
  }
  function sendResult(result, id) {
    clearTimeout(quanTimeout)
    quanTimeout = setTimeout(function () {
      cartUpdateQuantity(result, id)
    }, 1000)
  }

  $(document).on('click', cartCounterBtn, btnClick)
  $(document).on('input', cartCounterInput, inputEvent)
}

function cartUpdateQuantity(quantity, cartId) {
  $.ajax({
    url: '/wp-admin/admin-ajax.php',
    method: 'post',
    data: {
      'action': 'update_quantity',
      'cardID': cartId,
      'quantity': quantity
    },
    success: function (response) {
      cartContainer.empty();
      if(response)
        cartContainer.append(response)

      updateCartElements()
    }
  })
}

$(document).ready(function () {

  $(document).on('click', cartDeleteBtn, cartDeleteItem)
  quantitySelector()
});

