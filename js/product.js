
$(document).ready(function () {
  if(!$('.product').length) return false;
  const counterButtons = $(".counter-button");
  const header = $(".header");
  const anchoreLinks = $(".product__links a");
  const colorInputs = $(".variations__value.pa_czvet .variable-item");
  const colorInputslabels = $(".product__colors-values label");
  const colorSamples = $(".product__colors-sample ul li");
  const pattern = $(".product__composition-pattern");
  const addToCart = $(".single_add_to_cart_button");
  const variations = $('.variations__item')
  const varOpts = variations.find('.variable-item')

  if(!$('.pa_czvet.variations__value').length)
    $('.product__wrapper').addClass('no--colors')
  let lastScrollTop = 0;

  $(window).scroll(function (e) {
    const scrollTop = $(this).scrollTop();

    if ($(this).width() >= 1100) {
      scrollTop > lastScrollTop
        ? pattern.css("transform", "translateY(-75px)")
        : pattern.css("transform", "translateY(75px)");

      setTimeout(function () {
        pattern.css("transform", "");
      }, 550);
    }
    lastScrollTop = scrollTop;
  });

  function addQuantity() {
    let varId = $('[name="variation_id"]').val();
    if(!varId) return;
    let varArr = JSON.parse($('.variations_form').attr('data-product_variations')).find(item => item['variation_id'] == varId)
    let quantity = parseInt($('.product__order-counter input').eq(0).val())
    let priceNum = parseFloat((varArr['display_price'] * quantity).toFixed(2));
    let priceOldNum = parseFloat((varArr['display_regular_price'] * quantity).toFixed(2))
    let b2b = $('.product__item-desktop').attr('data-b2b')
    if(b2b && priceOldNum){
      priceOldNum = priceOldNum - ( priceOldNum / 100 * b2b );
    }
    let priceString = priceNum.toString()
    let priceOldString = priceOldNum.toString()
    let priceInt = priceString
    let priceOldInt = priceOldString
    let priceAfterDot = '.00'
    let priceOldAfterDot = '.00'


    if(priceInt.indexOf('.') !== -1){
      let priceArr = priceInt.split('.')
      priceInt = priceArr[0]
      priceAfterDot = '.'+ ( priceArr[1].length === 1 ? priceArr[1] + '0' : priceArr[1] )
    }
    if(priceOldInt.indexOf('.') !== -1){
      let priceArr = priceOldInt.split('.')
      priceOldInt = priceArr[0]
      priceOldAfterDot = '.'+ ( priceArr[1].length === 1 ? priceArr[1] + '0' : priceArr[1] )
    }

    let priceCurrency = $('span.currency').first().text()
    let priceHtml = $('<p>'+priceInt+'<span>'+priceAfterDot+'</span><span> </span><span class="currency">₴</span></p>')

    $('.product__order-price').empty().append(priceHtml)

    let priceOldDesktop = $('.product__order-discount>p')
    priceOldDesktop.each(function () {
      let thisPrice = $(this).find('>*')
      thisPrice.eq(0).text(priceOldInt)
      thisPrice.eq(1).text(priceOldAfterDot)
    })

    if(priceNum !== priceOldNum){
      let percents = Math.round(100 / (priceOldNum / (priceOldNum - priceNum)) );

      $('.product__order-discount').html('<p>-'+percents+'% <span>'+priceOldInt+'</span><span>'+priceOldAfterDot+' </span><span class="currency">₴</span></p>')
    } else {
      $('.product__order-discount').empty()
    }

  }

  $( ".single_variation_wrap" ).on( "show_variation", function () {
    addQuantity()
  });

  counterButtons.click(function (e) {
    const { value } = $(this).data();
    let wooInput = $('.woocommerce-variation-add-to-cart .quantity input')
    if (value === "minus") {
      $(this).siblings("input")[0].stepDown();
      $('.product__order-counter input').val($(this).siblings("input").val())
      wooInput.val($(this).siblings("input").val())
    } else if (value === "plus") {
      $(this).siblings("input")[0].stepUp();
      $('.product__order-counter input').val($(this).siblings("input").val())
      wooInput.val($(this).siblings("input").val())
    }

    addQuantity()
  });
  anchoreLinks.click(function (e) {
    e.preventDefault();
    $("html, body").animate(
      {
        scrollTop:
          $($.attr(this, "href")).offset().top - header.height() - 30,
      },
      500
    );
  });

  function selectColor(btn){
    const value = btn.attr('data-value');

    colorSamples.each(function () {
      const { color } = $(this).data();

      if (color == value) {
        $(this).show();
        $(this).find("video")[0].load();
      } else {
        $(this).hide();
        $(this).find("video")[0].pause();
      }
    });
  }

  function setPrice(){
    let calcInterval = false;
    let canContinue = true;

    variations.each(function () {
      if(!$(this).find('.selected').length)
        canContinue = false;
    })

    if(!canContinue) return;

    calcInterval = setInterval(function () {
      let priceText = $('.woocommerce-variation.single_variation .price').text();
    
        clearInterval(calcInterval)
        let currency = $('.woocommerce-variation.single_variation .woocommerce-Price-currencySymbol').first().text()

        if($('.woocommerce-variation.single_variation del').length){
          let b2b = $('.product__item-desktop').attr('data-b2b')
       
          let price = parseFloat($('.woocommerce-variation.single_variation ins').text().replace(currency, ''))
          let afterDot = price.toString().split('.').pop();
          if(price.toString().indexOf('.') === -1) afterDot = '00';
          if(afterDot.length === 1) afterDot+='0'
          let discountPrice = parseFloat($('.woocommerce-variation.single_variation del').text().replace(currency, ''))
          if (b2b) {
            discountPrice = discountPrice - ( discountPrice / 100 * b2b );
          }

          //$('.product__order-price').html('<p>'+price+'<span>.'+afterDot+'</span><span> </span><span class="currency">'+currency+'</span></p>')

          $('.product__order-promo').show()
        } else {
          let price = $('.woocommerce-variation.single_variation .price').text().replace(currency, '')
          let afterDot = price.split('.').pop();
          if(price.toString().indexOf('.') === -1) afterDot = '00';
          if(afterDot.length === 1) afterDot+='0'
          price = price.split('.')[0]
          //$('.product__order-price').html('<p>'+price+'<span>.'+afterDot+'</span><span> </span><span class="currency">'+currency+'</span></p>')

          $('.product__order-promo').hide()
        }

        $('.product__orderinfo-color').attr('data-value', $(".variations__value.pa_czvet .variable-item.selected").attr('data-value'))
        $('.product__orderinfo-name').text($(".variations__value.pa_czvet .variable-item.selected span").text())
        $('.product__orderinfo-size').text($(".variations__value.pa_obem .variable-item.selected span").text())
    
    }, 50)

  }

  colorInputs.on('click', function () {
    selectColor($(this))
  });
  setTimeout(function () {
    selectColor($(".variations__value.pa_czvet .variable-item.selected"))
    setPrice();
  }, 500)

  colorInputslabels.mouseenter(function () {
    const labelInput = $(this).find("input[type=radio]");

    if (labelInput.is(":checked")) {
      colorSamples.each(function () {
        const { color } = $(this).data();
        if (color == labelInput.val()) {
          $(this).find("video").trigger("play");
        }
      });
    }
  });

  $('form.cart').on('submit', function (e) {
    e.preventDefault()
    let form = this;
    let cartLink = $('[data-link="cart"]')
    let countContainer = cartLink.find('.cart-count')
    let cartContainer = $('.cart-popup__wrapper')
    let formdata = new FormData(this)

    $.ajax({
      url: $(form).attr('action'),
      method: $(form).attr('method'),
      data: formdata,
      processData: false,
      contentType: false,
      success: function () {
        successRequests()
        console.log('success')
      }
    })

    function successRequests() {

      $.ajax({
        url: '/wp-admin/admin-ajax.php',
        method: 'post',
        data: {
          action: 'count_update'
        },
        success: function (response) {
          countContainer.remove();
          if(response)
            cartLink.append(response)
        }
      })
      $.ajax({
        url: '/wp-admin/admin-ajax.php',
        method: 'post',
        data: {
          action: 'mini_cart_update',
          is_product: 'true'
        },
        success: function (response) {
          cartContainer.empty();
          cartContainer.append(response)
        }
      })
      
    }
  });
  


  varOpts.on('click', function () {
    setTimeout(function () {
      setPrice()
    }, 300)
  })
  
  $('.product__order-submit button').on('click', function () {
    $('.single_add_to_cart_button').click()
  })

});