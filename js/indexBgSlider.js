$(document).ready(function () {
  const wrapper = $(".bg-slider.swiper");
  let defaultSpeed = 3000
  let firstInit = true;
  let indexSwiper = new Swiper(wrapper[0], {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: true,
    speed: 500,
    allowTouchMove: false,
    autoplay: {
      delay: (wrapper.find('.swiper-slide:first-child video').length ? (wrapper.find('.swiper-slide:first-child video')[0].duration * 1000) - 500 : defaultSpeed),
      disableOnInteraction: false
    },
    pagination: {
      el: '.swiper-pagination',
      type: 'bullets',
      clickable: true
    },
    on: {
      afterInit: function(swiper) {
        setTimeout(function () {
          wrapper.addClass('can-play')
        }, 200)
      }
    }
  })

  function checkForVideo() {
    let activeSlide = wrapper.find('.swiper-slide-active')
    let thisVideo = activeSlide.find('video')
    let slideDuration = defaultSpeed;

    if(firstInit){
      slideDuration -= 200;
      firstInit = false;
    }

    if(!thisVideo.length){
      wrapper.find('.swiper-slide').removeClass('is-playing')
      indexSwiper.params.autoplay.delay = slideDuration
      wrapper.find('.swiper-pagination-bullet-active').css('transition-duration', slideDuration+'ms')
      return;
    }
    if(activeSlide.hasClass('is-playing')) return;

    slideDuration = (thisVideo[0].duration * 1000) - 500

    if(firstInit){
      slideDuration -= 200;
      firstInit = false;
    }

    thisVideo[0].currentTime = 0
    indexSwiper.params.autoplay.delay = slideDuration
    wrapper.find('.swiper-pagination-bullet-active').css('transition-duration', slideDuration+'ms')
    thisVideo[0].play()

    activeSlide.addClass('is-playing')
  }

  checkForVideo()
  indexSwiper.on('transitionStart', checkForVideo)

});
