$(document).ready(function () {
  if (!$('.company-history__button').length) return false;
  const historyToggle = $(".company-history__button");
  const historyHiddenText = $(".company-history__text.hidden-text");
  const sliderWrapper = $(".company-slider__wrapper");
  const dragSlider = $(".drag-slider__wrapper");
  const dragSliderImages = $(".company-images__wrapper ul li");
  const sliderOptions = {
    arrows: false,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 6000,
    cssEase: "linear",
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3,
          pauseOnHover: true,
        },
      },
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          autoplaySpeed: 0,
          speed: 3000,
          cssEase: "cubic-bezier(.15,1.3,.15,.91)",
          variableWidth: true,
          swipe: true,
          pauseOnHover: false,
        },
      },
    ],
  };
  let sliderTimer;

  historyToggle.click(function () {
    if (historyHiddenText.hasClass("hidden")) {
      historyHiddenText.slideDown(300);
    } else {
      historyHiddenText.slideUp(300);
    }
    historyHiddenText.toggleClass("hidden");
    $(this).toggleClass("open");
  });
  sliderWrapper.slick(sliderOptions);

  dragSlider.find("li").each(function (index) {
    const colorsLength = dragSlider.find("li").length;
    const rotateIndex = 360 / colorsLength;
    const rotateStart = 75;
    const rotateAngle = rotateStart + index * rotateIndex;
    const skew = 75;
    const transformStr = "rotate(" + rotateAngle + "deg) skew(" + skew + "deg)";

    $(this).css({ transform: transformStr });
    $(this).data({
      angle: (6.2832 / colorsLength) * index,
      index,
    });
  });
  dragSliderImages.each(function (index) {
    if (index === 0) $(this).show();
    $(this).data({
      index,
    });
  });
  dragSlider[0].ontouchstart = (e) => {
    e.preventDefault();
  };
  dragSlider[0].ontouchmove = (e) => {
    e.preventDefault();
  };
  dragSlider[0].ontouchend = (e) => {
    e.preventDefault();
  };
  interact(dragSlider[0]).draggable({
    onstart: function (event) {
      const element = event.target;
      const rect = element.getBoundingClientRect();

      element.dataset.centerX = rect.left + rect.width / 2;
      element.dataset.centerY = rect.top + rect.height / 2;

      element.dataset.angle = getDragAngle(event);
    },
    onmove: function (event) {
      var element = event.target;
      const radians = 6.2832;
      const itemsLength = dragSlider.find("li").length;
      const step = radians / itemsLength;

      var center = {
        x: parseFloat(element.dataset.centerX) || 0,
        y: parseFloat(element.dataset.centerY) || 0,
      };

      var angle = getDragAngle(event);
      const fullTurns = Math.abs(Math.trunc(angle / radians));
      const angleCorrection = angle - fullTurns * radians;
      const activeItem =
        angleCorrection >= 0
          ? Math.trunc(angleCorrection / step)
          : itemsLength - 1 - Math.abs(Math.trunc(angleCorrection / step));

      dragSliderImages.each(function (index) {
        if (index === activeItem) {
          $(this).show();
        } else {
          $(this).hide();
        }
      });

      element.style.transform = "rotate(" + angle + "rad" + ")";
    },
    onend: function (event) {
      const element = event.target;

      element.dataset.angle = getDragAngle(event);
    },
  });
  if ($(window).width() >= 1100) {
    sliderWrapper.find(".slick-track").on("mouseenter", hoverIn);
    sliderWrapper.find(".slick-track").on("mouseleave", hoverOut);
  }
  $(window).resize(function () {
    if ($(window).width() >= 1100) {
      sliderWrapper.find(".slick-track").off("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").off("mouseleave", hoverOut);
      sliderWrapper.find(".slick-track").on("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").on("mouseleave", hoverOut);
    } else {
      sliderWrapper.find(".slick-track").off("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").off("mouseleave", hoverOut);
    }
  });

  function getDragAngle(event) {
    var element = event.target;
    var startAngle = parseFloat(element.dataset.angle) || 0;
    var center = {
      x: parseFloat(element.dataset.centerX) || 0,
      y: parseFloat(element.dataset.centerY) || 0,
    };
    var angle = Math.atan2(center.y - event.clientY, center.x - event.clientX);

    return angle - startAngle;
  }
  function hoverIn() {
    $(this).addClass("hover-stop");
    const { slideWidth, currentLeft } = sliderWrapper[0].slick;
    const { speed, easing, autoplaySpeed } = sliderWrapper[0].slick.options;
    const offsetLeft = $(this)
      .css("transform")
      .replace("matrix(", "")
      .replace(")", "")
      .split(",")
      .reduce((num, acc) => Math.min(acc, num));

    const currentTransform = currentLeft - slideWidth;
    const progressCorrection = (
      (Math.abs(currentTransform) - Math.abs(offsetLeft)) /
      slideWidth
    ).toFixed(2);

    const timeCorrection = speed * progressCorrection;
    const transformStr = `translate3d(${currentTransform}px,0,0)`;
    $(this).data({
      transform: transformStr,
      transition: `transform ${timeCorrection}ms ${easing} ${autoplaySpeed}s`,
      timer: timeCorrection,
    });

    $(this).css({
      transform: `translate3d(${offsetLeft}px,0,0)`,
      transition: "",
    });
    sliderWrapper.slick("slickPause");
    sliderWrapper.on("afterChange", holdPosition);
  }
  function hoverOut() {
    $(this).removeClass("hover-stop");
    const { transform, transition, timer } = $(this).data();

    $(this).css({
      transform,
      transition,
    });
    if (sliderTimer) {
      clearTimeout(sliderTimer);
    }
    sliderTimer = setTimeout(function () {
      sliderWrapper.slick("slickPlay");
      sliderWrapper.off("afterChange", holdPosition);
    }, timer);
  }
  function holdPosition(_, slick) {
    const { $slideTrack, currentSlide, slideCount } = slick;
    const { transition } = $slideTrack.data();
    const offsetLeft = $slideTrack.offset().left - 60;

    setTimeout(function () {
      $slideTrack.css({
        transition,
        transform: `translate3d(${offsetLeft}px,0,0)`,
      });
    }, 0);
  }

  sliderWrapper.on("beforeChange", function (_, slider, current) {
    const that = $(this);
    const { length } = slider.$slides;

    if (current === length - 1 && $(window).width() < 1100) {
      $(".company-slider__item").each(function () {
        $(this).find("span").text($(this).data().slickIndex);
      });
      setTimeout(function () {
        that
          .find(`.slick-cloned[data-slick-index=${length}]`)
          .addClass("slick-center");
      }, 0);
    }
  });
  sliderWrapper.on("afterChange", function (_, slider, current) {
    const that = $(this);
    const { length } = slider.$slides;

    if ($(window).width() >= 1100) {
      const transform = slider.$slideTrack.css("transform");
      const transition = slider.$slideTrack.css("transition");
      slider.$slideTrack.css("transform", "");
      slider.$slideTrack.css("transition", "");

      setTimeout(function () {
        slider.$slideTrack.css({ transform });
        slider.$slideTrack.css({ transition });
      }, 0);
    }
  });
});
