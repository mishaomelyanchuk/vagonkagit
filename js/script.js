function onDocumentReady() {
  const body = $("body");
  const mobileMenuToggle = $(".header__mobile-toggle");
  const header = $(".header");
  const headerLang = $(".header__lang");
  const isHeaderTop = body.hasClass("header-top");
  const selects = $("select");
  const popupLinks = $(".header__actions a[data-link]");
  const lang = $(".header__lang-display");
  const langPopup = $(".header__lang-popup");
  const slider = $('.bg-slider');
  const ctaItems = $('.cta__scroll-item');
  const ctaContainer = $('.cta__scroll');

  let container_top_padding = slider.height() - 100;
  slider.next()
      .find('.container')
      .css({
        'padding-top': container_top_padding
      });


  // let maxRight = body.width() - 20;
  // if(ctaContainer.length > 0) {
  //   let ctaItemsWidth = maxRight - ctaContainer.position().left;
  //   ctaItems.each(function(i, el) {
  //     $(el).css({
  //       'width': ctaItemsWidth + 'px'
  //     });
  //   });
  // }


  mobileMenuToggle.click(function () {
    body.toggleClass("mobile-menu");
  });
  lang.click(function () {
    langPopup.toggleClass("show");
  });

  $(langPopup).find("a").click(function() {
    langPopup.toggleClass("show");
    let lang_prefix = $(this).html();
    $(lang).find('span').html(lang_prefix);
  });

  $(document).click(function (e) {
    if(!e.originalEvent) return;
    var path =
      e.originalEvent.path ||
      (e.originalEvent.composedPath && e.originalEvent.composedPath());

    if (!$(path).hasClass("header__actions")) {
      popupLinks.each(function () {
        $(this).siblings("section").removeClass("show");
      });
    }

    if (!$(path).hasClass("header__lang")) {
      headerLang.find(".header__lang-popup").removeClass("show");
    }
  });

  popupLinks.click(function (e) {
    e.preventDefault();
    e.stopPropagation();
    let that = $(this);
    let { link } = that.data();
    let popup = that.siblings("section");
    let path =
      e.originalEvent.path ||
      (e.originalEvent.composedPath && e.originalEvent.composedPath());

    headerLang.find(".header__lang-popup").removeClass("show");

    if (!path || ($(window).width() <= 760 && link !== "account")) {
      location.pathname = that[0].pathname;
    } else {
      if (popup.hasClass("show")) {
        popup.toggleClass("show");
      } else {
        popupLinks.each(function () {
          $(this).siblings("section").removeClass("show");
        });
        popup.addClass("show");
      }
    }
  });
  $(window).scroll(function () {
    const height = $(window).height();
    const scrollPosition = $(window).scrollTop();

    if (scrollPosition >= height / 4) {
      if (isHeaderTop) body.removeClass("header-top");
      body.addClass("header-fixed");
    } else {
      if (isHeaderTop) body.addClass("header-top");
      body.removeClass("header-fixed");
    }
  });


  selects.each(function () {
    const that = $(this);
    const numberOfOptions = $(this).children("option").length;
    const isColorSelect = that.attr("name") === "color";

    that.addClass("select-hidden");
    isColorSelect
      ? that.wrap('<div class="select select-color"></div>')
      : that.wrap('<div class="select"></div>');

    that.after('<div class="select-styled">'+(isColorSelect ? '<div>'+that.children("option").eq(0).text().trim()+'</div>' : '<input type="text" '+ (!that.hasClass('allow-type') ? 'readonly' : '') +'>')+'</div>');

    const $styledSelect = that.next("div.select-styled");

    if(that.find('[selected]').length){
      $styledSelect.find("input").val(that.find("[selected]").text().trim());
    } else {
      $styledSelect.find("input").val(that.children("option").eq(0).text().trim());
    }


    const $list = $("<ul />", {
      class: "select-options",
      id: $(this).attr('id') + '_list',
    }).insertAfter($styledSelect);

    for (var i = 0; i < numberOfOptions; i++) {
      const color =
        that.children("option").eq(i).attr("data-color") || "transparent";
      $("<div />", {
        css: {
          background: color,
        },
        text: that.children("option").eq(i).text(),
      }).appendTo(
        $("<li />", {
          "data-color": color,
          rel: that.children("option").eq(i).val(),
        }).appendTo($list)
      );
    }
    let $listItems = $list.children("li");
    const $mainInput = $styledSelect.find(">input");

    let textChanged = false

    $(this).closest('.select').find('.select-styled').attr('data-prev-text', $mainInput.val())

    $mainInput.on('focusin', function () {
      let thisInput = $(this)
      textChanged = false;
      $mainInput.val('')

      setTimeout(function () {
        thisInput.closest('.select').find('li').each(function () {
          $(this).show()
        })
      }, 100)

    })
    $mainInput.on('focusout', function () {
      $mainInput.val($(this).closest('.select').find('.select-styled').attr('data-prev-text'))
    })
    $mainInput.on('input', function () {
      let thisContainer = $(this).closest('.select')
      let thisSelect = $(this).closest('.select-styled')
      let thisVal = $(this).val().toLowerCase().trim()

      textChanged = true

      if(!thisSelect.hasClass('active')) {
        $("div.select-styled").each(function () {
          if ($(this).is(thisSelect)) {
            $(this).addClass("active").next("ul.select-options").toggle();
          } else {
            $(this).removeClass("active").next("ul.select-options").hide();
          }
        });
      }

      thisContainer.find('li').each(function () {
        if($(this).text().toLowerCase().trim().indexOf(thisVal) === -1) {
          $(this).hide()
        } else {
          $(this).show()
        }
      })
    })
    $styledSelect.click(function (e) {
      e.stopPropagation();
      const { currentTarget } = e;

      if ($(currentTarget).hasClass("active")) {
        $(currentTarget).removeClass("active").next("ul.select-options").hide();
      } else {
        $("div.select-styled").each(function () {
          if ($(this).is($(currentTarget))) {
            $(this).addClass("active").next("ul.select-options").toggle();
          } else {
            $(this).removeClass("active").next("ul.select-options").hide();
          }
        });
      }
    });

    $(document).click(function () {
      $styledSelect.removeClass("active");
      $list.hide();
    });
  });

  $(document).on('click', '.select li', function (e) {
    e.stopPropagation();
    let $list = $(this).closest('ul')
    let $styledSelect = $(this).closest('.select').find('.select-styled')
    let that = $(this).closest('.select').find('select')

    $styledSelect.children("div").css("background", $(this).data().color);
    $styledSelect.children("div").text($(this).text());
    $styledSelect.attr('data-prev-text', $(this).text())
    $styledSelect.find('input').val($(this).text().trim())
    $styledSelect.removeClass("active");
    that.val($(this).attr("rel"));
    that.trigger("change");
    $list.hide();
  });

  body.resize(function() {
    let container_top_padding = slider.height();
    slider.next()
        .find('.container')
        .css({
          'padding-top': container_top_padding
        });
  });
}

$(document).ready(function() {
  onDocumentReady();
});
