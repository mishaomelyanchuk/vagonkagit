$(document).ready(function () {
  const navigation = $(".account__pages");
  const navigationToggle = navigation.find(".account__pages-toggle");
  const bussinesAccountToggle = $("#iscommercial");
  const bussinesAccountFields = $(".section-company__fields");
  const ordersList = $(".account__order");
  const orderToggle = ordersList.find(".account__order-toggle");
  const inputs = $(".form__input input");

  ordersList.each(function () {
    if ($(this).hasClass("active")) {
      setContetStatus($(this), "show");
    }
  });

  inputs.each(function () {
    if ($(this).val().length > 0) {
      $(this).siblings(".input-filled__name").addClass("dirty");
    }
  });
  inputs.keyup(function () {
    if ($(this).val().length > 0) {
      $(this).siblings(".input-filled__name").addClass("dirty");
    } else {
      $(this).siblings(".input-filled__name").removeClass("dirty");
    }
  });
  bussinesAccountToggle.change(function () {
    if ($(this).is(":checked")) {
      bussinesAccountFields.addClass("show-grid");
    } else {
      bussinesAccountFields.removeClass("show-grid");
    }
  });
  navigationToggle.click(function () {
    navigation.toggleClass("show-navigation");
  });

  orderToggle.click(function () {
    const blockItem = $(this).parents(".account__order");
    if (blockItem.hasClass("active")) {
      blockItem.removeClass("active");
      setContetStatus(blockItem, "hide");
    } else {
      ordersList.each(function () {
        if ($(this).hasClass("active")) {
          $(this).removeClass("active");
          setContetStatus($(this), "hide");
        }
      });
      blockItem.addClass("active");
      setContetStatus(blockItem, "show");
    }
  });

  function setContetStatus(selector, type) {
    const name = selector.find(".account__order-info p");
    const adress = selector.find(".account__order-info span");
    const products = selector.find(".account__order-wrapper");
    const payText = selector.find(".account__order-pay>*:not(:first-child)");

    if (type === "show") {
      [name, adress, products, payText].forEach((item) => item.slideDown(300).css('display', 'block'));
    } else {
      [name, adress, payText].forEach((item) => item.hide());
      products.slideUp(300);
    }
  }
});
