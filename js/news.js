$(document).ready(function () {
  //not links leading to file
  const newsLink = $(".news__list-wrapper[data-link]");
  newsLink.click(function () {
    const { link } = $(this).data();

    if (link) {
      location.href = link;
    }
  });
});
