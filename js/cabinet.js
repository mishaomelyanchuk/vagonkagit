var Cabinet = function($) {

    this.init = function() {
        if($('#iscommercial').is(':checked') === false) {
            $('.section-company__fields').hide();
        }

        $('#iscommercial').click(function() {
            if($(this).is(':checked') === true) {
                $('.section-company__fields').show();

                $('#companyname').show();
                $('#companycode').show();
                $('#representative').show();
            } else {
                $('.section-company__fields').hide();
                $('#companyname').hide();
                $('#companycode').hide();
                $('#representative').hide();
            }
        });

        $('#fname').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#name').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#email').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#phone').focus(function() {
            $(this).removeClass('invalid');
        });

        $('#newpassword').focus(function() {
            $(this).removeClass('invalid');
            $('#newpassword1').removeClass('invalid');
        });

        $('#newpassword1').focus(function() {
            $(this).removeClass('invalid');
            $('#newpassword').removeClass('invalid');
        });


    }

    this.checkForm = function() {
        var valid = true;
        var fname = $('#fname');
        var name = $('#name');
        var email = $('#email');
        var phone = $('#phone');
        var pass = $('#newpassword');
        var pass1 = $('#newpassword1');

        if($(fname).val() === '') {
            valid = false;
            $(fname).addClass('invalid');
        }

        if($(name).val() === '') {
            valid = false;
            $(name).addClass('invalid');
        }

        if($(email).val() === '') {
            valid = false;
            $(email).addClass('invalid');
        }

        if($(phone).val() === '') {
            valid = false;
            $(phone).addClass('invalid');
        }

        if($(pass).val() !== '' && $(pass).val() !== $(pass1).val()) {
            valid = false;
            $(pass).addClass('invalid');
            $(pass1).addClass('invalid');
        }

        return valid;
    }

    this.postRead = function(post_id) {
        var admin_url = $('#admin-ajax-url').text();
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'reading_post',
                post_id: post_id,
            }
        });
    }
}

var cabinet = new Cabinet(jQuery);

jQuery(function() {
    cabinet.init();
});
