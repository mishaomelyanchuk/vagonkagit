function initFilters() {
  let form = $('#filters')
  let container = $('.catalog__items')
  let selects = form.find('select')
  let canContinue = false;

  function checkForm() {
    selects.each(function () {
      if($(this).val())
        canContinue = true
    })
    return canContinue;
  }

  function updateCatalog(products) {
    container.empty();
  
    if(products) {
      container.append(products)
    }

    canContinue = false;
  }

  function sendRequest(e) {
    e.preventDefault()
    console.log(checkForm())
    if(!checkForm) return false;
    $("#form-reset").show();
    let formData = {
      action: 'catalog_filters'
    }

    selects.each(function () {
      formData[$(this).attr('name')] = $(this).val()
    })

    $.ajax({
      url: '/wp-admin/admin-ajax.php',
      method: 'post',
      data: formData,
      success: function (response) {
        updateCatalog(response)
      }
    })
  }


  form.on('submit', sendRequest)
}

function resetFilters() {
  let container = $('.catalog__items')

  window.history.replaceState({}, document.title, document.location.origin + document.location.pathname);

  $.ajax({
    url: '/wp-admin/admin-ajax.php',
    method: 'post',
    data: {
      action: 'form_reset'
    },
    success: function (response) {
      container.empty();
      container.append(response)
    }
  })

}

$(document).ready(function () {
  const toggle = ".catalog__filters-toggle";
  const filters = $(".catalog__filters-wrapper");
  const form = $("#filters");
  const formResetBtn = $("#form-reset");
  const selects = $("select");

  const product = ".catalog-item";
  const colors = $(".catalog-item__color ul li");
  const colorsVideo = $(".catalog-item__video video");

  initFilters();

  $(document).on('click', toggle, function () {
    $(toggle).hide();
    filters.show();
  });
  formResetBtn.on('click',function () {
    form[0].reset();
    $(".select-options").each(function () {
      $(this).children("li").first()[0].click();
    });
    $(this).hide();

    resetFilters()
  });

  //show color video on hover
  $(document).on('mousemove', product, function (e) {
    const { target } = e;
    if (target.nodeName == "LI") {
      const { color } = target.dataset;
      const videos = $(this).find("video");

      videos.each(function () {
        const { colorVideo } = $(this).data();
        if(colorVideo === color){
          $(this).show()[0].play()
        } else {
          $(this).hide()[0].pause()
          this.currentTime = 0
        }

      });
    } else {
      $(this).find("video").hide();
    }
  });
  //redirect to product
  $(document).on('click', product, function (e) {
    var path =
        e.originalEvent.path ||
        (e.originalEvent.composedPath && e.originalEvent.composedPath());

    for (let i = 0; i < path.length; i++) {
      if (path[i].nodeName === "PICTURE") {
        const { productPath } = $(this).data();
        const { origin } = window.location;

        window.location.href = `${productPath}`;
        return;
      }
    }
  });


});