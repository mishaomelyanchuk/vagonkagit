var Checkout = function($) {

    this.deliveryCost = 0;

    this.deliveryAddress = '';

    this.promoAmount = 0;

    this.promoType = '';

    this.checkInputs = function (inputToCheck = false) {
        let inputsToCheck = $('.order__delivery-descriptions').find('input[required]:not(.not-required), select[required]:not(.not-required)')

        if(inputToCheck)
            inputsToCheck = inputToCheck

        inputsToCheck.each(function () {
            if($(this).closest('.select.not-required').length) return;

            if(this.checkValidity()){
                $(this).removeClass('is-error')
                if ($(this).is('select')) {
                    $(this).closest('.select').removeClass('is-error')
                }
            } else {
                $(this).addClass('is-error')
                if ($(this).is('select')) {
                    $(this).closest('.select').addClass('is-error')
                }
            }
        })
    }

    this.init = function() {
        var _this = this;
        $('[name=delivery-payment]').change(function() {
            $('#payment_method_id').val($('[name=delivery-payment]:checked').val());
        });

        $('[name=delivery]').change(function() {
            _this.calcTotalOrder();

            let delType = $('[name="delivery"]:checked').attr('delivery_type')
            let selectedType = $('.order__delivery-descriptions_item[data-delivery="'+delType+'"]')
            let elseTypes = $('.order__delivery-descriptions_item').not(selectedType)


            elseTypes.find('input:not(.not-required), select:not(.not-required)').removeAttr('required')
            selectedType.find('input:not(.not-required), select:not(.not-required)').attr('required', 'required')
        });

        $(document).on('change', '#novaposhta_region', function () {
            if(!$(this).val()){
                $('#novaposhta_city').val('').closest('.select').addClass('is-error')
                $('#novaposhta_houses').val('').closest('.select').addClass('is-error')
            }
        })

        $('.order__delivery-descriptions').find('input, select').on('change input', function () {
            _this.checkInputs($(this))
        })
    }

    this.onSubmit = function() {
        const isValid = $('.order__delivery-descriptions')[0].reportValidity();
        let isLoginValid = true

        if (!isValid) {
            this.checkInputs()
        } else {
            $('.order__delivery-descriptions').find('input[required]:not(.not-required), .select:not(.not-required)').removeClass('is-error')
        }

        if(isValid) {
            var orderTotal = Number($('#order_total').val());
            var promo_code = $("#promo_el").val();

            promo_code = promo_code.toUpperCase();
            $('#order_total').val(orderTotal + this.deliveryCost);
            this.setDeliveryAddress();
            $('#delivery_address_id').val(this.deliveryAddress);
            $('#shipping_cost_id').val(this.deliveryCost);
            $('#promo_code_id').val(promo_code);
        }

        if($('#order__client-id').val() <= 0) {
            if($('.order__client-fields.active.login-client').length){

                $('[name="customer-login"]').addClass('is-error')
                $('[name="customer-password"]').addClass('is-error')

                isLoginValid = false;
            } else {
                isLoginValid = registration.checkForm() && $('#order__client-id').val() > 0;
            }
        }

        return isLoginValid && isValid;
    }

    this.setDeliveryAddress = function() {
        let currentLang = $('.order').data('lang');
        console.log(currentLang);
        var deliveryType = $('[name=delivery]:checked').attr('delivery_type');
        $('#delivery_id').val(deliveryType);
        switch (deliveryType) {
            case 'self':
                if(!$('#delivery_self_id').val()) return false;
                if ( currentLang === 'uk' ) {
                    this.deliveryAddress = 'Самовивіз: ' + $('#delivery_self_id').val();
                } else {
                    this.deliveryAddress = 'Самовывоз: ' + $('#delivery_self_id').val();
                }
                break;
            case 'courier':
                if(!$('#courier_address_id').val()) return false;
                if ( currentLang === 'uk' ) {
                    this.deliveryAddress = 'Доставка кур`єром: ' + $('#courier_address_id').val();
                } else {
                    this.deliveryAddress = 'Доставка куръером: ' + $('#courier_address_id').val();
                }
                break;
            case 'novaposhta':
                if(!$('#novaposhta_region').val()) return false;
                if ( currentLang === 'uk' ) {
                    this.deliveryAddress = 'Нова Пошта: ' + $('#novaposhta_region').find('option:selected').html() + ", " + $('#novaposhta_city').find('option:selected').html() + ", " + $('#novaposhta_houses').find('option:selected').html();
                } else {
                    this.deliveryAddress = 'Новая почта: ' + $('#novaposhta_region').find('option:selected').html() + ", " + $('#novaposhta_city').find('option:selected').html() + ", " + $('#novaposhta_houses').find('option:selected').html();
                }
                break;
        }
    }

    this.setPromoCode = function() {
        var _this = this;
        var admin_url = $('#admin-ajax-url').text();
        var promo_code = $("#promo_el").val();
        let currentLang = $('.order').data('lang');
        promo_code = promo_code.toUpperCase();
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'promo_code',
                promo_code: promo_code
            },
            success: function(result) {
                if(result.res === 0) {
                    if(result.promo.discount_type === 'fixed_cart') {
                        _this.promoAmount = Number(result.promo.coupon_amount);
                        _this.promoType = result.promo.discount_type;
                        _this.calcTotalOrder();
                        $('#promo_code_id').val(promo_code);
                        $('#promo_description').html(result.promo.coupon_description);
                        $('#promo_amount').html("-" + result.promo.coupon_amount + ' <span class="currency">₴</span>');
                        $('#discount_wrapper_id').show();
                    } else if(result.promo.discount_type === 'percent') {
                        _this.promoAmount = Number(result.promo.coupon_amount);
                        _this.promoType = result.promo.discount_type;
                        _this.calcTotalOrder();
                        $('#promo_code_id').val(promo_code);
                        $('#promo_description').html(result.promo.coupon_description);
                        $('#promo_amount').html("-" + result.promo.coupon_amount + ' <span class="currency">%</span>');
                        $('#discount_wrapper_id').show();
                    }
                } else {
                    _this.promoAmount = 0;
                    _this.promoType = '';
                    $('#promo_code_id').val('');
                    if ( currentLang === 'uk' ) {
                        $('#promo_description').html('Промокод не знайдено');
                    } else {
                        $('#promo_description').html('Промокод не найден');
                    }
                    $('#promo_amount').html('');
                    $('#discount_wrapper_id').show();
                }
            }
        });
    }

    this.calcTotalOrder = function() {
        _this = this;
        let currentLang = $('.order').data('lang');
        var orderTotal = Number($('#order_total').val());
        if ( $('[name=delivery]:checked').val() === 'novaposhta' ) {
            _this.deliveryCost = 0;
        } else {
            _this.deliveryCost = Number($('[name=delivery]:checked').val());
        }
      
        var totalAmount = orderTotal + _this.deliveryCost;
        if(_this.promoAmount > 0) {
            if(_this.promoType === 'fixed_cart') {
                totalAmount = totalAmount - _this.promoAmount;
            } else {
                totalAmount = Math.ceil(totalAmount - (totalAmount * (_this.promoAmount / 100)));
            }
        }

    
        totalAmount = totalAmount.toString();
        totalAmount = totalAmount.split('.');
       
      
        if ( !totalAmount[1] ) {
            totalAmount[1] = '00';
        }

        if ( totalAmount[1].length == 1 ) {
            totalAmount[1] = totalAmount[1] + '0';
        }

        if ( totalAmount[1].length == 2 && totalAmount[1].length > 2 ) {
            totalAmount[1] = totalAmount[1];
        }


        totalAmount = Number(totalAmount.join('.'));
        totalAmount = totalAmount.toFixed(2);

        if(Number($('[name=delivery]:checked').val()) > 0) {

            $('#delivery_cost_id').html(_this.deliveryCost + '.00 ₴');
            $('#total_price_id').html(totalAmount);
        } else if($('[name=delivery]:checked').val() === 'novaposhta' ) {
            if ( currentLang === "uk" ) {
                $('#delivery_cost_id').html('У відділенні');
            } else {
                $('#delivery_cost_id').html('В отделении');
            }
            $('#total_price_id').html(totalAmount);
            _this.deliveryCost = 0;
        } else {
            if ( currentLang ==='uk' ) {
                $('#delivery_cost_id').html('безкоштовно');
            } else {
                $('#delivery_cost_id').html('бесплатно');
            }
            $('#total_price_id').html(totalAmount);
            _this.deliveryCost = 0;
        }
    }

}

var checkout = new Checkout(jQuery);

jQuery(function() {
    checkout.init();
});