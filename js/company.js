$(document).ready(function () {
  const historyToggle = $(".company-history__button");
  const historyHiddenText = $(".company-history__text.hidden-text");
  const sliderWrapper = $(".company-slider__wrapper");
  const dragSlider = $(".drag-slider__wrapper");
  const dragSliderImages = $(".company-images__wrapper ul li");
  const sliderOptions = {
    arrows: false,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 0,
    speed: 6000,
    cssEase: "linear",
    pauseOnHover: false,
    rows: 0,
    responsive: [
      {
        breakpoint: 1440,
        settings: {
          slidesToShow: 3,
          centralMode: true,
          infinite: true,
        },
      },
      {
        breakpoint: 1100,
        settings: {
          slidesToShow: 1,
          centerMode: true,
          autoplaySpeed: 0,
          speed: 3000,
          cssEase: "cubic-bezier(.15,1.3,.15,.91)",
          variableWidth: true,
          swipe: true,
          pauseOnHover: false,
        },
      },
    ],
  };

  historyToggle.click(function () {
    if (historyHiddenText.hasClass("hidden")) {
      historyHiddenText.slideDown(300);
    } else {
      historyHiddenText.slideUp(300);
    }
    historyHiddenText.toggleClass("hidden");
    $(this).toggleClass("open");
  });

  sliderWrapper.slick(sliderOptions);

  dragSlider.find("li").each(function (index) {
    const colorsLength = dragSlider.find("li").length;
    const rotateIndex = 360 / colorsLength;
    const rotateStart = 75;
    const rotateAngle = rotateStart + index * rotateIndex;
    const skew = 75;
    const transformStr = "rotate(" + rotateAngle + "deg) skew(" + skew + "deg)";

    $(this).css({ transform: transformStr });
    $(this).data({
      angle: (6.2832 / colorsLength) * index,
      index,
    });
  });
  dragSliderImages.each(function (index) {
    if (index === 0) $(this).show();
    $(this).data({
      index,
    });
  });
  dragSlider[0].ontouchstart = (e) => {
    e.preventDefault();
  };
  dragSlider[0].ontouchmove = (e) => {
    e.preventDefault();
  };
  dragSlider[0].ontouchend = (e) => {
    e.preventDefault();
  };
  interact(dragSlider[0]).draggable({
    onstart: function (event) {
      const element = event.target;
      const rect = element.getBoundingClientRect();

      element.dataset.centerX = rect.left + rect.width / 2;
      element.dataset.centerY = rect.top + rect.height / 2;

      element.dataset.angle = getDragAngle(event);
    },
    onmove: function (event) {
      var element = event.target;
      const radians = 6.2832;
      const itemsLength = dragSlider.find("li").length;
      const step = radians / itemsLength;

      var center = {
        x: parseFloat(element.dataset.centerX) || 0,
        y: parseFloat(element.dataset.centerY) || 0,
      };

      var angle = getDragAngle(event);
      const fullTurns = Math.abs(Math.trunc(angle / radians));
      const angleCorrection = angle - fullTurns * radians;
      let activeItem = 0;

      if(angleCorrection >= 0) {
        activeItem = itemsLength - Math.abs(Math.trunc(angleCorrection / step))
        if(activeItem == itemsLength)
          activeItem = 0
      } else {
        activeItem = (0 - Math.trunc(angleCorrection / step)) + 1
      }


// console.log(angleCorrection, Math.trunc(angleCorrection / step),  0 - Math.trunc(angleCorrection / step), 'act:'+activeItem)
      dragSliderImages.each(function (index) {
        if (index === activeItem) {
          $(this).show();
        } else {
          $(this).hide();
        }
      });

      element.style.transform = "rotate(" + angle + "rad" + ")";
    },
    onend: function (event) {
      const element = event.target;

      element.dataset.angle = getDragAngle(event);
    },
  });
  if ($(window).width() >= 1100) {
    sliderWrapper.find(".slick-track").on("mouseenter", hoverIn);
    sliderWrapper.find(".slick-track").on("mouseleave", hoverOut);
  }
  $(window).resize(function () {
    if ($(window).width() >= 1100) {
      sliderWrapper.find(".slick-track").off("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").off("mouseleave", hoverOut);
      sliderWrapper.find(".slick-track").on("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").on("mouseleave", hoverOut);
    } else {
      sliderWrapper.find(".slick-track").off("mouseenter", hoverIn);
      sliderWrapper.find(".slick-track").off("mouseleave", hoverOut);
    }
  });

  function getDragAngle(event) {
    var element = event.target;
    var startAngle = parseFloat(element.dataset.angle) || 0;
    var center = {
      x: parseFloat(element.dataset.centerX) || 0,
      y: parseFloat(element.dataset.centerY) || 0,
    };
    var angle = Math.atan2(center.y - event.clientY, center.x - event.clientX);

    return angle - startAngle;
  }
  function hoverIn() {
    sliderWrapper.slick("customStop");
  }
  function hoverOut() {
    sliderWrapper.slick("customResume");
  }
});
