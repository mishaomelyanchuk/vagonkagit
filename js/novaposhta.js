var Novaposhta = function($) {

    this.init = function() {
        var _this = this;
        $('#novaposhta_region').change(function() {
            _this.fillCities($(this).val());
            $('#novaposhta_city').next().find('div').html('Выберите город');
            $('#novaposhta_houses').next().find('div').html('Выберите отделение');
        });
    }

    this.fillCities = function(region_id) {
        var _this = this;
        var admin_url = $('#admin-ajax-url').text();
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'novaposhta_cities',
                region_id: region_id,
            },
            success: function(res) {
                _this.setCities(res.cities);
            }
        });
    }

    this.setCities = function(cities) {
        var _this= this;
        var select = $('#novaposhta_city');
        var select_list = $('#novaposhta_city_list');
        $(select_list).find('li').remove();
        $.each(cities, function(index, city) {
          $('#novaposhta_city').append('<option value="' + city.ref + '">' + city.description + '</option>');
          $(select_list).append('<li data-color="transparent" data-rel="' + city.description + '" rel="' + city.ref + '"><div style="background: transparent;">' + city.description + '</div></li>');
        });

        $(select_list).find('li').click(function() {
            var value = $(this).attr("rel");
            $(select).val(value);
            $(select).next().find('div').html($(this).attr('data-rel'));
            _this.fillHouses();
            $('#novaposhta_houses').next().find('div').html('Выберите отделение');
        });
    }

    this.fillHouses = function() {
        var _this = this;
        var admin_url = $('#admin-ajax-url').text();
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'novaposhta_houses',
                city_id: $('#novaposhta_city').val(),
            },
            success: function(res) {
                _this.setHouses(res.houses);
            }
        });
    }

    this.setHouses = function(houses) {
        var _this= this;
        var select = $('#novaposhta_houses');
        var select_list = $('#novaposhta_houses_list');
        $(select_list).find('li').remove();
        $.each(houses, function(index, house) {
            $(select).append('<option value="' + house.ref + '">' + house.description + '</option>');
            $(select_list).append('<li data-color="transparent" data-rel="' + house.description + '" rel="' + house.ref + '"><div style="background: transparent;">' + house.description + '</div></li>');
        });

        $(select_list).find('li').click(function() {
            var value = $(this).attr("rel");
            $(select).val(value);
            $(select).next().find('div').html($(this).attr('data-rel'));
        });
    }

}

var novaposhta = new Novaposhta(jQuery);

jQuery(function() {
    novaposhta.init();
});