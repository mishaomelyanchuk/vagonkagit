var LiqPayFront = function($) {

    this.init = function() {

    }

    this.pay = function() {
        var _this = this;
        var admin_url = $('#admin-ajax-url').text();
        $.ajax({
            url: admin_url,
            type: 'post',
            dataType: 'json',
            data: {
                action: 'liq_pay',
                amount: $('#order_total').val()
            },
            success: function(result) {
                if(typeof result.res !== "undefined" && result.res === 0) {
                    window.location.href = result.link;
                }
            }
        });
    }

}

var liqPayFront = new LiqPayFront(jQuery);

jQuery(function() {
    liqPayFront.init();
});