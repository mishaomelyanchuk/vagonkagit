function initMap(){
  let map = new google.maps.Map(document.getElementById("map"), {
    styles: [
      {
        featureType: "administrative",
        elementType: "labels",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "administrative.country",
        elementType: "geometry.stroke",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "administrative.province",
        elementType: "geometry.stroke",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "landscape",
        elementType: "geometry",
        stylers: [
          {
            visibility: "on",
          },
          {
            color: "#F5F5F5",
          },
        ],
      },
      {
        featureType: "landscape.natural",
        elementType: "labels",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "poi",
        elementType: "all",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "all",
        stylers: [
          {
            color: "#C9C9C9",
          },
        ],
      },
      {
        featureType: "road",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit",
        elementType: "labels.icon",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.line",
        elementType: "geometry",
        stylers: [
          {
            visibility: "on",
          },
        ],
      },
      {
        featureType: "transit.line",
        elementType: "labels.text",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.station.airport",
        elementType: "geometry",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "transit.station.airport",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "geometry",
        stylers: [
          {
            color: "#C9C9C9",
          },
        ],
      },
      {
        featureType: "water",
        elementType: "labels",
        stylers: [
          {
            visibility: "off",
          },
        ],
      },
    ],
  });
  let minZoom = 13;
  let markers = []
  let dropdowns = $('.offices__search-dropdowns select')
  let featuresWrapper = $('.offices__result-wrapper')

  function createMarkers(data) {
    let bounds = new google.maps.LatLngBounds();

    $.each(data, function (i, thisFeature) {
      var info = thisFeature.addresses.length > 0 ? (thisFeature.name + "\r\n" + thisFeature.addresses.join("\r\n")) : thisFeature.name;
      const marker = new google.maps.Marker({
        position: thisFeature.position,
        icon: icons[thisFeature.type.id],
        map: map,
        title: info,
      });

      markers.push(marker)
      let featureTemplate = $(`
        <li class="offices__result-item">
          <span class="offices__result-type">${thisFeature.type.text}</span>
          <h2 class="offices__result-name">${thisFeature.name}</h2>
        </li>`);

      if(thisFeature.addresses.length){
        let addressesTemplate = $('<ul class="offices__result-place"></ul>')

        $.each(thisFeature.addresses, function (_, address) {
          addressesTemplate.append('<li>'+address+'</li>')
        })
        featureTemplate.append(addressesTemplate)
      }

      if(thisFeature.phones.length){
        let phonesTemplate = $('<ul class="offices__result-phones"></ul>')

        $.each(thisFeature.phones, function (_, phone) {
          phonesTemplate.append(`<li><a href="${phone.link}">${phone.number}</a>${phone.text ? '<span>'+phone.text+'</span>' : ''}</li>`)
        })
        featureTemplate.append(phonesTemplate)
      }

      if(thisFeature.links.length){
        let linksTemplate = $('<ul class="offices__result-links"></ul>')

        $.each(thisFeature.links, function (_, link) {
          linksTemplate.append(`<li><a href="${link.link}" target="_blank">${link.text}</a></li>`)
        })
        featureTemplate.append(linksTemplate)
      }

      featuresWrapper.append(featureTemplate)

      bounds.extend(thisFeature.position);
    })

    map.fitBounds(bounds);

    if(map.getZoom() > minZoom)
      map.setZoom(minZoom)
  }

  function deleteMarkers(){
    for (let i = 0; i < markers.length; i++) {
      markers[i].setMap(null);
    }
    featuresWrapper.empty();
  }

  function filterMap() {
    let areaSelect = $('select[name=area]')
    let typeSelect = $('select[name=type]')
    let areaOpts = areaSelect.parent().find('li:not([rel=""])')
    let typeBaseOpt = $('.offices__search-select li[rel="base"]')
    let typeSmallOpt = $('.offices__search-select li[rel="small"]')
    let area = areaSelect.val()
    let type = typeSelect.val()
    let result = '';

    typeBaseOpt.show()
    typeSmallOpt.show()
    areaOpts.show()

    if(area){
      result = features.filter(feature => feature.area.id == area)
      let isHaveBase = !!result.find(type => type.type.id === 'base');
      let isHaveSmall = !!result.find(type => type.type.id === 'small');

      if(!isHaveBase)
        typeBaseOpt.hide()

      if(!isHaveSmall)
        typeSmallOpt.hide()

    }
    if(type){
      if(result) {
        result = result.filter(feature => feature.type.id == type)
      } else {
        result = unique_features.filter(feature => feature.type.id == type)
      }

      areaOpts.each(function () {
        let thisArea = $(this);
        let thisAreaShops = features.filter(feature => feature.area.id == thisArea.attr('rel'))

        if(!thisAreaShops.find(feature => feature.type.id == type))
          thisArea.hide()

      })
    }

    deleteMarkers()

    if(result.length){
      createMarkers(result)
    }
    if(!area && !type){
      createMarkers(unique_features)
    }
  }

  function resizeMap() {
    const { clientWidth } = document.querySelector("body");
    const blocksGap = 25;
    const containerPaddings = 120;
    const maxContainerWidth = 1600;
    const insideMapWidth =
        ((Math.min(clientWidth, maxContainerWidth) - containerPaddings) / 3) * 2 -
        blocksGap;

    const outWidth = Math.max(0, (clientWidth - maxContainerWidth) / 2);
    const setWidth = outWidth + insideMapWidth + containerPaddings / 2;

    document.querySelector("#map").style.width = setWidth + "px";
  }

  resizeMap();
  window.onresize = resizeMap;
  createMarkers(unique_features)

  dropdowns.on('change', filterMap)
}

$(document).ready(function () {
  initMap()
})