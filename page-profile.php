<?php
/*
 * Template name: Profile
 */
?>
<?php
ob_start();
require_once get_template_directory() . "/classes/vg_user.php";

/** @var $VG_User VG_user */
global $VG_user;
$VG_user = new VG_User();
ob_end_clean();

if(!$VG_user->is_auth()) {
    ob_end_flush();
    ob_clean();
    wp_redirect("/");
    exit;
}

$form_header  = new Page_Profile_Account_Header();
$account_form = new Page_Profile_Account_Form();
?>
<?php get_header(); ?>
<div class="account">
    <div class="container">
        <section class="secton__outer">
            <section class="section__inner">
                <div class="account__wrapper">
                    <?php $form_header->render(); ?>
                    <?php $account_form->render(); ?>
                </div>
            </section>
        </section>
    </div>
</div>
<?php get_footer(); ?>
