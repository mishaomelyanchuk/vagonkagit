<!DOCTYPE html>
<!-- saved from url=(0046)https://dneprvagonka-demo.web.app/article.html -->
<html lang="ru"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <title>LL</title>
    <link rel="stylesheet" href="./article_files/style.css">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=1">
  </head>
  <body>
    <div class="header">
 
  <div class="container">
    <section class="section__outer">
      <section class="section__inner">
        <header class="header__wrapper">
          <picture class="header__logo">
            <a href="https://dneprvagonka-demo.web.app/">
              <img src="./article_files/Logo.svg" alt="">
            </a>
          </picture>
          <nav class="header__nav" aria-label="primary navigaion">
            <ul>
              <li><a href="https://dneprvagonka-demo.web.app/about.html">О компании</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/news.html">Статьи</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/partners.html">Партнерам</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/offices.html">Представительства</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/contacts.html">Контакты</a></li>
            </ul>
          </nav>
          <div class="header__contacts">
            <ul>
              <li><a href="tel:+380963956985">+38 (096) 395-69-85</a></li>
              <li><a href="tel:+380503421580">+38 (050) 342-15-80</a></li>
            </ul>
          </div>
          <div class="header__socials">
            <ul>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
                  <svg width="24" height="18">
                    <use href="../static/svg-youtube.svg#youtube"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
                  <svg width="18" height="18">
                    <use href="../static/svg-meta.svg#meta"></use>
                  </svg>
                </a>
              </li>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
                  <svg width="18" height="18">
                    <use href="../static/svg-instagram.svg#instagram"></use>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
          <div class="header__lang">
            <ul>
              <li class="header__lang-item active">
                <span>ru</span>
                <i>
                  <svg width="16" height="15">
                    <use href="../static/svg-lang.svg#lang"></use>
                  </svg>
                </i>
              </li>
            </ul>
          </div>
          <div class="header__actions">
            <ul>
              <li>
                <a data-link="catalog" href="https://dneprvagonka-demo.web.app/catalog.html">
                  <i>
                    <svg width="14" height="15">
                      <use href="../static/svg-catalog.svg#catalog"></use>
                    </svg>
                  </i>
                  <span>Каталог продукции</span>
                </a>
                <section class="header__actions-popup popup-catalog">
                  <div class="catalog-popup">
  <div class="catalog-popup__wrapper">
    <div class="catalog-popup__side">
      <h4>в нашем каталоге продукции</h4>
      <p>56 цветов, 6 красок <br>5 растворителей для чего угодно</p>
      <a href="https://dneprvagonka-demo.web.app/catalog.html" class="btn btn-colored">Весь каталог</a>
    </div>
    <ol class="catalog-popup__products">
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
      <li>
        <a href="https://dneprvagonka-demo.web.app/product.html">
          <picture><img src="./article_files/product-test.png" alt=""></picture>
          <p>Эмаль ПФ-133 «Дніпровська вагонка»</p>
          <div class="catalog-popup__discount">
            <span class="catalog-popup__discount-text">скидка 5%</span>
          </div>
        </a>
      </li>
    </ol>
  </div>
</div>

                </section>
              </li>
              <li>
                <a data-link="cart" href="https://dneprvagonka-demo.web.app/cart.html">
                  <i>
                    <svg width="25" height="16">
                      <use href="../static/svg-cart.svg#cart"></use>
                    </svg>
                  </i>
                  <span>Корзина</span>
                  <div class="cart-count">
                    <p>3</p>
                  </div>
                </a>
                <section class="header__actions-popup popup-cart">
                  <div class="cart-popup">
  <div class="cart-popup__wrapper">
    <ul class="cart-popup__list">
      <li class="cart-popup__product">
        <picture>
          <img src="./article_files/product-test.png" alt="">
        </picture>
        <div class="cart-popup__product-info">
          <div class="cart-popup__product-title">
            <p>Эмаль швидкосохнуча по металлу</p>
          </div>
          <div class="cart-popup__product-details">
            <p>
              2,5 л
              <span>ЧернЫй 9005</span>
              <span>3 шт</span>
            </p>
          </div>
        </div>
        <div class="cart-popup__product-total">
          <p>1 643<span>.00</span><span class="currency"> ₴</span></p>
        </div>
      </li>
      <li class="cart-popup__product">
        <picture>
          <img src="./article_files/product-test.png" alt="">
        </picture>
        <div class="cart-popup__product-info">
          <div class="cart-popup__product-title">
            <p>Эмаль швидкосохнуча по металлу</p>
          </div>
          <div class="cart-popup__product-details">
            <p>
              2,5 л
              <span>ЧернЫй 9005</span>
              <span>3 шт</span>
            </p>
          </div>
        </div>
        <div class="cart-popup__product-total">
          <p>1 643<span>.00</span><span class="currency"> ₴</span></p>
        </div>
      </li>
      <li class="cart-popup__product">
        <picture>
          <img src="./article_files/product-test.png" alt="">
        </picture>
        <div class="cart-popup__product-info">
          <div class="cart-popup__product-title">
            <p>Эмаль швидкосохнуча по металлу</p>
          </div>
          <div class="cart-popup__product-details">
            <p>
              2,5 л
              <span>ЧернЫй 9005</span>
              <span>3 шт</span>
            </p>
          </div>
        </div>
        <div class="cart-popup__product-total">
          <p>1 643<span>.00</span><span class="currency"> ₴</span></p>
        </div>
      </li>
    </ul>
    <div class="cart-popup__bottom">
      <div class="cart-popup__link">
        <a class="btn btn-colored" href="https://dneprvagonka-demo.web.app/cart.html">К заказу</a>
      </div>
      <div class="cart-popup__total">
        <span class="cart-popup__total-text">на сумму</span>
        <p class="cart-popup__total-price">
          2 822<span>.00</span><span class="currency">₴</span>
        </p>
      </div>
    </div>
  </div>
</div>

                </section>
              </li>
              <li>
                <a data-link="login" href="https://dneprvagonka-demo.web.app/login.html">
                  <i>
                    <svg width="12" height="17">
                      <use class="svg-catalog" href="../static/svg-singin.svg#singin"></use>
                    </svg>
                  </i>
                  <span>Вход</span>
                </a>
                <section class="header__actions-popup popup-login">
                  <div class="login__form">
  <form class="form">
    <div class="form__section section-company">
      <div class="form__input input-required">
        <input name="login" type="text" placeholder="Логин">
      </div>

      <div class="form__input input-required">
        <input type="password" placeholder="пароль" required="">
      </div>
    </div>
    <div class="form__submit">
      <button class="btn btn-colored">Войти</button>
    </div>
  </form>
  <ul class="login__form-links">
    <li><a href="https://dneprvagonka-demo.web.app/article.html#">Забыли пароль?</a></li>
    <li><a href="https://dneprvagonka-demo.web.app/signup.html">Регистрация</a></li>
  </ul>
</div>

                </section>
              </li>
              <li style="display: none">
                <a data-link="account" href="https://dneprvagonka-demo.web.app/account.html">
                  <i>
                    <svg width="17" height="17">
                      <use class="svg-catalog" href="../static/svg-account.svg#account"></use>
                    </svg>
                  </i>
                  <span>Кабинет</span>
                </a>
                <section class="header__actions-popup popup-account">
                  <div class="account-popup">
  <div class="account-popup__wrapper">
    <ul>
      <li><a href="https://dneprvagonka-demo.web.app/account-profile.html">Профиль</a></li>
      <li><a href="https://dneprvagonka-demo.web.app/account-orders.html">Мои заказы</a></li>
      <li>
        <div class="account-count"><span>2</span></div>
        <a href="https://dneprvagonka-demo.web.app/account-materials.html">Материалы</a>
      </li>
    </ul>
    <div class="account-popup__exit">
      <a href="https://dneprvagonka-demo.web.app/article.html#">Выйти</a>
    </div>
  </div>
</div>

                </section>
              </li>
            </ul>
          </div>
          <div class="header__mobile-toggle">
            <ul>
              <li class="open">
                <i>
                  <svg width="14" height="17">
                    <use href="../static/svg-burger.svg#burger"></use>
                  </svg>
                </i>
                <span>Весь сайт</span>
              </li>
              <li class="close">
                <i>
                  <svg width="14" height="17">
                    <use href="../static/svg-burger-close.svg#burger-close"></use>
                  </svg>
                </i>
                <span>Закрыты</span>
              </li>
            </ul>
          </div>
        </header>
      </section>
    </section>
  </div>
  <div class="header__mobile">
    <div class="container">
      <nav class="header__mobile-nav" aria-label="mobile navigaion">
        <ul>
          <li>
            <a href="https://dneprvagonka-demo.web.app/catalog.html">Каталог продукции</a>
            <i>
              <svg width="14" height="15">
                <use href="../static/svg-catalog.svg#catalog"></use>
              </svg>
            </i>
          </li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/about.html">О компании</a></li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/news.html">Статьи</a></li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/partners.html">Партнерам</a></li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/offices.html">Представительства</a></li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/contacts.html">Контакты</a></li>
        </ul>
      </nav>
      <div class="header__mobile-actions">
        <ul>
          <li>
            <a href="https://dneprvagonka-demo.web.app/cart.html">
              <span>Корзина</span>
              <i>
                <svg width="25" height="16">
                  <use href="../static/svg-cart.svg#cart"></use>
                </svg>
              </i>
              <div class="cart-count">
                <p>3</p>
              </div>
            </a>
          </li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/login.html">
              <span>Вход</span>
              <i>
                <svg width="12" height="17">
                  <use class="svg-catalog" href="../static/svg-singin.svg#singin"></use>
                </svg>
              </i>
            </a>
          </li>
        </ul>
      </div>
      <div class="header__mobile-contacts">
        <ul>
          <li><a href="tel:+380963956985">+38 (096) 395-69-85</a></li>
          <li><a href="tel:+380503421580">+38 (050) 342-15-80</a></li>
        </ul>
      </div>
      <div class="header__mobile-socials">
        <ul>
          <li>
            <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
              <span>Facebook</span>
              <i>
                <svg width="18" height="18">
                  <use href="../static/svg-meta.svg#meta"></use>
                </svg>
              </i>
            </a>
          </li>
          <li>
            <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
              <span>YouTube</span>
              <i><svg width="24" height="18">
                  <use href="../static/svg-youtube.svg#youtube"></use>
                </svg>
              </i>
            </a>
          </li>

          <li>
            <a href="https://dneprvagonka-demo.web.app/article.html#" target="_blank">
              <span>Instagram</span>
              <i>
                <svg width="18" height="18">
                  <use href="../static/svg-instagram.svg#instagram"></use>
                </svg>
              </i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>

    <div class="article">
      <div class="container">
        <section class="section__outer">
          <section class="section__inner">
            <div class="article__wrapper">
              <div class="article__title">
                <h1>
                  На рынке появляются новые материалы для борьбы с коррозией
                </h1>
              </div>
              <div class="article__date">
                <div>
                  <p>21</p>
                  <span>ноября</span>
                  <span>2024</span>
                </div>
              </div>
              <picture>
                <img src="./article_files/about.jpg" alt="">
              </picture>
              <div class="article__text">
                <p>
                  В последнее время инновационные разработки в области
                  лакокрасочной продукции были направлены на создание новых
                  уникальных свойств и их сочетаний. Объясняется это тем, что
                  появились новые виды и расширена структура окрашиваемых
                  поверхностей. А также изменился характер запросов
                  потребителей, увеличился ассортимент лакокрасочных материалов.
                </p>
                <p>
                  Как известно, лакокрасочные материалы (ЛКМ) выполняют три
                  основные функции: декоративную, защитную (защищают укрываемую
                  поверхность) и специальную, то есть придают покрытию
                  специальные функции трех отдельных материалов:
                </p>
                <p>
                  свойства. Напомним, грунт-эмаль по ржавчине сочетает в себе
                </p>
                <ul>
                  <li>преобразователя продуктов коррозии</li>
                  <li>грунта с антикоррозионными свойствами</li>
                  <li>финишного декоративного покрытия</li>
                </ul>
                <h2>Рекомендации к применению</h2>
                <p>
                  Одним из основных направлений последних лет в области
                  инноваций является разработка материалов с еще более жесткими
                  требованиями к содержанию опасных веществ (вплоть до полного
                  запрета к применению) при одновременном достижении высоких
                  потребительских свойств ЛКМ.
                </p>
                <p>
                  Получение материалов, например, с минимальным содержанием
                  легколетучих органических соединений, одновременным
                  сохранением необходимых показателей свойств и себестоимости
                  традиционной рецептуры является нетривиальной задачей и
                  требует продолжительной и вдумчивой лабораторной работы.
                </p>
                <p>
                  Напомним, грунт-эмаль по ржавчине сочетает в себе функции трех
                  отдельных материалов:
                </p>
                <ol>
                  <li>преобразователя продуктов коррозии</li>
                  <li>грунта с антикоррозионными свойствами</li>
                  <li>финишного декоративного покрытия</li>
                </ol>
              </div>
              <div class="article__slider">
                <div class="article__slider-arrows">
                  <div class="article__slider-next slick-arrow" style="">
                    <svg width="22" height="63">
                      <use href="../static/svg-article-arrow-left.svg#arrow"></use>
                    </svg>
                  </div>
                  <div class="article__slider-prev slick-arrow" style="">
                    <svg width="22" height="63">
                      <use href="../static/svg-article-arrow-right.svg#arrow"></use>
                    </svg>
                  </div>
                </div>
                <ul class="slick-initialized slick-slider">
                  <div class="slick-list draggable"><div class="slick-track" style="opacity: 1; width: 3894px; transform: translate3d(-1062px, 0px, 0px);"><li class="slick-slide slick-cloned" data-slick-index="-3" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider2.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="-2" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider3.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="-1" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider3.jpg" alt=""></li><li class="slick-slide slick-current slick-active" data-slick-index="0" aria-hidden="false" style="width: 344px;" tabindex="0"><img src="./article_files/article-slider1.jpg" alt=""></li><li class="slick-slide slick-active" data-slick-index="1" aria-hidden="false" style="width: 344px;" tabindex="0"><img src="./article_files/article-slider2.jpg" alt=""></li><li class="slick-slide slick-active" data-slick-index="2" aria-hidden="false" style="width: 344px;" tabindex="0"><img src="./article_files/article-slider3.jpg" alt=""></li><li class="slick-slide" data-slick-index="3" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider3.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="4" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider1.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="5" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider2.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="6" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider3.jpg" alt=""></li><li class="slick-slide slick-cloned" data-slick-index="7" id="" aria-hidden="true" style="width: 344px;" tabindex="-1"><img src="./article_files/article-slider3.jpg" alt=""></li></div></div>
                  
                  
                  
                </ul>
              </div>
              <div class="article__files">
                <ul>
                  <li>
                    <a href="https://dneprvagonka-demo.web.app/article.html#">Техрегламент ЕАЭС на лакокрасочные материалы, DOCX 4,6
                      Mb</a>
                  </li>
                  <li>
                    <a href="https://dneprvagonka-demo.web.app/article.html#">Техрегламент ЕАЭС на лакокрасочные материалы, DOCX 4,6
                      Mb</a>
                  </li>
                </ul>
              </div>
              <div class="article__back-link">
                <a href="https://dneprvagonka-demo.web.app/account-materials.html">Вернуться к списку публикаций</a>
              </div>
            </div>
          </section>
        </section>
      </div>
    </div>
    <footer class="footer">
  <div class="container">
    <section class="section__outer">
      <section class="section__inner">
        <div class="footer__wrapper">
          <div class="footer__logo">
            <img src="./article_files/Logo.svg" alt="">
            <p>Общество с ограниченной ответственностью «Универсал-Инрафарб»</p>
          </div>
          <div class="footer__contacts">
            <ul>
              <li class="footer__contacts-phone">
                <a href="tel:+380963956985">+38 (096) 395-69-85</a>
              </li>
              <li class="footer__contacts-phone">
                <a href="tel:+380503421580">+38 (050) 342-15-80</a>
              </li>
              <li class="footer__contacts-mail">
                <a href="mailto:inrafarb@%20gmail.com">inrafarb@ gmail.com</a>
              </li>
            </ul>
          </div>
          <nav class="footer__nav">
            <ul>
              <li><a href="https://dneprvagonka-demo.web.app/catalog.html">Каталог</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/about.html">О компании</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/news.html">Статьи</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/partners.html">Партнерам</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/offices.html">Представительства</a></li>
              <li><a href="https://dneprvagonka-demo.web.app/contacts.html">Контакты</a></li>
            </ul>
          </nav>
          <div class="footer__politics">
            <ul>
              <li><a href="https://dneprvagonka-demo.web.app/article.html#">Пользовательское соглашение</a></li>
              <li>
                <a href="https://dneprvagonka-demo.web.app/privacypolicy.html">Политика конфиденциальности</a>
              </li>
            </ul>
          </div>
          <div class="footer__permissions">
            <img src="./article_files/svg-master.svg" alt="">
            <img src="./article_files/svg-visa.svg" alt="">
          </div>
          <div class="footer__socials">
            <ul>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#">
                  <svg height="18px" width="18px">
                    <use href="../static/svg-meta.svg#meta"></use>
                  </svg>
                  <p>facebook</p>
                </a>
              </li>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#">
                  <svg height="18px" width="24px">
                    <use href="../static/svg-youtube.svg#youtube"></use>
                  </svg>
                  <p>YouTube</p>
                </a>
              </li>
              <li>
                <a href="https://dneprvagonka-demo.web.app/article.html#">
                  <svg height="18px" width="18px">
                    <use href="../static/svg-instagram.svg#instagram"></use>
                  </svg>
                  <p>Instagram</p>
                </a>
              </li>
            </ul>
          </div>
          <div class="footer__copyrights">
            <p>
              © Дніпровська вагонка<br>
              2024 Все права защищены
            </p>
          </div>
          <div class="footer__madeby">
            <img src="./article_files/svg-madeby-logo.svg" alt="">
            <p>Сайт создан в Page Media Solutions</p>
          </div>
        </div>
      </section>
    </section>
  </div>
</footer>

    <script src="./article_files/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="./article_files/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="./article_files/article.js"></script>
    <script src="./article_files/script.js"></script>
  

</body></html>