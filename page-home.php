<?php
/*
 * Template name: Home
 */
?>
<?php get_header(); ?>
<?php ( new Page_Home_First_Section() )->render(); ?>
<?php ( new Page_Home_Products_Section() )->render(); ?>
<?php ( new Page_Home_Cta_Section() )->render(); ?>
<?php ( new Page_Home_Features_Section() )->render(); ?>
<?php ( new Page_Home_About_Section() )->render(); ?>
<?php ( new Page_Home_Publisity_Section() )->render(); ?>
<?php get_footer(); ?>


