<?php
/*
 * Template name: Primacypolicy
 */
?>
<?php get_header();?>
<div class="privacy">
    <div class="container">
        <section class="section__outer">
            <section class="section__inner">
                <div class="privacy__wrapper">
                    <div class="privacy__title">
                        <h1><?php echo get_the_title(); ?></h1>
                    </div>
                    <?php echo get_the_content(); ?>
                </div>
            </section>
        </section>
    </div>
</div>
<?php get_footer();?>


