<?php

    $lang = wpml_get_current_language();

    if(!have_posts()) {
        wp_redirect("/" . ($lang == 'uk' ? 'uk/' : ''));
        exit;
    }
    
    require_once __DIR__ . "/classes/vg_user.php";

    global $VG_user;
    $VG_user = new VG_User();
    $new_posts_count = $VG_user->get_count_new_materials();


    $header = get_field('options_header', 'option');
    $login_form = new Header_LoginForm();
    $cabinet_menu = new Header_CabinetMenu();
    if(count($header) > 0) $header = $header[0];
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title><?php wp_title(); ?></title>
    <link rel="stylesheet" href="<?= TEMPLATE_PATH ?>css/style.css?v=1.4">
    <?php if (is_singular('post') or is_product()) : ?>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css">
    <?php endif; ?>
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=1">
    <?php if ( is_front_page() ) : ?>
      <link rel="stylesheet" href="<?=TEMPLATE_PATH?>css/swiper.min.css">
      <script src="<?=TEMPLATE_PATH?>js/swiper.min.js"></script>
    <?php endif; ?>
    <?php wp_head(); ?>
</head>
    <script>
        let currentLanguage = '<?php echo wpml_get_current_language(); ?>';
    </script>
<div id="admin-ajax-url" style="display:none;"><?php echo admin_url('admin-ajax.php'); ?></div>

<?php if ( is_front_page() ) {?>
    <body class="header-top">
    <div class="first-screen">
<?php } else { ?>
    <body>
<?php }?>

<?php if (get_the_ID() == Page_Offices::get_ID()) { ?>
<div class="offices">
    <?php } ?>

    <?php if (get_the_ID() == Page_Partners::get_ID()) { ?>
    <div class="partners">
        <div class="partners-first-screen">
            <?php } ?>

            <?php if (get_the_ID() == Page_Contacts::get_ID()) : ?>
            <div class="contacts">
                <?php endif; ?>

                <div class="header">

                    <div class="container">
                        <section class="section__outer">
                            <section class="section__inner">
                                <header class="header__wrapper">
                                    <?php if (!empty($header['option_logo'])) { ?>
                                        <picture class="header__logo">
                                            <a href="<?php echo home_url(); ?>">
                                                <img src="<?php echo $header['option_logo']; ?>"
                                                     alt="">
                                            </a>
                                        </picture>
                                    <?php } ?>
                                    <?php if (!empty($header['option_menu'])) { ?>
                                        <nav class="header__nav" aria-label="primary navigaion">
                                            <ul>
                                                <?php foreach ($header['option_menu'] as $item) {
                                                    ?>
                                                    <?php if ( (get_the_ID() == get_page_by_title($item['menu_links']['title'])->ID) or ( strstr( $item['menu_links']['url'], $_SERVER['REQUEST_URI'] )) and !is_front_page()) { ?>
                                                        <li class="active"><a
                                                                    href="<?php echo $item['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                        </li>
                                                    <?php } elseif (is_singular('post') && ( ( Page_Posts::get_url() == $item['menu_links']['url'] ) or ( strstr( $item['menu_links']['url'], '/uk/stati' ) ) ) ) { ?>
                                                        <li class="active"><a
                                                                    href="<?php echo $item['menu_links']['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                        </li>
                                                    <?php } else { ?>
                                                        <li>
                                                            <a href="<?php echo $item['menu_links']['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                        </li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </ul>
                                        </nav>
                                    <?php } ?>
                                    <?php if (!empty($header['phones'])) { ?>
                                        <div class="header__contacts">
                                            <ul>
                                                <?php foreach ($header['phones'] as $item) { ?>
                                                    <li>
                                                        <a href="tel:<?= preg_replace('/[^\d]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <?php
                                        $social = get_field('options_header', 'option');
                                        if(count($social) > 0) $social = $social[0]['option_socials'];
                                    ?>
                                    <?php if (!empty($social)) { ?>
                                        <div class="header__socials">
                                            <ul>
                                                <?php foreach ($social as $item) { ?>
                                                    <li>
                                                        <a href="<?php echo $item['header_socials_link']["url"]; ?>" target="<?php echo $item['header_socials_link']['target']; ?>">
                                                            <svg width="24" height="18">
                                                                <use href="<?php echo $item["header_socials_icon"]["url"] . "#" . strtolower($item["header_socials_link"]["title"]); ?>"/>
                                                            </svg>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    <?php } ?>
                                    <div class="header__lang">
                                        <div class="header__lang-display">
                                            <ul>
                                                <li class="header__lang-item active">
                                                    <span><?php echo $lang == "uk" ? "UA" : $lang; ?></span>
                                                    <i>
                                                        <svg width="16" height="15">
                                                            <use href="/wp-content/themes/vagonka/static/svg-lang.svg#lang"/>
                                                        </svg>
                                                    </i>
                                                </li>
                                            </ul>
                                        </div>

                                        <div style="display:none;" class="wpml-floating-language-switcher">
                                            <?php do_action('wpml_add_language_selector'); ?>
                                        </div>

                                        <?php 
                                        // if($lang === "ru") {
                                        //     if(empty(apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk')) or apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk') == apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'ru') ) {
                                        //         $post_url_uk = "/uk/";
                                        //     } else {
                                        //         $post_url_uk = "/uk" . str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        //     }
                                        //     $post_url_ru = str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        // } else {
                                        //     if(empty(apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk')) or apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk') == apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'ru') ) {
                                        //         $post_url_ru = "/";
                                        //     } else { 
                                        //         $post_url_ru = str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        //     }
                                        //     $post_url_uk = "/uk" . str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        // }
                                        if($lang === "ru") {
                                            if(empty(apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk')) or apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk') == apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'ru') ) {
                                                $post_url_uk = "/";
                                            } else {
                                                $post_url_uk = str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                            }
                                            $post_url_ru = "/ru" . str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        } else {
                                            if(empty(apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk')) or apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'uk') == apply_filters( 'wpml_object_id', get_the_ID(), 'post', true, 'ru') ) {
                                                $post_url_ru = "/ru";
                                            } else { 
                                                $post_url_ru = "/ru" . str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                            }
                                            $post_url_uk = str_replace(array("/ru", "/uk"), "", $_SERVER["REQUEST_URI"]);
                                        }
                                        ?>

                                        <section class="header__lang-popup" id="header__lang_id">
                                            <ul>
                                                <li><a <?php if($lang === "ru") echo "class='active'"; ?> href="<?php echo $post_url_ru; ?>">ru</a></li>
                                                <li><a <?php if($lang === "uk") echo "class='active'"; ?> href="<?php echo $post_url_uk; ?>">ua</a></li>
                                            </ul>
                                        </section>
                                    </div>
                                    <div class="header__actions">
                                        <ul>
                                            <?php if ( strstr( $_SERVER['REQUEST_URI'], 'shop' ) or is_product() ) : ?>
                                                <li class="active">
                                            <?php else : ?>
                                                <li>
                                            <?php endif; ?>
                                                <?php if (!empty($header['catalog_link'])) { ?>
                                                    <a href="<?php echo $header['catalog_link']['url']; ?>">
                                                        <i>
                                                            <svg width="14" height="15">
                                                                <use href="<?= TEMPLATE_PATH ?>static/svg-catalog.svg#catalog"/>
                                                            </svg>
                                                        </i>
                                                        <span><?php echo $header['catalog_link']['title']; ?></span>
                                                    </a>
                                                <?php } ?>
                                            </li>
                                            <li>
                                                <a data-link="cart" href="<?php echo wc_get_cart_url(); ?>">
                                                    <i>
                                                        <svg width="25" height="16">
                                                            <use href="<?= TEMPLATE_PATH ?>static/svg-cart.svg#cart"/>
                                                        </svg>
                                                    </i>
                                                    <span><?php echo get_field('header','option')['tekst_korzina']?></span>
                                                    <?php
                                                    $cart_count = WC()->cart->get_cart_contents_count();
                                                    $cart_total = WC()->cart->get_cart_total(); ?>
                                                    <?php if ($cart_count != 0) : ?>
                                                        <div class="cart-count">
                                                            <p><?php echo $cart_count; ?></p>
                                                        </div>
                                                    <?php endif; ?>
                                                </a>


                                                <section class="header__actions-popup popup-cart">
                                                    <div class="cart-popup">
                                                        <div class="cart-popup__wrapper">
                                                            <?php
                                                            $cart_item = WC()->cart->get_cart();
                                                            $cart_count = WC()->cart->get_cart_contents_count();
                                                            if (!WC()->cart->prices_include_tax) {
                                                                $cart_total = WC()->cart->cart_contents_total;
                                                            } else {
                                                                $cart_total = WC()->cart->cart_contents_total + WC()->cart->tax_total;
                                                            }
                                                            $cart_total = explode('.', number_format($cart_total, 2, ".", " "));
                                                            if (!empty($cart_item)) : ?>
                                                                <ul class="cart-popup__list">
                                                                    <?php foreach ($cart_item as $item => $values) :
                                                                        $variation = wc_get_product($values['data']->get_id());
                                                                        $product = wc_get_product($variation->get_parent_id());
                                                                        $attributes = $variation->get_variation_attributes();
                                                                        $obem = get_term_by('slug', $attributes['attribute_pa_obem'], 'pa_obem')->name;
                                                                        $color = get_term_by('slug', $attributes['attribute_pa_czvet'], 'pa_czvet')->name;
                                                                        $line_price = number_format($values['line_total'], 2, ".", " ");
                                                                        $price = explode('.', $line_price);
                                                                        ?>
                                                                        <li class="cart-popup__product">
                                                                            <picture>
                                                                                <img src="<?php echo get_the_post_thumbnail_url($product->get_id()); ?>"
                                                                                     alt="">
                                                                            </picture>
                                                                            <div class="cart-popup__product-info">
                                                                                <div class="cart-popup__product-title">
                                                                                    <p><?php echo $product->get_title(); ?></p>
                                                                                </div>
                                                                                <div class="cart-popup__product-details">
                                                                                    <p>
                                                                                        <?php echo $obem; ?>
                                                                                        <span> <?php echo $color; ?></span>
                                                                                        <span><?php echo $values['quantity']; ?> шт</span>
                                                                                    </p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="cart-popup__product-total">
                                                                                <p><?php echo $price[0]; ?>
                                                                                    <span>.<?php echo $price[1]; ?></span><span
                                                                                            class="currency"> ₴</span>
                                                                                </p>
                                                                            </div>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                                <div class="cart-popup__bottom">
                                                                    <div class="cart-popup__link">
                                                                    <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                                        <a class="btn btn-colored"
                                                                           href="<?php echo wc_get_cart_url(); ?>">
                                                                            замовлення
                                                                        </a>
                                                                        <?php else : ?>
                                                                            <a class="btn btn-colored"
                                                                           href="<?php echo wc_get_cart_url(); ?>">К
                                                                            заказу
                                                                        </a>
                                                                        <?php endif; ?>
                                                                        
                                                                    </div>
                                                                    <div class="cart-popup__total">
                                                                        <span class="cart-popup__total-text">
                                                                        <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                                            на суму
                                                                        <?php else : ?>
                                                                            на сумму
                                                                        <?php endif; ?>
                                                                        </span>
                                                                        <p class="cart-popup__total-price">
                                                                            <?php echo $cart_total[0]; ?>
                                                                            <span>.<?php echo $cart_total[1]; ?> </span><span
                                                                                    class="currency">₴</span>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            <?php else : ?>
                                                                
                                                            <p>
                                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                                В кошику поки немає товарів
                                                            <?php else : ?>
                                                                В корзине пока нет товаров
                                                            <?php endif; ?>
                                                            <p>
                                                                <?php endif; ?>
                                                        </div>
                                                    </div>

                                                </section>

                                         
                                            </li>
                                            <?php if($VG_user->id == 0 or $VG_user->id == null ) { ?>
                                                <?php $login_form->render(); ?>
                                            <?php } else { ?>
                                                <?php $cabinet_menu->render(); ?>
                                            <?php } ?>
                                         
                                        </ul>
                                  
                                    </div>
                                    <div class="header__mobile-toggle">
                                        <ul>
                                            <li class="open">
                                                <i>
                                                    <svg width="14" height="17">
                                                        <use href="<?= TEMPLATE_PATH ?>static/svg-burger.svg#burger"/>
                                                    </svg>
                                                </i>
                                                <span>Весь сайт</span>
                                            </li>
                                            <li class="close">
                                                <i>
                                                    <svg width="14" height="17">
                                                        <use href="<?= TEMPLATE_PATH ?>static/svg-burger-close.svg#burger-close"/>
                                                    </svg>
                                                </i>
                                                <span><?php echo get_field('header','option')['tekst_zakryt']?></span>
                                            </li>
                                        </ul>
                                    </div>
                                </header>
                            </section>
                        </section>
                    </div>
                  
                    <div class="header__mobile">
                        <div class="container">
                            <nav class="header__mobile-nav" aria-label="mobile navigaion">
                                <ul>
                                    <?php if (!empty($header['catalog_link'])) { ?>
                                        <?php if ( strstr( $_SERVER['REQUEST_URI'], 'shop' ) or is_product() ) : ?>
                                            <li class="active">
                                        <?php else : ?>
                                            <li>
                                        <?php endif; ?>
                                            <a href="<?php echo $header['catalog_link']['url']; ?>"><?php echo $header['catalog_link']['title']; ?></a>
                                            <i>
                                                <svg width="14" height="15">
                                                    <use href="<?= TEMPLATE_PATH ?>static/svg-catalog.svg#catalog"></use>
                                                </svg>
                                            </i>
                                        </li>
                                    <?php } ?>
        
                                    <?php if (!empty($header['option_menu'])) { ?>
                                        <?php foreach ($header['option_menu'] as $item) { ?>
                                            <?php if ( (get_the_ID() == get_page_by_title($item['menu_links']['title'])->ID) or ( strstr( $item['menu_links']['url'], $_SERVER['REQUEST_URI'] )) and !is_front_page()) { ?>
                                                <li class="active">
                                                    <a href="<?php echo $item['menu_links']['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                </li>
                                            <?php } elseif (is_singular('post') && ( ( Page_Posts::get_url() == $item['menu_links']['url'] ) or ( strstr( $item['menu_links']['url'], '/uk/stati' ) ) ) ) { ?>
                                                <li class="active">
                                                    <a href="<?php echo $item['menu_links']['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                </li>
                                            <?php } else { ?>
                                                <li>
                                                    <a href="<?php echo $item['menu_links']['url']; ?>"><?php echo $item['menu_links']['title']; ?></a>
                                                </li>
                                            <?php } ?>
                                          
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                            </nav>
                            <div class="header__mobile-actions">
                                <ul>
                                    <li>
                                        <a href="<?php echo wc_get_cart_url(); ?>">
                                            <span><?php echo get_field('header','option')['tekst_korzina']?></span>
                                            <i>
                                                <svg width="25" height="16">
                                                    <use href="<?= TEMPLATE_PATH ?>static/svg-cart.svg#cart"/>
                                                </svg>
                                            </i>
                            
                                            <?php $cart_count = WC()->cart->get_cart_contents_count();
                                            $cart_total = WC()->cart->get_cart_total(); ?>
                                            <?php if ($cart_count != 0) : ?>
                                                <div class="cart-count">
                                                    <p><?php echo $cart_count; ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </a>
                                    </li>
                                    
                                    <?php //if ( !wp_is_mobile() ) : ?>
                                    <li>
                              
                                        <?php if ( $lang == 'uk' ) : ?>
                                            <a href="<?php echo ( $VG_user->is_auth() ) ? '/profile' : '/login-page' ;?>">
                                        <?php else : ?>
                                            <a href="<?php echo ( $VG_user->is_auth() ) ? '/ru/profile' : '/ru/login-page' ;?>">
                                        <?php endif; ?>
                                        <?php if ( $VG_user->is_auth() ) : ?>
                                            <span><?php echo get_field('header','option')['tekst_kabinet']; ?></span>
                                            <i>
                                                <svg width="17" height="17">
                                                    <use class="svg-catalog"
                                                        href="<?= TEMPLATE_PATH ?>static/svg-account.svg#account"/>
                                                </svg>
                                            </i>
                                        <?php else : ?>
                                            <span><?php echo get_field('header','option')['tekst_vhod']; ?></span>
                                            <i>
                                                <svg width="12" height="17">
                                                    <use class="svg-catalog"
                                                         href="<?= TEMPLATE_PATH ?>static/svg-singin.svg#singin"/>
                                                </svg>
                                            </i>
                                        <?php endif; ?>
                                        </a>
                                    </li>
                                    <?php //endif; ?>
                                </ul>
                            </div>
                            <?php if (!empty($header['phones'])) { ?>
                                <div class="header__mobile-contacts">
                                    <ul>
                                        <?php foreach ($header['phones'] as $item) { ?>
                                        <li>
                                            <a href="tel:<?= preg_replace('/[^\d]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                            <?php if (!empty($social)) { ?>
                                <div class="header__mobile-socials">
                                    <ul>
                                        <?php foreach ($social as $item) { ?>
                                            <li>
                                                <a href="<?php echo $item['header_socials_link']['url']; ?>" target="_blank">
                                                    <span><?php echo $item['header_socials_link']['title']; ?></span>
                                                    <i>
                                                        <svg width="24" height="18">
                                                            <use href="<?php echo $item["header_socials_icon"]["url"] . "#" . strtolower($item["header_socials_link"]["title"]); ?>"/>
                                                        </svg>
                                                    </i>
                                                </a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                