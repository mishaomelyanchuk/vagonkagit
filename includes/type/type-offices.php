<?php

class Type_Offices {
	

	public static function init() {

        add_action('init', [ __CLASS__, 'register_type' ] );
        add_action( 'init', [ __CLASS__, 'register_taxonomy' ] );
       
	}

    public function register_type() {
        register_post_type('offices', array(
            'labels'=> array(
                'name'=> 'Представительства', 
                'singular_name'=> 'Представительства', 
    
              ),
            'public' => false,
            'show_ui'             => true, 
		    'show_in_nav_menus'   => true, 
		    'show_in_menu'        => true, 
            'supports'=> array( 'title' )
        ) );
    }

    public function register_taxonomy() {

        register_taxonomy( 'areas', 'offices', array(
            "hierarchical" => true,
            "label" => "Области",
            "singular_label" => "Области",
            'show_ui' => true,
            'show_admin_column' => true
            ));

    }

    public static function acf_add_local_field_group() {
		if( function_exists('acf_add_local_field_group') ):
          
            acf_add_local_field_group(array(
                'key' => 'group_6274dd081cab0',
                'title' => 'Представительства пост тайп',
                'fields' => array(
                    array(
                        'key' => 'field_6274dd49f48ea',
                        'label' => 'Тип придставительства',
                        'name' => 'office_type',
                        'type' => 'select',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'shop' => 'Магазин',
                            'regional_office' => 'Региональное представительство',
                        ),
                        'default_value' => false,
                        'allow_null' => 0,
                        'multiple' => 0,
                        'ui' => 0,
                        'return_format' => 'value',
                        'ajax' => 0,
                        'placeholder' => '',
                    ),
                    array(
                        'key' => 'field_6275183a10433',
                        'label' => 'Расположение',
                        'name' => 'map',
                        'type' => 'google_map',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'center_lat' => '',
                        'center_lng' => '',
                        'zoom' => '',
                        'height' => '',
                    ),
                    array(
                        'key' => 'field_6274ddca13899',
                        'label' => 'Адрес',
                        'name' => 'adress',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_6274ddca1389a',
                        'label' => 'Номера',
                        'name' => 'phones',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'table',
                        'button_label' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_6274ddca1389b',
                                'label' => 'Номер',
                                'name' => 'phone',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                            array(
                                'key' => 'field_6274ddca1389c',
                                'label' => 'Должность',
                                'name' => 'position',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_6274ddca1389d',
                        'label' => 'Ссылки',
                        'name' => 'links',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'table',
                        'button_label' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_6274ddca1389e',
                                'label' => 'Ссылка',
                                'name' => 'link',
                                'type' => 'link',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'return_format' => 'array',
                            ),
                        ),
                    ),
                    array(
                        'key' => 'field_6274ddca1389f',
                        'label' => 'Емейлы',
                        'name' => 'emails',
                        'type' => 'repeater',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'collapsed' => '',
                        'min' => 0,
                        'max' => 0,
                        'layout' => 'table',
                        'button_label' => '',
                        'sub_fields' => array(
                            array(
                                'key' => 'field_6274ddca138a0',
                                'label' => 'Емейл',
                                'name' => 'email',
                                'type' => 'text',
                                'instructions' => '',
                                'required' => 0,
                                'conditional_logic' => 0,
                                'wrapper' => array(
                                    'width' => '',
                                    'class' => '',
                                    'id' => '',
                                ),
                                'default_value' => '',
                                'placeholder' => '',
                                'prepend' => '',
                                'append' => '',
                                'maxlength' => '',
                            ),
                        ),
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'offices',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
                'show_in_rest' => 0,
            ));
            

		endif;
	}

}
