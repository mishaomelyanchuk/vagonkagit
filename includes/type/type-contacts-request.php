<?php

class Type_Contacts_Request {


    public static function init() {

        add_action( 'acf/init', [ __CLASS__, 'acf_add_local_field_group' ] );
        add_action('init', [ __CLASS__, 'register_type' ] );
        add_action( 'wp_ajax_nopriv_contacts_contact_us' ,[ __CLASS__, 'process' ] );
        add_action( 'wp_ajax_contacts_contact_us', [ __CLASS__, 'process' ] );
    }

    public static function process() {
        $formdata = filter_input_array( INPUT_GET );
        $contact_name = $formdata['name'];
        $contact_email = $formdata['email'];
        $contact_offer = $formdata['offer'];

        $order_id = wp_insert_post( [
            'post_title'    => 'Новая заявка',
            'post_type'     => 'contacts_request',
        ], true);

        update_post_meta($order_id, 'contact_name', $contact_name);
        update_post_meta($order_id, 'contact_email', $contact_email);
        update_post_meta($order_id, 'contact_offer', $contact_offer);

        header('Location: '.Page_Contacts::get_url());die;

    }

    public function register_type() {
        register_post_type('contacts_request', array(
            'labels'=> array(
                'name'=> 'Заявки с Контактов',
                'singular_name'=> 'Заявки с Контактов',

            ),
            'public' => false,
            'show_ui'             => true,
            'show_in_nav_menus'   => true,
            'show_in_menu'        => true,
            'supports'=> array( 'title' )
        ) );
    }

    public static function acf_add_local_field_group() {
        if( function_exists('acf_add_local_field_group') ):

            acf_add_local_field_group(array(
                'key' => 'group_62582fd6a5935',
                'title' => 'Заявки с Контактов',
                'fields' => array(
                    array(
                        'key' => 'field_62582fd6acd1c',
                        'label' => 'Имя контакта',
                        'name' => 'contact_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_62582fd6acdc8',
                        'label' => 'E-mail контакта',
                        'name' => 'contact_email',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_62582fd6ace05',
                        'label' => 'Предложение',
                        'name' => 'contact_offer',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
                'location' => array(
                    array(
                        array(
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'contacts_request',
                        ),
                    ),
                ),
                'menu_order' => 0,
                'position' => 'normal',
                'style' => 'default',
                'label_placement' => 'top',
                'instruction_placement' => 'label',
                'hide_on_screen' => '',
                'active' => true,
                'description' => '',
                'show_in_rest' => 0,
            ));

        endif;

    }

}