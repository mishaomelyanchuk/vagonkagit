<?php

use FFI\Exception;

class Page_Catalog {
    public static function init() {
        add_action( 'wp_ajax_nopriv_catalog_filters', [ __CLASS__, 'ajax_handler' ] );
        add_action( 'wp_ajax_catalog_filters', [ __CLASS__, 'ajax_handler' ] );

        add_action( 'wp_ajax_nopriv_form_reset', [ __CLASS__, 'ajax_handler_filter_reser' ] );
        add_action( 'wp_ajax_form_reset', [ __CLASS__, 'ajax_handler_filter_reser' ] );

    }

    public static function ajax_handler_filter_reser() {
 
        $args =  array(
         'post_type'   => 'product',
         'post_status' => 'publish'
        );
 
         $products = new WP_Query( $args );
 
         if ( $products->have_posts() ) : ?>
             <?php while ( $products->have_posts() ) : $products->the_post(); ?>
             <?php  $product = wc_get_product( get_the_ID() ); ?>
                 <div class="catalog-item" data-product-path="<?php echo get_the_permalink(); ?>">
                     <div class="catalog-item__wrapper">
                         <div class="catalog-item__promo">
                         <?php 
                         if ( !empty( get_field( 'sale_flash', $product->id ) ) )  : ?> 
                             <div><span><?php echo get_field( 'sale_flash', $product->id ); ?></span></div>
                         <?php endif; 
                         ?>
                         </div>
                         <div class="catalog-item__title">
                         <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                         </div>
                         <picture>
                         <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                         <?php 

                             $colors = (woocommerce_get_product_terms($product->id, 'pa_czvet'));
                             ?>
                             <?php if ( !empty( $colors ) ) : ?>
                             <div class="catalog-item__video">
                                 <?php foreach ( $colors as $item ) : ?>
                                     <video data-color-video="<?php echo $item->slug; ?>" loop muted>
                                     <source src="<?php echo get_field( 'video', 'term_'.$item->term_id )['url']; ?>">
                                     </video>
                                 <?php endforeach?>
                             </div>
                             <?php endif; ?>
                         </picture>
                         <?php 
                         $obem = (woocommerce_get_product_terms($product->id, 'pa_obem', 'names'));
                         ?>
                         <?php if ( !empty( $obem ) ) : ?>
                         <div class="catalog-item__sizes">
                         <span>
                             <?php $count = 0;?>
                             <?php foreach ( $obem as $item ) : ?>
                                 <?php $count++; ?>
                                 <?php if ( $count == count($obem) ) : ?>
                                     <?php echo $item;?>
                                 <?php else : ?>
                                     <?php echo $item . ' /';?>
                                 <?php endif; ?>
                             <?php endforeach ; ?>
                         </span>
                         </div>
                         <?php endif; ?>
                         <?php if ( !empty( $colors ) ) : 
                            $available_variations = $product->get_available_variations();
                            foreach ($colors as $color_item) :
                                $on = true;
                                foreach ( $available_variations as $item ) :
                                    if ( $item['attributes']['attribute_pa_czvet'] == $color_item->slug ) :
                                        $on = true;
                                        break;
                                    else :
                                        $on = false;
                                    endif; 
                                endforeach;
                                if ( !$on ) :
                                    $disabled[] = $color_item->slug;
                                endif; 
                            endforeach; 
                            ?>
                             <div class="catalog-item__color">
                             <ul>
                                 <?php foreach ( $colors as $item ) :
                                 if ( !in_array( $item->slug, $disabled ) ) : ?>
                                    <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                <?php endif; ?> 
                                 <?php endforeach?>
                             </ul>
                             </div>
                         <?php endif; ?>
                         <?php 
                            $prices = $product->get_variation_prices();
                            $price = min($prices['price']);
                            $sale_price = min($prices['sale_price']);
                            if ( $sale_price < $price ) {
                                $price = min($prices['price']);
                            } 
                            if ( wp_get_current_user()->data->user_type == '1' ) {
                                if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                    $price =  $price- ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
                                }
                            } else {
                                $prices = $product->get_variation_prices();
                                $price = min($prices['price']);
                                $sale_price = min($prices['sale_price']);
                                if ( $sale_price < $price ) {
                                    $price = min($prices['price']);
                                } 
                            }
                        ?>
                         <div class="catalog-item__btn">
                         <a href="<?php echo get_the_permalink(); ?>">
                             <button class="btn btn-colored"><?php echo get_field('catalog','option')['tekst_vybrat']?></button>
                             <span class="btn btn-small"><?php echo get_field('catalog','option')['tekst_ot']?> <?php echo number_format($price, 2, '.', '') ?> <i class="currency"> ₴</i></span>
                         </a>
                         </div>
                         <div class="catalog-item__description">
                         <p>
                            <?php the_excerpt(); ?>
                         </p>
                         </div>
                     </div>
                 </div>
             <?php endwhile; ?>
         <?php endif; 
         die;
 
     }

    public static function ajax_handler() {
     
       $categories = $_POST['categories'];
       $use = $_POST['use'];
       $color = $_POST['color'];

       $args =  array(
        'post_type'   => 'product',
        'post_status' => 'publish'
       );

       if ( !empty( $categories ) ) {
            $args['tax_query'][] = [
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $categories,
                'operator' => 'IN'
            ];
        }
 
        if ( !empty( $use ) ) {
            $args['tax_query'][] = [
                'taxonomy' => 'product_tag',
                'field' => 'slug',
                'terms' => $use,
                'operator' => 'IN'
            ];
        }

        if ( !empty( $color ) ) {
            $args['tax_query'][] = [
                'taxonomy' => 'color_palitra',
                'field' => 'slug',
                'terms' => $color,
                'operator' => 'IN'
            ];
        }
        $products = new WP_Query( $args );
       

        if ( $products->have_posts() ) : ?>
            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
            <?php  $product = wc_get_product( get_the_ID() ); ?>
                <div class="catalog-item" data-product-path="<?php echo get_the_permalink(); ?>">
                    <div class="catalog-item__wrapper">
                        <div class="catalog-item__promo">
                        <?php 
                        if ( !empty( get_field( 'sale_flash', $product->id )) )  : ?> 
                            <div><span><?php echo get_field( 'sale_flash', $product->id ); ?></span></div>
                        <?php endif; 
                        ?>
                        </div>
                        <div class="catalog-item__title">
                        <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                        </div>
                        <picture>
                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                        <?php 
                            $colors = woocommerce_get_product_terms($product->id, 'pa_czvet');
                            ?>
                            <?php if ( !empty( $colors ) ) : ?>
                            <div class="catalog-item__video">
                                <?php foreach ( $colors as $item ) : ?>
                                    <video data-color-video="<?php echo $item->slug; ?>" loop muted>
                                    <source src="<?php echo get_field( 'video', 'term_'.$item->term_id )['url']; ?>">
                                    </video>
                                <?php endforeach?>
                            </div>
                            <?php endif; ?>
                        </picture>
                        <?php 
                        $obem = woocommerce_get_product_terms($product->id, 'pa_obem', 'names');
                        // $obem = $product->get_attribute( 'pa_obem' );
                        ?>
                        <?php if ( !empty( $obem ) ) : ?>
                        <?php //$obem = explode( ', ', $obem ); ?>
                        <div class="catalog-item__sizes">
                        <span>
                            <?php $count = 0;?>
                            <?php foreach ( $obem as $item ) : ?>
                                <?php $count++; ?>
                                <?php if ( $count == count($obem) ) : ?>
                                    <?php echo $item;?>
                                <?php else : ?>
                                    <?php echo $item . ' /';?>
                                <?php endif; ?>
                            <?php endforeach ; ?>
                        </span>
                        </div>
                        <?php endif; ?>
                        <?php if ( !empty( $colors ) ) : 
                            $available_variations = $product->get_available_variations();
                            foreach ($colors as $color_item) :
                                $on = true;
                                foreach ( $available_variations as $variations_item ) :
                                    if ( $variations_item['attributes']['attribute_pa_czvet'] == $color_item->slug ) :
                                        $on = true;
                                        break;
                                    else :
                                        $on = false;
                                    endif; 
                                endforeach;
                                if ( !$on ) :
                                    $disabled[] = $color_item->slug;
                                endif; 
                            endforeach; 
                            ?>
                            <div class="catalog-item__color">
                            <ul>
                                <?php foreach ( $colors as $item ) : 
                                if ( !in_array( $item->slug, $disabled ) ) : 
                                ?>
                                <?php if ( !empty( $color ) ) : ?>
                                    <?php if ( get_field( 'color_category', 'term_'.$item->term_id )[0]->slug == $color ) : ?>
                                        <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                    <?php else : ?>
                                        <li></li>
                                    <?php endif; ?>
                                <?php else : ?>
                                    <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                <?php endif; ?>
                                <?php endif; ?>
                                <?php endforeach; ?>
                            </ul>
                            </div>
                        <?php endif; ?>
                        <?php 
                            $prices = $product->get_variation_prices();
                            $price = min($prices['price']);
                            $sale_price = min($prices['sale_price']);
                            if ( $sale_price < $price ) {
                                $price = min($prices['price']);
                            } 
                            if ( wp_get_current_user()->data->user_type == '1' ) {
                                if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                    $price = $price- ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
                                }
                            } else {
                                $prices = $product->get_variation_prices();
                                $price = min($prices['price']);
                                $sale_price = min($prices['sale_price']);
                                if ( $sale_price < $price ) {
                                    $price = min($prices['price']);
                                } 
                            }
                        ?>
                        <div class="catalog-item__btn">
                        <a href="<?php echo get_the_permalink(); ?>">
                            <button class="btn btn-colored"><?php echo get_field('catalog','option')['tekst_vybrat']?></button>
                            <span class="btn btn-small"><?php echo get_field('catalog','option')['tekst_ot']?> <?php echo number_format($price, 2, '.', '') ?> <i class="currency"> ₴</i></span>
                        </a>
                        </div>
                        <div class="catalog-item__description">
                        <p>
                           <?php the_excerpt(); ?>
                        </p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; 
        die;

    }
  
}
