<?php

class Theme_Option {
    public static function init() {
        add_action( 'acf/init', [ __CLASS__, 'acf_add_options_page' ] );
        add_action( 'acf/init', [ __CLASS__, 'acf_add_local_field_group' ] );

        add_action( 'wp_ajax_nopriv_count_update', [ __CLASS__, 'count_update' ] );
        add_action( 'wp_ajax_count_update', [ __CLASS__, 'count_update' ] );
       
        add_action( 'wp_ajax_nopriv_mini_cart_update', [ __CLASS__, 'mini_cart_update' ] );
        add_action( 'wp_ajax_mini_cart_update', [ __CLASS__, 'mini_cart_update' ] );

        add_action( 'wp_ajax_nopriv_show_popup', [ __CLASS__, 'show_popup' ] );
        add_action( 'wp_ajax_show_popup', [ __CLASS__, 'show_popup' ] );

        add_action( 'wp_ajax_nopriv_delete_cart_item', [ __CLASS__, 'delete_cart_item' ] );
        add_action( 'wp_ajax_delete_cart_item', [ __CLASS__, 'delete_cart_item' ] );

        add_action( 'wp_ajax_update_quantity', [ __CLASS__, 'update_quantity' ] );
        add_action( 'wp_ajax_nopriv_update_quantity', [ __CLASS__, 'update_quantity' ] );


    }

    public static function update_quantity() {
		$cart_item_key = $_POST['cardID'];
        $quantity = $_POST['quantity'];
		WC()->cart->set_quantity( $cart_item_key, $quantity, true );
        $cart_item = WC()->cart->get_cart();
        $cart_count = WC()->cart->get_cart_contents_count();
        if ( ! WC()->cart->prices_include_tax ) {
            $cart_total = WC()->cart->cart_contents_total;
        } else {
            $cart_total = WC()->cart->cart_contents_total + WC()->cart->tax_total;
        }
        $cart_total = explode('.' , number_format($cart_total, 2, ".", " "));
        if ( !empty( $cart_item ) ) : ?>
        <div class="cart__title">
			<h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
			</div>
			<ul class="cart__list">
				<?php $count = 1; ?>
				<?php foreach ( $cart_item as $item => $values ) : 
					$variation = wc_get_product( $values['data']->get_id() ); 
                    $product = wc_get_product( $variation->get_parent_id() );
                    $attributes =  $variation->get_variation_attributes() ;
                    $obem = get_term_by('slug',  $attributes['attribute_pa_obem'], 'pa_obem')->name;
                    $color = get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->name;
                    $line_price = number_format($values['line_total'], 2, ".", " ");
                    $price = explode('.' , $line_price);
                    ?>
					<li class="cart__list-item">
						<div class="cart__product">
						<div class="cart__product-position">
						<span><?php echo $count; ?></span>
						</div>
						<picture>
						<img src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" alt="">
						</picture>
						<div class="cart__product-title">
                            <a href="<?php echo get_permalink( $product->get_id() ); ?>" target="_blank"><?php echo $product->get_title(); ?></a>
						</div>
						<div class="cart__product-info">
						<div class="cart__product-info_size"><?php echo $obem; ?></div>
						<div class="cart__product-info_color" style="background-color: <?php echo get_field( 'color', 'term_'.get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->term_id );?>"></div>
						<div class="cart__product-info_name"><?php echo $color; ?></div>
						<i>
						<svg height="24px" width="24px">
							<use href="<?=TEMPLATE_PATH?>tatic/svg-edit.svg#edit"></use>
						</svg>
						</i>
						</div>
						<div class="cart__product-count">
						<div class="product__order-counter">
						<button class="counter-button" data-value="minus">
							<svg width="14px" height="2px">
							<use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#minus"></use>
							</svg>
						</button>
						<input data-value="display" type="number" onkeyup="this.value = this.value.replace (/\D/, '')" min="0" value="<?php echo $values['quantity']?>">
						<button class="counter-button" data-value="plus">
							<svg width="14px" height="14px">
							<use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#plus"></use>
							</svg>
						</button>
						</div>
						</div>
						<div class="cart__product-summary">
						<div class="cart__product-summary_total">
                            <p>
                              <?php echo $price[0]; ?><span>.<?php echo $price[1]; ?></span><span> </span><span class="currency">₴</span>
                            </p>
						</div>
						<div class="cart__product-summary_price">
						<ul>
							<?php if ( $variation->is_on_sale() ) : ?>
								<li class="price-new">
                                <?php 
                                 if ( wp_get_current_user()->data->user_type == '1' ) : 
                                    $temp = $variation->get_sale_price();
                                    $new_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                 else : 
                                    $new_price = $variation->get_sale_price();
                                   endif; 
                                   $var_price = number_format($new_price, 2, ".", " ");
                                    echo $var_price;
                                ?>
					            <span class="currency">₴</span> за шт.
								</li>
                                <?php 
                                 if ( wp_get_current_user()->data->user_type == '1' ) : 
                                    $temp = $variation->get_regular_price();
                                    $old_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                 else : 
                                    $old_price = $variation->get_regular_price();
                                   endif; 
                                  $var_price = number_format($old_price, 2, ".", " ");
                                ?>
								<li class="price-old"><?php echo $var_price; ?><span class="currency"> ₴</span></li>
							<?php else : ?>
								<li class="price-new">
								<?php 
                                $var_price = number_format($variation->get_price(), 2, ".", " ");
                                echo $var_price;
                                ?><span class="currency">₴</span> за шт.
								</li>
							<?php endif; ?>
						</ul>
						</div>
						</div>
						<div class="cart__product-remove" data-cartID="<?php echo $item; ?>">
						<i>
						<svg height="20px" width="18px">
							<use href="<?=TEMPLATE_PATH?>static/svg-remove.svg#remove"></use>
						</svg>
						</i>
						</div>
						</div>
					</li>
				<?php $count++; ?>
				<?php endforeach; ?>

			</ul>
			<div class="cart__total">
				<div class="cart__total-price">
                    <p>
                      <?php echo $cart_total[0]; ?><span>.<?php echo $cart_total[1]; ?> </span><span class="currency">₴ </span><span class="price-text">
                      <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            всього
                        <?php else : ?>
                            итого
                        <?php endif; ?>
                      </span>
                    </p>
				</div>
				<div class="cart__total-order">
					<a href="<?php echo wc_get_checkout_url(); ?>" class="btn btn-colored">
					<?php if ( wpml_get_current_language() == 'uk' ): ?>
                        Оформити замовлення
                    <?php else : ?>
                        Оформить заказ
                    <?php endif; ?>
					</a>
				</div>
			</div>
        <?php else : ?>
            <div class="cart__title">
			<h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
			</div>
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <p>В кошику поки немає товарів</p>
            <?php else : ?>
                <p>В корзине пока нет товаров</p>
            <?php endif; ?>
        <?php endif; 
        die;
	}


    public static function delete_cart_item() {
        $cart_item_key = $_POST['cardID'];
        WC()->cart->remove_cart_item( $cart_item_key );
        $cart_item = WC()->cart->get_cart();
        if ( ! WC()->cart->prices_include_tax ) {
            $cart_total = WC()->cart->cart_contents_total;
        } else {
            $cart_total = WC()->cart->cart_contents_total + WC()->cart->tax_total;
        }
        $cart_total = explode('.' , number_format($cart_total, 2, ".", " "));
        if ( !empty( $cart_item ) ) : ?>
        <div class="cart__title">
			<h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
			</div>
			<ul class="cart__list">
				<?php $count = 1; ?>
				<?php foreach ( $cart_item as $item => $values ) : 
					$variation = wc_get_product( $values['data']->get_id() ); 
                    $product = wc_get_product( $variation->get_parent_id() );
                    $attributes =  $variation->get_variation_attributes() ;
                    $obem = get_term_by('slug',  $attributes['attribute_pa_obem'], 'pa_obem')->name;
                    $color = get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->name;
                    $line_price = number_format($values['line_total'], 2, ".", " ");
                    $price = explode('.' , $line_price);
                    ?>
					<li class="cart__list-item">
						<div class="cart__product">
						<div class="cart__product-position">
						<span><?php echo $count; ?></span>
						</div>
						<picture>
						<img src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" alt="">
						</picture>
						<div class="cart__product-title">
                            <a href="<?php echo get_permalink( $product->get_id() ); ?>" target="_blank"><?php echo $product->get_title(); ?></a>
						</div>
						<div class="cart__product-info">
						<div class="cart__product-info_size"><?php echo $obem; ?></div>
						<div class="cart__product-info_color" style="background-color: <?php echo get_field( 'color', 'term_'.get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->term_id );?>"></div>
						<div class="cart__product-info_name"><?php echo $color; ?></div>
						<i>
						<svg height="24px" width="24px">
							<use href="<?=TEMPLATE_PATH?>tatic/svg-edit.svg#edit"></use>
						</svg>
						</i>
						</div>
						<div class="cart__product-count">
						<div class="product__order-counter">
						<button class="counter-button" data-value="minus">
							<svg width="14px" height="2px">
							<use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#minus"></use>
							</svg>
						</button>
						<input data-value="display" type="number" onkeyup="this.value = this.value.replace (/\D/, '')" min="0" value="<?php echo $values['quantity']?>">
						<button class="counter-button" data-value="plus">
							<svg width="14px" height="14px">
							<use href="<?=TEMPLATE_PATH?>static/svg-plus-minus.svg#plus"></use>
							</svg>
						</button>
						</div>
						</div>
						<div class="cart__product-summary">
						<div class="cart__product-summary_total">
                            <p>
                              <?php echo $price[0]; ?><span>.<?php echo $price[1]; ?></span><span> </span><span class="currency">₴</span>
                            </p>
						</div>
						<div class="cart__product-summary_price">
						<ul>
							<?php if ( $variation->is_on_sale() ) : ?>
								<li class="price-new">
                                <?php 
                                 if ( wp_get_current_user()->data->user_type == '1' ) : 
                                    $temp = $variation->get_sale_price();
                                    $new_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                 else : 
                                    $new_price = $variation->get_sale_price();
                                   endif; 
                                   $var_price = number_format($new_price, 2, ".", " ");
                                    echo $var_price;
                                ?>
								<span class="currency">₴</span> за шт.
								</li>
                                <?php 
                                 if ( wp_get_current_user()->data->user_type == '1' ) : 
                                    $temp = $variation->get_regular_price();
                                    $old_price = $temp - ( $temp / 100 * get_field( 'b2b_sale', 'option' ) );
                                 else : 
                                    $old_price = $variation->get_regular_price();
                                   endif; 
                                  $var_price = number_format($old_price, 2, ".", " ");
                                ?>
								<li class="price-old"><?php echo $var_price; ?><span class="currency"> ₴</span></li>
							<?php else : ?>
								<li class="price-new">
                                    
								<?php 
                                $var_price = number_format($variation->get_price(), 2, ".", " ");
                                echo $var_price;
                                ?> <span class="currency">₴</span> за шт.
								</li>
							<?php endif; ?>
						</ul>
						</div>
						</div>
						<div class="cart__product-remove" data-cartID="<?php echo $values['key'];?>">
						<i>
						<svg height="20px" width="18px">
							<use href="<?=TEMPLATE_PATH?>static/svg-remove.svg#remove"></use>
						</svg>
						</i>
						</div>
						</div>
					</li>
				<?php $count++; ?>
				<?php endforeach; ?>

			</ul>
			<div class="cart__total">
				<div class="cart__total-price">
                    <p>
                      <?php echo $cart_total[0]; ?><span>.<?php echo $cart_total[1]; ?> </span><span class="currency">₴ </span><span class="price-text">
                      <?php if ( wpml_get_current_language() == 'uk' ): ?>
                            всього
                        <?php else : ?>
                            итого
                        <?php endif; ?>
                      </span>
                    </p>
				</div>
				<div class="cart__total-order">
					<a href="<?php echo wc_get_checkout_url(); ?>" class="btn btn-colored">
					<?php if ( wpml_get_current_language() == 'uk' ): ?>
                        Оформити замовлення
                    <?php else : ?>
                        Оформить заказ
                    <?php endif; ?>
					</a>
				</div>
			</div>
        <?php else : ?>
            <div class="cart__title">
			<h1><?php echo get_field('header','option')['tekst_korzina']?></h1>
			</div>
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <p>В кошику поки немає товарів</p>
            <?php else : ?>
                <p>В корзине пока нет товаров</p>
            <?php endif; ?>
        <?php endif; 
        die;
    }

    public static function count_update() {
        $cart_count = WC()->cart->get_cart_contents_count();
        $cart_total = WC()->cart->get_cart_total(); ?>
        <?php if ( $cart_count != 0 ) : ?>
            <div class="cart-count">
                <p><?php echo $cart_count; ?></p>
            </div>
        <?php endif; ?>
    <?php die;
    }

    public static function mini_cart_update() {
        $cart_item = WC()->cart->get_cart();
        $cart_count = WC()->cart->get_cart_contents_count();
      if ( ! WC()->cart->prices_include_tax ) {
        $cart_total = WC()->cart->cart_contents_total;
      } else {
        $cart_total = WC()->cart->cart_contents_total + WC()->cart->tax_total;
      }
      $cart_total = explode('.' , number_format($cart_total, 2, ".", " "));
        if ( !empty( $cart_item ) ) : ?>
        <ul class="cart-popup__list">
        <?php foreach ( $cart_item as $item => $values ) :
        $variation = wc_get_product( $values['data']->get_id() ); 
        $product = wc_get_product( $variation->get_parent_id() );?>
        <?php 
        $attributes =  $variation->get_variation_attributes() ;
        $obem = get_term_by('slug',  $attributes['attribute_pa_obem'], 'pa_obem')->name;
        $color = get_term_by('slug',  $attributes['attribute_pa_czvet'], 'pa_czvet')->name;
          $line_price = number_format($values['line_total'], 2, ".", " ");
          $price = explode('.' , $line_price);
        ?>
          <li class="cart-popup__product">
            <picture>
              <img src="<?php echo get_the_post_thumbnail_url( $product->get_id() ); ?>" alt="">
            </picture>
            <div class="cart-popup__product-info">
              <div class="cart-popup__product-title">
                <p><?php echo $product->get_title(); ?></p>
              </div>
              <div class="cart-popup__product-details">
                <p>
                  <?php echo $obem; ?>
                  <span><?php echo $color; ?></span>
                  <span><?php echo $values['quantity']; ?> шт</span>
                </p>
              </div>
            </div>
            <div class="cart-popup__product-total">
              <p><?php echo $price[0]; ?><span>.<?php echo $price[1]; ?></span><span class="currency"> ₴</span></p>
            </div>
          </li>
        <?php endforeach; ?>
        </ul>
        <div class="cart-popup__bottom">
          <div class="cart-popup__link">
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <a class="btn btn-colored" href="<?php echo wc_get_cart_url(); ?>">Замовлення</a>
            <?php else : ?>
                <a class="btn btn-colored" href="<?php echo wc_get_cart_url(); ?>">К заказу</a>
            <?php endif; ?>
          </div>
            <div class="cart-popup__total">
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <span class="cart-popup__total-text">на суму</span>
            <?php else : ?>
                <span class="cart-popup__total-text">на сумму</span>
            <?php endif; ?>
                <p class="cart-popup__total-price">
                  <?php echo $cart_total[0]; ?><span>.<?php echo $cart_total[1]; ?> </span><span class="currency">₴</span>
                </p>
            </div>
        </div>
        <?php else : ?>
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <p>В кошику поки немає товарів</p>
            <?php else : ?>
                <p>В корзине пока нет товаров</p>
            <?php endif; ?>
        <?php endif; ?>
        <?php if ( $_POST['is_product'] == 'true' ) : ?>
            <script>
                jQuery('.popup-cart').addClass('show');
            </script>
        <?php endif; ?>
        <?php die;
    } 


    public static function acf_add_options_page() {
        if ( ! function_exists('acf_add_options_page') ) return;

        acf_add_options_page( [
            'page_title' => 'Настройки сайта',
            'menu_title' => 'Настройки сайта',
            'menu_slug' => 'theme-options',
            'redirect' => false,
        ]);

    }

    public static function acf_add_local_field_group() {

        /** Autogenerated by ACF */
        if ( ! function_exists('acf_add_local_field_group') ) return;

        acf_add_local_field_group(array(
            'key' => 'group_625346cd184e2',
            'title' => 'Футер и хедер',
            'fields' => array(
                array(
                    'key' => 'field_6257d1c58c14d',
                    'label' => 'Хедер',
                    'name' => '',
                    'type' => 'tab',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'placement' => 'top',
                    'endpoint' => 0,
                ),
                array(
                    'key' => 'field_6257d1d08c14e',
                    'label' => '',
                    'name' => 'header',
                    'type' => 'group',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'layout' => 'block',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_625345cd50c6a',
                            'label' => 'Логотип',
                            'name' => 'logotype',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'url',
                            'preview_size' => 'medium',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array(
                            'key' => 'field_625345fc50c6b',
                            'label' => 'Меню',
                            'name' => 'menu',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_6253460850c6c',
                                    'label' => 'Ссылка',
                                    'name' => 'link',
                                    'type' => 'link',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'array',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_6253462150c6d',
                            'label' => 'Ссылка на каталог',
                            'name' => 'shop_link',
                            'type' => 'link',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                        ),
                        array(
                            'key' => 'field_6253464250c6e',
                            'label' => 'Соцсети',
                            'name' => 'social',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_6253466d50c6f',
                                    'label' => 'Иконка',
                                    'name' => 'icons',
                                    'type' => 'image',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'url',
                                    'preview_size' => 'medium',
                                    'library' => 'all',
                                    'min_width' => '',
                                    'min_height' => '',
                                    'min_size' => '',
                                    'max_width' => '',
                                    'max_height' => '',
                                    'max_size' => '',
                                    'mime_types' => '',
                                ),
                                array(
                                    'key' => 'field_6253468050c70',
                                    'label' => 'Ссылка',
                                    'name' => 'link',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => '',
                                ),
                            ),
                        ),
                    ),
                ),
                array(
                    'key' => 'field_6257d0e008505',
                    'label' => 'Футер',
                    'name' => '',
                    'type' => 'tab',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'placement' => 'top',
                    'endpoint' => 0,
                ),
                array(
                    'key' => 'field_6257d0fe08506',
                    'label' => '',
                    'name' => 'footer',
                    'type' => 'group',
                    'instructions' => '',
                    'required' => 0,
                    'conditional_logic' => 0,
                    'wrapper' => array(
                        'width' => '',
                        'class' => '',
                        'id' => '',
                    ),
                    'layout' => 'block',
                    'sub_fields' => array(
                        array(
                            'key' => 'field_625346db8b206',
                            'label' => 'Логотип',
                            'name' => 'logotype',
                            'type' => 'image',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'url',
                            'preview_size' => 'medium',
                            'library' => 'all',
                            'min_width' => '',
                            'min_height' => '',
                            'min_size' => '',
                            'max_width' => '',
                            'max_height' => '',
                            'max_size' => '',
                            'mime_types' => '',
                        ),
                        array(
                            'key' => 'field_625346ed8b207',
                            'label' => 'Текст под логотипом',
                            'name' => 'logotype_text',
                            'type' => 'textarea',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'maxlength' => '',
                            'rows' => '',
                            'new_lines' => '',
                        ),
                        array(
                            'key' => 'field_625347018b208',
                            'label' => 'Меню',
                            'name' => 'menu',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_6253470c8b209',
                                    'label' => 'Ссылка',
                                    'name' => 'link',
                                    'type' => 'link',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'array',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_625347358b20a',
                            'label' => 'Телефоны',
                            'name' => 'phones',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_625347488b20b',
                                    'label' => 'Телефон',
                                    'name' => 'phone',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => '',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_625347548b20c',
                            'label' => 'Эмейлы',
                            'name' => 'emails',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_625347548b20d',
                                    'label' => 'Эмейл',
                                    'name' => 'email',
                                    'type' => 'text',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'default_value' => '',
                                    'placeholder' => '',
                                    'prepend' => '',
                                    'append' => '',
                                    'maxlength' => '',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_6253488fbc521',
                            'label' => 'Соцсети',
                            'name' => 'socials',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_6253489ebc522',
                                    'label' => 'Иконка',
                                    'name' => 'icon',
                                    'type' => 'image',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'url',
                                    'preview_size' => 'medium',
                                    'library' => 'all',
                                    'min_width' => '',
                                    'min_height' => '',
                                    'min_size' => '',
                                    'max_width' => '',
                                    'max_height' => '',
                                    'max_size' => '',
                                    'mime_types' => '',
                                ),
                                array(
                                    'key' => 'field_625348acbc523',
                                    'label' => 'Ссылка',
                                    'name' => 'link',
                                    'type' => 'link',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'array',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_625348d3dbafc',
                            'label' => 'Ссылка пользовательского соглашение',
                            'name' => 'terms_of_use',
                            'type' => 'link',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                        ),
                        array(
                            'key' => 'field_625348f5dbafd',
                            'label' => 'Ссылка политики конфиденциальности',
                            'name' => 'privacy_policy',
                            'type' => 'link',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'return_format' => 'array',
                        ),
                        array(
                            'key' => 'field_62534921dbafe',
                            'label' => 'Иконки',
                            'name' => 'icons',
                            'type' => 'repeater',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'collapsed' => '',
                            'min' => 0,
                            'max' => 0,
                            'layout' => 'table',
                            'button_label' => '',
                            'sub_fields' => array(
                                array(
                                    'key' => 'field_6253492edbaff',
                                    'label' => 'Иконка',
                                    'name' => 'icon',
                                    'type' => 'image',
                                    'instructions' => '',
                                    'required' => 0,
                                    'conditional_logic' => 0,
                                    'wrapper' => array(
                                        'width' => '',
                                        'class' => '',
                                        'id' => '',
                                    ),
                                    'return_format' => 'url',
                                    'preview_size' => 'medium',
                                    'library' => 'all',
                                    'min_width' => '',
                                    'min_height' => '',
                                    'min_size' => '',
                                    'max_width' => '',
                                    'max_height' => '',
                                    'max_size' => '',
                                    'mime_types' => '',
                                ),
                            ),
                        ),
                        array(
                            'key' => 'field_625349451cd2b',
                            'label' => 'Копирайт',
                            'name' => 'copyright',
                            'type' => 'text',
                            'instructions' => '',
                            'required' => 0,
                            'conditional_logic' => 0,
                            'wrapper' => array(
                                'width' => '',
                                'class' => '',
                                'id' => '',
                            ),
                            'default_value' => '',
                            'placeholder' => '',
                            'prepend' => '',
                            'append' => '',
                            'maxlength' => '',
                        ),
                    ),
                ),
            ),
            'location' => array(
                array(
                    array(
                        'param' => 'options_page',
                        'operator' => '==',
                        'value' => 'theme-options',
                    ),
                ),
            ),
            'menu_order' => 0,
            'position' => 'normal',
            'style' => 'default',
            'label_placement' => 'top',
            'instruction_placement' => 'label',
            'hide_on_screen' => '',
            'active' => true,
            'description' => '',
            'show_in_rest' => 0,
        ));
        
        
    }
}
