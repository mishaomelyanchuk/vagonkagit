<?php

class Catalog_Section {
    public function __construct() {
       $this->terms = get_terms( array(
            'taxonomy' => 'product_cat',
            'hide_empty' => true,
        ) );

        $this->product_tag = get_terms( array(
            'taxonomy' => 'product_tag',
            'hide_empty' => true,
        ) );

        $this->color = get_terms( array(
            'taxonomy' => 'color_palitra',
            'hide_empty' => true,
        ) );

     
    }

    public function render() { ?>
        <div class="catalog">
            <div class="container">
                <section class="section__outer">
                <section class="section__inner">
                    <div class="catalog__wrapper">
                    <div class="catalog__title">
                        <h1><?php echo get_field('catalog','option')['tekst_katalog_produkczii']?></h1>
                    </div>
                    <div class="catalog__filters">
                        <div class="catalog__filters-toggle">
                        <span class="btn btn-filter"><?php echo get_field('catalog','option')['tekst_filtr']?></span>
                        </div>
                        <div class="catalog__filters-wrapper">
                        <form id="filters">
                            <?php if ( !empty( $this->terms ) ) : ?>
                            <select name="categories">
                            <option value=""><?php echo get_field('catalog','option')['tekst_po_kategorii']?></option>
                            <?php foreach ( $this->terms as $item ) : ?>
                                <option value="<?php echo $item->slug; ?>"><?php echo $item->name; ?></option>
                            <?php endforeach; ?>
                            </select>
                            <?php endif; ?>
                            <?php if ( !empty( $this->product_tag ) ) : ?>
                            <select name="use">
                            <?php if ( !empty( $_GET['prod_tag'] ) ) : ?>
                              <option value=""><?php echo get_field('catalog','option')['tekst_po_primeneniyu']?></option>
                                <?php $prod_tag = get_term_by( 'slug', $_GET['prod_tag'], 'product_tag' )?>
                                <option value="<?php echo $prod_tag->slug; ?>" selected><?php echo $prod_tag->name; ?></option>
                                <?php foreach ( $this->product_tag as $item ) : ?>
                                    <?php if ( $prod_tag->slug != $item->slug ) : ?>
                                    <option value="<?php echo $item->slug; ?>"><?php echo $item->name; ?></option>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php else : ?>
                                <option value=""><?php echo get_field('catalog','option')['tekst_po_primeneniyu']?></option>
                                <?php foreach ( $this->product_tag as $item ) : ?>
                                    <option value="<?php echo $item->slug; ?>"><?php echo $item->name; ?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                           
                            </select>
                            <?php endif; ?>
                            <?php if ( !empty( $this->color ) ) : ?>
                            <select name="color">
                            <option value=""><?php echo get_field('catalog','option')['tekst_po_czvetu']?></option>
                            <?php foreach ( $this->color as $item ) : ?>
                                <?php echo get_fields( $item->term_id ); ?>
                                <option value="<?php echo $item->slug; ?>" data-color="<?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></option>
                            <?php endforeach; ?>
                            </select>
                            <?php endif; ?>
                            <div class="actions">
                            <button class="btn btn-filter" type="submit">
                                <?php echo get_field('catalog','option')['tekst_filtrovat']?>
                            </button>
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                <span id="form-reset"  <?php echo ( !empty( $_GET['prod_tag'] ) ) ?  'style="display: block;"' : '';?>>Скинути</span>
                            <?php else : ?>
                                <span id="form-reset"  <?php echo ( !empty( $_GET['prod_tag'] ) ) ?  'style="display: block;"' : '';?>>Сбросить</span>
                            <?php endif; ?>
                            </div>
                        </form>
                        </div>
                    </div>

                    <!-- <div class="catalog__items catalog__items-promo">
                            <div class="catalog-item" data-product-path="/product.html?id=1">
                                <div class="catalog-item__wrapper">
                                    <div class="catalog-item__promo">
                                    <div><span>АКЦИЯ</span></div>
                                    </div>
                                    <div class="catalog-item__title">
                                    <h3>Эмаль ПФ-133 «Дніпровська вагонка»</h3>
                                    </div>
                                    <picture>
                                    <img src="../static/product-test.png" alt="">
                                    <div class="catalog-item__video">
                                        <video data-color-video="red" autoplay muted>
                                        <source src="../static/videoplayback.mp4">
                                        </video>
                                    </div>
                                    </picture>
                                    <div class="catalog-item__sizes">
                                    <span>2,5 л / 0,85 л </span>
                                    </div>
                                    <div class="catalog-item__color">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li data-color="red" style="background: linear-gradient(
                                            151.19deg,
                                            #fbd50d 8.87%,
                                            #ffe073 50.45%,
                                            #734500 92.9%
                                            );"></li>
                                        <li data-color="red" style="background-color: #89441e"></li>
                                    </ul>
                                    </div>
                                    <div class="catalog-item__btn">
                                    <a href="/product.html">
                                        <button class="btn btn-colored">Выбрать</button>
                                        <span class="btn btn-small">от 137 <i class="currency"> ₴</i></span>
                                    </a>
                                    </div>
                                    <div class="catalog-item__description">
                                    <p>
                                        Эмаль с молотковым эффектом образует на различных поверхностях рельефную
                                        структуру с ярко выраженными прожилками, визуально маскирует и
                                        выравнивает неровности поверхностей (места стыков, локально
                                        отремонтированные места, окалины и т.д.).
                                    </p>
                                    </div>
                                </div>
                            </div>

                            <div class="catalog-item" data-product-path="/product.html?id=1">
                                <div class="catalog-item__wrapper">
                                    <div class="catalog-item__promo">
                                    <div><span>АКЦИЯ</span></div>
                                    </div>
                                    <div class="catalog-item__title">
                                    <h3>Эмаль ПФ-133 «Дніпровська вагонка»</h3>
                                    </div>
                                    <picture>
                                    <img src="../static/product-test.png" alt="">
                                    <div class="catalog-item__video">
                                        <video data-color-video="red" autoplay muted>
                                        <source src="../static/videoplayback.mp4">
                                        </video>
                                    </div>
                                    </picture>
                                    <div class="catalog-item__sizes">
                                    <span>2,5 л / 0,85 л </span>
                                    </div>
                                    <div class="catalog-item__color">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li data-color="red" style="background: linear-gradient(
                                            151.19deg,
                                            #fbd50d 8.87%,
                                            #ffe073 50.45%,
                                            #734500 92.9%
                                            );"></li>
                                        <li data-color="red" style="background-color: #89441e"></li>
                                    </ul>
                                    </div>
                                    <div class="catalog-item__btn">
                                    <a href="/product.html">
                                        <button class="btn btn-colored">Выбрать</button>
                                        <span class="btn btn-small">от 137 <i class="currency"> ₴</i></span>
                                    </a>
                                    </div>
                                    <div class="catalog-item__description">
                                    <p>
                                        Эмаль с молотковым эффектом образует на различных поверхностях рельефную
                                        структуру с ярко выраженными прожилками, визуально маскирует и
                                        выравнивает неровности поверхностей (места стыков, локально
                                        отремонтированные места, окалины и т.д.).
                                    </p>
                                    </div>
                                </div>
                            </div>

                    </div> -->
                    <div class="catalog__items">
                    <?php if ( empty( $_GET['prod_tag'] ) ) : ?>
                        <?php if ( !empty( $this->terms ) ) : ?>
                        <?php $start_terms = array_slice( $this->terms, 0, 2 ); 
                        $start_slug = [];
                        foreach ( $start_terms as $item ) :
                            $start_slug[] = $item->slug;
                        endforeach; 
                        $products = new WP_Query( 
                            array( 
                                'post_type' => 'product',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field'    => 'slug',
                                        'terms'    => $start_slug,
                                    ),
                                 ) 
                             )
                        );
                        unset( $this->terms[0] );
                        unset( $this->terms[1] );
                        ?>
                          <?php if ( $products->have_posts() ) : ?>
                                <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                                <?php  $product = wc_get_product( get_the_ID() ); ?>
                                    <div class="catalog-item" data-product-path="<?php echo get_the_permalink(); ?>">
                                        <div class="catalog-item__wrapper">
                                            <div class="catalog-item__promo">
                                            <?php 
                                            if ( !empty( get_field( 'sale_flash', $product->id ) ))  : ?> 
                                                <div><span><?php echo get_field( 'sale_flash', $product->id ); ?></span></div>
                                            <?php endif; 
                                            ?>
                                            </div>
                                            <div class="catalog-item__title">
                                            <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                                            </div>
                                            <picture>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                            <?php 
                                            $colors = woocommerce_get_product_terms($product->id, 'pa_czvet');
                                            ?>
                                            <?php if ( !empty( $colors ) ) : ?>
                                            <div class="catalog-item__video">
                                                <?php foreach ( $colors as $item ) : ?>
                                                    <video data-color-video="<?php echo $item->slug; ?>" loop muted>
                                                    <source src="<?php echo get_field( 'video', 'term_'.$item->term_id )['url']; ?>">
                                                    </video>
                                                <?php endforeach?>
                                            </div>
                                            <?php endif; ?>
                                            </picture>
                                            <?php 
                                            $obem = woocommerce_get_product_terms($product->id, 'pa_obem', 'names');
                                            ?>
                                            <?php if ( !empty( $obem ) ) : ?>
                                            <div class="catalog-item__sizes">
                                            <span>
                                                <?php $count = 0;?>
                                                <?php foreach ( $obem as $item ) : ?>
                                                    <?php $count++; ?>
                                                    <?php if ( $count == count($obem) ) : ?>
                                                        <?php echo $item;?>
                                                    <?php else : ?>
                                                        <?php echo $item . ' /';?>
                                                    <?php endif; ?>
                                                <?php endforeach ; ?>
                                            </span>
                                            </div>
                                            <?php endif; ?>
                                            <?php if ( !empty( $colors ) ) : 
                                            $available_variations = $product->get_available_variations();
                                            foreach ($colors as $color) :
                                                $on = true;
                                                foreach ( $available_variations as $item ) :
                                                    if ( $item['attributes']['attribute_pa_czvet'] == $color->slug ) :
                                                        $on = true;
                                                        break;
                                                    else :
                                                        $on = false;
                                                    endif; 
                                                endforeach;
                                                if ( !$on ) :
                                                    $disabled[] = $color->slug;
                                                endif; 
                                            endforeach;    
                                            ?>
                                            <div class="catalog-item__color">
                                            <ul>
                                                <?php foreach ( $colors as $item ) : 
                                                    if ( !in_array( $item->slug, $disabled ) ) : ?>
                                                        <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                                    <?php endif; ?>
                                                <?php endforeach?>
                                            </ul>
                                            </div>
                                            <?php endif; ?>
                                            <?php 
                                           $prices = $product->get_variation_prices();
                                           $price = min($prices['price']);
                                           $sale_price = min($prices['sale_price']);
                                           if ( $sale_price < $price ) {
                                               $price = min($prices['price']);
                                           } 
                                           if ( wp_get_current_user()->data->user_type == '1' ) {
                                               if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                                   $price =  $price- ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
                                               }
                                           } else {
                                               $prices = $product->get_variation_prices();
                                               $price = min($prices['price']);
                                               $sale_price = min($prices['sale_price']);
                                               if ( $sale_price < $price ) {
                                                   $price = min($prices['price']);
                                               } 
                                           }
                                            ?>
                                            <div class="catalog-item__btn">
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <button class="btn btn-colored"><?php echo get_field('catalog','option')['tekst_vybrat']?></button>
                                                <span class="btn btn-small"><?php echo get_field('catalog','option')['tekst_ot']?> <?php echo number_format($price, 2, '.', '') ?><i class="currency"> ₴</i></span>
                                            </a>
                                            </div>
                                            <div class="catalog-item__description">
                                               <?php the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        <?php foreach ( $this->terms as $item ) : ?>
                            <div class="catalog__items-title">
                                <h2><?php echo $item->name; ?></h2>
                            </div>
                            <?php $products = new WP_Query( 
                                array( 
                                    'post_type' => 'product',
                                    'tax_query' => array(
                                        array(
                                            'taxonomy' => 'product_cat',
                                            'field'    => 'slug',
                                            'terms'    => $item->slug,
                                        ),
                                     ) 
                                 )
                            );
                            ?>
                            <?php if ( $products->have_posts() ) : ?>
                                <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                                <?php  $product = wc_get_product( get_the_ID() ); ?>
                                    <div class="catalog-item" data-product-path="<?php echo get_the_permalink(); ?>">
                                        <div class="catalog-item__wrapper">
                                            <div class="catalog-item__promo">
                                            <?php 
                                            if ( !empty( get_field( 'sale_flash', $product->id ) ))  : ?> 
                                                <div><span><?php echo get_field( 'sale_flash', $product->id ); ?></span></div>
                                            <?php endif; 
                                            ?>
                                            </div>
                                            <div class="catalog-item__title">
                                            <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                                            </div>
                                            <picture>
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                            <?php 
                                            $colors = woocommerce_get_product_terms($product->id, 'pa_czvet');
                                            ?>
                                            <?php if ( !empty( $colors ) ) : ?>
                                            <div class="catalog-item__video">
                                                <?php foreach ( $colors as $item ) : ?>
                                                    <video data-color-video="<?php echo $item->slug; ?>" loop muted>
                                                    <source src="<?php echo get_field( 'video', 'term_'.$item->term_id )['url']; ?>">
                                                    </video>
                                                <?php endforeach?>
                                            </div>
                                            <?php endif; ?>
                                            </picture>
                                            <?php 
                                            $obem = woocommerce_get_product_terms($product->id, 'pa_obem', 'names');
                                            ?>
                                            <?php if ( !empty( $obem ) ) : ?>
                                            <div class="catalog-item__sizes">
                                            <span>
                                                <?php $count = 0;?>
                                                <?php foreach ( $obem as $item ) : ?>
                                                    <?php $count++; ?>
                                                    <?php if ( $count == count($obem) ) : ?>
                                                        <?php echo $item;?>
                                                    <?php else : ?>
                                                        <?php echo $item . ' /';?>
                                                    <?php endif; ?>
                                                <?php endforeach ; ?>
                                            </span>
                                            </div>
                                            <?php endif; ?>
                                            <?php if ( !empty( $colors ) ) : 
                                            $available_variations = $product->get_available_variations();
                                            foreach ($colors as $color) :
                                                $on = true;
                                                foreach ( $available_variations as $item ) :
                                                    if ( $item['attributes']['attribute_pa_czvet'] == $color->slug ) :
                                                        $on = true;
                                                        break;
                                                    else :
                                                        $on = false;
                                                    endif; 
                                                endforeach;
                                                if ( !$on ) :
                                                    $disabled[] = $color->slug;
                                                endif; 
                                            endforeach;       
                                            ?>
                                            <div class="catalog-item__color">
                                            <ul>
                                                <?php foreach ( $colors as $item ) : 
                                                    if ( !in_array( $item->slug, $disabled ) ) : ?>
                                                        <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                                    <?php endif; ?>
                                                <?php endforeach?>
                                            </ul>
                                            </div>
                                            <?php endif; ?>
                                            <?php 
                                           $prices = $product->get_variation_prices();
                                           $price = min($prices['price']);
                                           $sale_price = min($prices['sale_price']);
                                           if ( $sale_price < $price ) {
                                               $price = min($prices['price']);
                                           } 
                                           if ( wp_get_current_user()->data->user_type == '1' ) {
                                               if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                                   $price =  $price- ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
                                               }
                                           } else {
                                               $prices = $product->get_variation_prices();
                                               $price = min($prices['price']);
                                               $sale_price = min($prices['sale_price']);
                                               if ( $sale_price < $price ) {
                                                   $price = min($prices['price']);
                                               } 
                                           }
                                            ?>
                                            <div class="catalog-item__btn">
                                            <a href="<?php echo get_the_permalink(); ?>">
                                                <button class="btn btn-colored"><?php echo get_field('catalog','option')['tekst_vybrat']?></button>
                                                <span class="btn btn-small"><?php echo get_field('catalog','option')['tekst_ot']?> <?php echo number_format($price, 2, '.', '') ?><i class="currency"> ₴</i></span>
                                            </a>
                                            </div>
                                            <div class="catalog-item__description">
                                               <?php the_excerpt(); ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php endwhile; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php else : 
                        $use = $_GET['prod_tag'];

                        $args =  array(
                            'post_type'   => 'product',
                        );
                
                        if ( !empty( $use ) ) {
                            $args['tax_query'][] = [
                                'taxonomy' => 'product_tag',
                                'field' => 'slug',
                                'terms' => $use,
                                'operator' => 'IN'
                            ];
                        }


                        $products = new WP_Query( $args );

                        if ( $products->have_posts() ) : ?>
                            <?php while ( $products->have_posts() ) : $products->the_post(); ?>
                            <?php  $product = wc_get_product( get_the_ID() ); ?>
                                <div class="catalog-item" data-product-path="<?php echo get_the_permalink(); ?>">
                                    <div class="catalog-item__wrapper">
                                        <div class="catalog-item__promo">
                                        <?php 
                                        if ( !empty( get_field( 'sale_flash', $product->id ) ) )  : ?> 
                                            <div><span><?php echo get_field( 'sale_flash', $product->id ); ?></span></div>
                                        <?php endif; 
                                        ?>
                                        </div>
                                        <div class="catalog-item__title">
                                        <a href="<?php echo get_the_permalink(); ?>"><h3><?php echo get_the_title(); ?></h3></a>
                                        </div>
                                        <picture>
                                        <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                        <?php 
                                            $colors = woocommerce_get_product_terms($product->id, 'pa_czvet');
                                            ?>
                                            <?php if ( !empty( $colors ) ) : ?>
                                            <div class="catalog-item__video">
                                                <?php foreach ( $colors as $item ) : ?>
                                                    <video data-color-video="<?php echo $item->slug; ?>" loop muted>
                                                    <source src="<?php echo get_field( 'video', 'term_'.$item->term_id )['url']; ?>">
                                                    </video>
                                                <?php endforeach?>
                                            </div>
                                            <?php endif; ?>
                                        </picture>
                                        <?php 
                                        $obem = woocommerce_get_product_terms($product->id, 'pa_obem', 'names');
                                        ?>
                                        <?php if ( !empty( $obem ) ) : ?>
                                        <div class="catalog-item__sizes">
                                        <span>
                                            <?php $count = 0;?>
                                            <?php foreach ( $obem as $item ) : ?>
                                                <?php $count++; ?>
                                                <?php if ( $count == count($obem) ) : ?>
                                                    <?php echo $item;?>
                                                <?php else : ?>
                                                    <?php echo $item . ' /';?>
                                                <?php endif; ?>
                                            <?php endforeach ; ?>
                                        </span>
                                        </div>
                                        <?php endif; ?>
                                        <?php if ( !empty( $colors ) ) : 
                                        $available_variations = $product->get_available_variations();
                                        foreach ($colors as $color) :
                                            $on = true;
                                            foreach ( $available_variations as $item ) :
                                                if ( $item['attributes']['attribute_pa_czvet'] == $color->slug ) :
                                                    $on = true;
                                                    break;
                                                else :
                                                    $on = false;
                                                endif; 
                                            endforeach;
                                            if ( !$on ) :
                                                $disabled[] = $color->slug;
                                            endif; 
                                        endforeach;      
                                        ?>
                                            <div class="catalog-item__color">
                                            <ul>
                                                <?php foreach ( $colors as $item ) : 
                                                if ( !in_array( $item->slug, $disabled ) ) : ?>
                                                <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li>
                                                <?php //if ( !empty( $color ) ) : ?>
                                                    <?php //if ( get_field( 'color_category', 'term_'.$item->term_id )[0]->slug == $color ) : ?>
                                                        <!-- <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li> -->
                                                    <?php //else : ?>
                                                        <!-- <li></li> -->
                                                    <?php //endif; ?>
                                                <?php //else : ?>
                                                    <!-- <li data-color="<?php echo $item->slug; ?>" style="background-color: <?php echo get_field( 'color', 'term_'.$item->term_id ); ?>"></li> -->
                                                <?php //endif; ?>
                                                <?php endif; ?>
                                                <?php endforeach?>
                                            </ul>
                                            </div>
                                        <?php endif; ?>
                                        <?php 
                                        $prices = $product->get_variation_prices();
                                        $price = min($prices['price']);
                                        $sale_price = min($prices['sale_price']);
                                        if ( $sale_price < $price ) {
                                            $price = min($prices['price']);
                                        } 
                                        if ( wp_get_current_user()->data->user_type == '1' ) {
                                            if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                                $price = $price - ( $price / 100 * get_field( 'b2b_sale', 'option' ) );
                                            }
                                        } else {
                                            $prices = $product->get_variation_prices();
                                            $price = min($prices['price']);
                                            $sale_price = min($prices['sale_price']);
                                            if ( $sale_price < $price ) {
                                                $price = min($prices['price']);
                                            } 
                                        }
                                        ?>
                                        <div class="catalog-item__btn">
                                        <a href="<?php echo get_the_permalink(); ?>">
                                            <button class="btn btn-colored"><?php echo get_field('catalog','option')['tekst_vybrat']?></button>
                                            <span class="btn btn-small"><?php echo get_field('catalog','option')['tekst_ot']?> <?php echo number_format($price, 2, '.', '') ?> <i class="currency"> ₴</i></span>
                                        </a>
                                        </div>
                                        <div class="catalog-item__description">
                                        <p>
                                        <?php the_excerpt(); ?>
                                        </p>
                                        </div>
                                    </div>
                                </div>
                            <?php endwhile; ?>
                        
                        <?php endif; 
                    endif; ?>
                    </div>
                    </div>
                </section>
                </section>
            </div>
        </div>
    <?php }

}
