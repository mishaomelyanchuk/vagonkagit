<?php

class Page_About_Company_Section {
    public function __construct() {
        $this->hide_section = get_field( 'hide_section' );
        $this->about_section = get_field( 'about_section' );
        $this->color_section = get_field( 'color_section' );
        $this->catalog_link = get_field( 'link' );
    }

    public function render() { ?>
    <div class="company">
      <div class="company-title">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-title__wrapper">
                <div class="company-title__header"><h1><?php echo get_the_title(); ?></h1></div>
                <?php if ( !empty( $this->hide_section['text'] ) ) { ?>
                <div class="company-title__description">
                  <p>
                    <?php echo $this->hide_section['text']; ?>
                  </p>
                </div>
                <?php } ?>
              </div>
            </section>
          </section>
        </div>
      </div>
      <?php if ( !empty( $this->hide_section['slider'] ) ) { ?>
      <div class="company-slider">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-slider__wrapper">
                <?php foreach( $this->hide_section['slider'] as $item ) { ?>
                <div class="company-slider__item">
                  <?php if ( !empty( $item['image'] ) ) { ?>
                  <picture class="company-slider__item-img">
                    <img src="<?php echo $item['image']; ?>" alt="">
                  </picture>
                  <?php } ?>
                  <div class="company-slider__item-text">
                    <?php if ( !empty( $item['number'] ) ) { ?>
                    <h4><?php echo $item['number']; ?></h4>
                    <?php } ?>
                    <?php if ( !empty( $item['text'] ) ) { ?>
                    <p>
                    <?php echo $item['text']; ?>
                    </p>
                    <?php } ?>
                  </div>
                </div>
                <?php } ?>
              </div>
            </section>
          </section>
        </div>
      </div>
      <?php } ?>
      <div class="company-history">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-history__wrapper">
                <?php if ( !empty( $this->hide_section['open_text'] ) ) { ?>
                <div class="company-history__text">
                  <p>
                  <?php echo nl2br($this->hide_section['open_text']); ?>
                  </p>
                </div>
                <?php } ?>
                <?php if ( !empty( $this->hide_section['close_text'] ) ) { ?>
                <div class="company-history__text hidden-text hidden">
                  <p>
                  <?php echo nl2br($this->hide_section['close_text']); ?>
                  </p>
                </div>
                <?php } ?>
                <?php if ( !empty( $this->about_section['history_link'] ) ) { ?>
                <div class="company-history__button">
                  <span><?php echo $this->about_section['history_link']; ?></span>
                </div>
                <?php } ?>
              </div>
            </section>
          </section>
        </div>
      </div>
      <div class="company-manufacture">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-manufacture__wrapper">
                <?php if ( !empty( $this->about_section['title'] ) ) { ?>
                <div class="company-manufacture__title">
                  <h2><?php echo $this->about_section['title']; ?></h2>
                </div>
                <?php } ?>
                <?php if ( !empty( $this->about_section['hide_text'] ) ) { ?>
                <div class="company-manufacture__text-big">
                  <p>
                    <?php echo $this->about_section['hide_text']; ?>
                  </p>
                </div>
                <?php } ?>
                <?php if ( !empty( $this->about_section['down_text'] ) ) { ?>
                <div class="company-manufacture__text-small">
                <?php if ( !empty( $this->about_section['down_image'] ) ) { ?>
                  <img src=" <?php echo $this->about_section['down_image']; ?>" alt="">
                  <?php } ?>
                  <p>
                  <?php echo $this->about_section['down_text']; ?>
                  </p>
                </div>
                <?php } ?>
                <?php if ( !empty( $this->about_section['video'] ) ) { ?>
                <div class="media-block">
                  <div class="media-block-video">
                    <video autoplay="" muted="" loop="">
                      <source src=" <?php echo $this->about_section['video']['url']; ?>">
                    </video>
                  </div>
                </div>
                <?php } ?>
                <!--<div class="media-block-image" style="background-image: url('<?php echo $this->about_section['down_image']; ?>')"></div>-->
              </div>
            </section>
          </section>
        </div>
      </div>
      <div class="company-color">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-color__wrapper">
                <div class="drag-slider">
  <?php if ( !empty( $this->color_section ) ) { ?>
  <div class="drag-slider__wrapper" style="transform: rotate(0)">
    <ul>
    <?php foreach ( $this->color_section as $item ) { ?>
      <li style="background-color: <?php echo $item['color']; ?>">
        <span></span>
      </li>
    <?php } ?>
    </ul>
  </div>

  <div class="drag-slider__limit"></div>
  <div class="drag-slider__pointer"></div>
  <div class="drag-slider__text">
    <p><?php echo !empty($this->about_section["down_carousel_text"]) ? $this->about_section["down_carousel_text"] : ""; ?></p>
  </div>
    </div>

              </div>
            </section>
          </section>
        </div>
      </div>
      <div class="company-images">
        <div class="container">
          <section class="section__outer">
            <section class="section__inner">
              <div class="company-images__wrapper">
                <ul>
                <?php foreach ( $this->color_section as $item ) { ?>
                  <li>
                    <div class="image-wrapper">
                      <div class="image-main">
                        <img src="<?php echo $item['image']; ?>" alt="">
                      </div>
                      <div class="image-second">
                        <img src="<?php echo $item['image']; ?>" alt="">
                      </div>
                      <div class="image-third">
                        <img src="<?php echo $item['image']; ?>" alt="">
                      </div>
                    </div>
                  </li>
                  <?php } ?>
                </ul>
                <?php if ( !empty( $this->catalog_link ) ) { ?>
                <div class="company-images__btn" style="margin-top: 70px;">
                  <a href="<?php echo $this->catalog_link['url']; ?>" class="btn btn-colored"><?php echo $this->catalog_link['title']; ?></a>
                </div>
                <?php } ?>
              </div>
            </section>
          </section>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php }

}
