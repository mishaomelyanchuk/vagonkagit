<?php
class Page_Profile_Account_Header {

    private $user;

    private $lang;

    private $new_posts_count;

    function __construct()
    {
        global $VG_user;
        $this->lang = wpml_get_current_language();
        $this->user = $VG_user->get_current_user();
        $this->new_posts_count = $VG_user->get_count_new_materials();
    }

    public function render() { ?>
        <div class="account__header">
            <div class="account__type business-account">
                <?php if((int)$this->user["user_type"] == 0) { ?>
                    <p class="standart_account"><?php echo get_account_name(); ?></p>
                <?php } else { ?>
                    <p class="b2b_account"><?php echo get_account_name(1); ?></p>
                <?php } ?>
            </div>
            <div class="account__title">
                <h1><?php echo get_post()->post_title; ?></h1>
            </div>
            <nav class="account__pages">
                <ul>
                    <li>
                        <a href="<?php echo ($this->lang == 'ru' ? '/ru' : ''); ?>/orders"><?php echo get_page_by_slug("orders")->post_title; ?></a>
                    </li>
                    <?php if((int)$this->user["user_type"] !== 0) { ?>
                        <li>
                            <a href="<?php echo ($this->lang == 'ru' ? '/ru' : ''); ?>/materials"><?php echo get_page_by_slug("materials")->post_title; ?></a>
                            <?php if($this->new_posts_count > 0) { ?>
                                <div class="account__count">
                                    <span><?php echo $this->new_posts_count; ?></span>
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                </ul>
                <div class="account__pages-toggle">
                    <img src="<?php echo get_template_directory_uri(); ?>/static/svg-arrow-select.svg" alt="">
                </div>
            </nav>
        </div>
    <?php
    }

}