<?php
class Page_Profile_Form_Personal {

    /**
     * @var array|mixed|stdClass
     */
    private $user;

    /**
     * @var mixed
     */
    private $data;

    /**
     * @var bool
     */
    public $error = false;

    function __construct() {
        $this->data = get_field("user_data")[0];
        $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);

        if($action === "save_form_data") {
            $id     = filter_input(INPUT_POST, "customer-id", FILTER_SANITIZE_STRING);
            $f_name = filter_input(INPUT_POST, "customer-fname", FILTER_SANITIZE_STRING);
            $name   = filter_input(INPUT_POST, "customer-name", FILTER_SANITIZE_STRING);
            $email  = filter_input(INPUT_POST, "customer-email", FILTER_SANITIZE_STRING);
            $phone  = filter_input(INPUT_POST, "customer-phone", FILTER_SANITIZE_STRING);

            /** @var $VG_User VG_user */
            global $VG_user;
            $saved = $VG_user->update_user($id, $f_name, $name, $email, $phone);
            $this->error = $saved["res"] != 0;
        }
    }

    public function render()
    {
        global $VG_user;
    ?>
        <div class="form__section section-personal">
            <h3><?php echo $this->data["title"]; ?></h3>

            <div class="form__input input-filled">
                <input id="name" type="text" name="customer-name" placeholder="<?php echo $this->data["name"]; ?>" value="<?php echo $VG_user->fname; ?>">
            </div>
            <div class="form__input input-filled">
                <input id="fname" type="text" name="customer-fname" placeholder="<?php echo $this->data["f_name"]; ?>" value="<?php echo $VG_user->name; ?>">
            </div>
            <div class="form__input input-filled">
                <input id="email" type="email" readonly name="customer-email" placeholder="<?php echo $this->data["email"]; ?>" value="<?php echo $VG_user->email; ?>">
            </div>
            <div class="form__input input-filled">
                <input id="phone" type="tel" name="customer-phone" placeholder="<?php echo $this->data["phone"]; ?>" value="<?php echo $VG_user->phone; ?>">
            </div>
        </div>
    <?php
    }

}