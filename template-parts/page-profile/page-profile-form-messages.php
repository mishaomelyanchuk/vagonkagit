<?php
class Page_Profile_Form_Messages {

    private $data;

    function __construct() {
        $this->data = get_field("messages")[0];
    }

    public function render($name) {
    ?>
        <div class="form-message-wrapper">
            <?php echo $this->data[$name]; ?><br>
        </div>
    <?php
    }

}