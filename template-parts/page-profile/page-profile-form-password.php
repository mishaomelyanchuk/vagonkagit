<?php
class Page_Profile_Form_Password {

    /**
     * @var bool
     */
    public $error = false;

    /**
     * @var mixed
     */
    private $data;

    function __construct() {
        global $VG_user;

        $this->data = get_field("change_password")[0];

        $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);
        if($action === "save_form_data") {
            $old_pass  = filter_input(INPUT_POST, "old_password", FILTER_SANITIZE_STRING);
            $new_pass  = filter_input(INPUT_POST, "new_password", FILTER_SANITIZE_STRING);
            $new_pass1 = filter_input(INPUT_POST, "new_password1", FILTER_SANITIZE_STRING);

            if(!empty($old_pass)) {
                if(wp_login($VG_user->email, $old_pass)) {
                    if($new_pass !== "" && $new_pass === $new_pass1) {
                        wp_set_password($new_pass, $VG_user->id);
                        wp_set_auth_cookie($VG_user->id, true);
                        $user = wp_set_current_user($VG_user->id);
                        do_action('wp_login', $VG_user->email, $user);
                        wp_redirect("/profile?change_password=done");
                        exit;
                    }
                } else {
                    $this->error = true;
                }
            }
        }
    }

    public function render()
    {
    ?>
        <div class="form__section section-password">
            <h3><?php echo $this->data["title"]; ?></h3>
            <div class="form__input">
                <input id="oldpassword" type="password" name="old_password" placeholder="<?php echo $this->data["old_pass"]; ?>">
            </div>
            <div class="form__input">
                <input id="newpassword" name="new_password" type="password" placeholder="<?php echo $this->data["new_pass"]; ?>">
            </div>
            <div class="form__input">
                <input id="newpassword1" name="new_password1" type="password" placeholder="<?php echo $this->data["new_pass1"]; ?>" >
            </div>
        </div>
    <?php
    }

}