<?php

class Page_Profile_Account_Form {

    private $form_personal;

    private $form_company;

    private $form_password;

    private $form_messages;

    private $custom;

    function __construct()
    {
        $this->form_personal = new Page_Profile_Form_Personal();
        $this->form_company  = new Page_Profile_Form_Company();
        $this->form_password = new Page_Profile_Form_Password();
        $this->form_messages = new Page_Profile_Form_Messages();

        $this->custom = get_field("custom")[0];
    }

    public function render() {
        global $VG_user;
        $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);
        $change_password = filter_input(INPUT_GET, "change_password", FILTER_SANITIZE_STRING);
        ?>
        <div class="account__form">
            <form class="form" method="post" onsubmit="return cabinet.checkForm();">
                <input type="hidden" name="action" value="save_form_data"/>
                <input type="hidden" name="customer-id" value="<?php echo $VG_user->id; ?>">
                <?php $this->form_personal->render(); ?>

                <?php $this->form_company->render(); ?>

                <?php $this->form_password->render(); ?>

                <?php if($action == "save_form_data" &&
                        !$this->form_personal->error &&
                        !$this->form_password->error) {
                            $this->form_messages->render("update_success");
                        }
                ?>

                <?php if($action == "save_form_data" &&
                         $this->form_password->error === true) {
                            $this->form_messages->render("old_password");
                        }
                ?>

                <?php if($change_password == "done") {
                    $this->form_messages->render("done_password");
                }
                ?>

                <div class="form__submit">
                    <button type="submit" class="btn btn-colored"><?php echo $this->custom["save_btn"]; ?></button>
                </div>
            </form>
        </div>
        <?php
    }

}