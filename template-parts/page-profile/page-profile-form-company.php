<?php
class Page_Profile_Form_Company {

    private $data;

    function __construct() {

        global $VG_user;
        $this->data = get_field("company_form")[0];

        $action = filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING);
        if($action === "save_form_data") {
            $is_commercial   = filter_input(INPUT_POST, "is_commercial", FILTER_SANITIZE_STRING) == "on" ? 1 : 0;
            $id              = filter_input(INPUT_POST, "customer-id", FILTER_SANITIZE_STRING);
            $name            = filter_input(INPUT_POST, "company-name", FILTER_SANITIZE_STRING);
            $code            = filter_input(INPUT_POST, "company-code", FILTER_SANITIZE_STRING);
            $representative  = filter_input(INPUT_POST, "representative", FILTER_SANITIZE_STRING);
            $VG_user->update_company($id, $is_commercial, $name, $code, $representative);

        }
    }

    public function render() {
        global $VG_user;
    ?>
        <div class="form__section section-company">
            <h3><?php echo $this->data["title"]; ?></h3>
            <div class="form__checkbox">
                <label for="iscommercial">
                    <input name="is_commercial" type="checkbox" id="iscommercial" <?php if($VG_user->is_commercial == 1) { echo "checked='checked'"; }?>>
                    <div class="form__checkbox-custom"></div>
                    <span><?php echo $this->data["account_text"]; ?></span>
                </label>
            </div>
            <div class="section-company__fields  show-grid">
                <div class="form__input input-filled">
                    <input id="companyname" type="text" name="company-name" placeholder="<?php echo $this->data["name"]; ?>" value="<?php echo $VG_user->company_name; ?>">
                </div>
                <div class="form__input input-filled">
                    <input id="companycode" type="text" name="company-code" placeholder="<?php echo $this->data["code"]; ?>" class="numeric" value="<?php echo $VG_user->company_code; ?>">
                </div>
                <div class="form__input input-filled">
                    <input id="representative" type="text" name="representative" placeholder="<?php echo $this->data["fio"]; ?>" value="<?php echo $VG_user->representative; ?>">
                </div>
            </div>
        </div>
    <?php
    }

}