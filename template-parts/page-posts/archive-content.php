<?php

class Archive_Content {

    public function __construct() {
        $this->blogpage_title = get_field('blogpage_title');
        $currentPage = get_query_var('paged');
        $this->args = array(
            'numberposts' => -1,
            'posts_per_page' => 8,
            'post_type'   => 'post',
            'paged' => $currentPage
        );

    }

    public function render() {
        require_once get_template_directory() . "/classes/vg_user.php";
        $vg_user = new VG_User();
        $user = $vg_user->get_current_user();
        $new_posts_count = $vg_user->get_count_new_materials();
        ?>
        <?php $query = new WP_Query($this->args);?>
        <div class="news">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="news__wrapper">
                            <?php if($user['customer-name'] != "") : ?>
                            <div class="account">
                                <div class="news__title">
                                    <?php if( !empty( $this->blogpage_title ) ) : ?>
                                        <h1><?php echo $this->blogpage_title; ?></h1>
                                    <?php endif; ?>
                                </div>
                                <!-- <nav class="account__pages">
                                    <div class="account__pages-toggle">
                                        <img src="<?php echo get_template_directory_uri(); ?>/static/svg-arrow-select.svg" alt="">
                                    </div>
                                </nav> -->
                            </div>
                            <?php else : ?>
                                <?php if( !empty( $this->blogpage_title ) ) : ?>
                                    <div class="news__title">
                                        <h1><?php echo $this->blogpage_title; ?></h1>
                                    </div>
                                <?php endif; ?>
                            <?php endif; ?>
                            <ul class="news__list"
                                <?php if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>
                                    <li class="news__list-item"><a href="<?php echo get_the_permalink(); ?>" >
                                        <div class="news__list-wrapper">
                                            <picture>
                                                <div class="news__list-image" style="background-image: url(<?php echo get_the_post_thumbnail_url(); ?>)"></div>
                                            </picture>
                                            <div class="news__list-text">
                                                <p><?php echo get_the_title(); ?></p>
                                            </div>
                                            <div class="news__list-date">
                                                <?php  if ( wpml_get_current_language() == 'uk' ) : 
                                                    $arr = [
                                                        'нулября',
                                                        'січня',
                                                        'лютого',
                                                        'березня',
                                                        'квітня',
                                                        'травня',
                                                        'червня',
                                                        'липня',
                                                        'серпня',
                                                        'вересня',
                                                        'жовтня',
                                                        'листопада',
                                                        'грудня'
                                                    ];
                                                else :
                                                    $arr = [
                                                        'нулября',
                                                        'января',
                                                        'февраля',
                                                        'марта',
                                                        'апреля',
                                                        'мая',
                                                        'июня',
                                                        'июля',
                                                        'августа',
                                                        'сентября',
                                                        'октября',
                                                        'ноября',
                                                        'декабря'
                                                    ];
                                                endif; 
                                                $post = get_post(get_the_ID());
                                                $month = $arr[date('n', strtotime($post->post_date))];
                                                $day = date('d', strtotime($post->post_date));
                                                $year = date('Y', strtotime($post->post_date));?>
                                                <span><?php echo $day . " " . $month . " " . $year; ?></span>
                                            </div>
                                        </div></a>
                                    </li>
                                <?php endwhile; endif; ?>
                            </ul>
                            <div class="news__pagination">
                                <ul>
                                    <?php echo vagonka_paginate_links( array( 'total' => $query->max_num_pages ) ); ?>
                                </ul>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>

        <?php
    }

}