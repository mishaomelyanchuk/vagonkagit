<?php
require_once get_template_directory() . "/classes/vg_user.php";
class Header_CabinetMenu extends VG_User {

    private array $cabinet_menu;

    private bool $accent;

    function __construct()
    {
        $this->cabinet_menu = get_field("cabinet_menu", "option")[0];
        $this->accent = in_array(get_post()->post_name, array("profile", "orders", "materials"));
    }

    public function render() {
        if(empty($this->cabinet_menu)) return;
        ?>
        <li>
            <a data-link="account" href="http://vagonka.page.ua/profile/">
                <i>
                    <svg width="17" height="17">
                        <use class="svg-catalog"
                             href="<?= TEMPLATE_PATH ?>static/svg-account.svg#account"/>
                    </svg>
                </i>
                <span <?php if($this->accent === true) { echo "class='accent'"; } ?>><?php echo $this->cabinet_menu["title"]; ?></span>
            </a>

            <section class="header__actions-popup popup-account">
                <div class="account-popup">
                    <div class="account-popup__wrapper">
                        <ul>
                            <?php foreach($this->cabinet_menu["menu"] as $item): ?>
                                <?php if(strpos($item["menu_item"]["url"], "materials") !== false): ?>
                                    <?php if(((int)$this->get_current_user()["user_type"]) == 0) continue; ?>
                                    <li>
                                        <?php if($this->get_count_new_materials() > 0): ?>
                                            <div class="account-count"><span><?php echo $this->get_count_new_materials(); ?></span></div>
                                        <?php endif; ?>
                                        <a href="<?php echo $item["menu_item"]["url"]; ?>"><?php echo $item["menu_item"]["title"]; ?></a>
                                    </li>
                                <?php else: ?>
                                    <li><a href="<?php echo $item["menu_item"]["url"]; ?>"><?php echo $item["menu_item"]["title"]; ?></a></li>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </ul>
                        <div class="account-popup__exit">
                            <a href="<?php echo str_replace( '/ru', '', wp_logout_url("/")); ?>"><?php echo $this->cabinet_menu["exit_button"]; ?></a>
                        </div>
                    </div>
                </div>

            </section>

        </li>
        <?php
    }


}