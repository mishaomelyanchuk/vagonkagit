<?php
class Header_LoginForm {

    private array $login_form = array();

    function __construct()
    {
        $this->login_form = get_field('login_form', 'option')[0];
    }

    public function render() {
        global $VG_user;
        // $VG_user = new VG_User();
        if(empty($this->login_form)) return;
        ?>
        <li>
        <?php if ( wpml_get_current_language() == 'uk' ) : ?>
            <a data-link="login" href="<?php echo ( $VG_user->is_auth() ) ? '/profile' : '/login-page' ;?>">
        <?php else : ?>
            <a data-link="login" href="<?php echo ( $VG_user->is_auth() ) ? '/ru/profile' : '/ru/login-page' ;?>">
        <?php endif; ?>
                <i>
                    <svg width="12" height="17">
                        <use class="svg-catalog"
                             href="<?= TEMPLATE_PATH ?>static/svg-singin.svg#singin"/>
                    </svg>
                </i>
                <span><?php echo $this->login_form["button_label"]; ?></span>
            </a>
            <section class="header__actions-popup popup-login <?php if(isset($_GET["auth"])): ?>show<?php endif; ?>">
                <div class="login__form">
                    <form class="form" action="/login" method="post">
                        <div class="form__section section-company">
                            <div class="form__input input-required">
                                <input name="email" id="login_email" type="email" placeholder="<?php echo $this->login_form["email_label"]; ?>">
                            </div>

                            <div class="form__input input-required">
                                <input type="password" id="login_pass" placeholder="<?php echo $this->login_form["password_label"]; ?>"
                                       required>
                            </div>
                            <div id="login_error_message"></div>
                        </div>
                        <div class="form__submit">
                            <button type="button" onclick="Login.request();" class="btn btn-colored">
                                <?php echo $this->login_form["button_label"]; ?>
                            </button>
                        </div>
                    </form>
                    <ul class="login__form-links">
                        <li><a href="<?php echo $this->login_form["forgot_password"]["url"]; ?>"><?php echo $this->login_form["forgot_password"]["title"]; ?></a></li>
                        <li><a href="<?php echo $this->login_form["registration"]["url"]; ?>"><?php echo $this->login_form["registration"]["title"]; ?></a></li>
                    </ul>
                </div>
            </section>
        </li>
        <?php
    }



}