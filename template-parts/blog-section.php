<?php

class Blog_Section {
    public function __construct() {
        
          if ( wp_is_mobile() ) {
            $this->posts = get_posts( array(
                'numberposts' => 3,
                'post_type'   => 'post',

              ) );
          } else {
            $this->posts = get_posts( array(
                'numberposts' => 6,
                'post_type'   => 'post',
              ) );
          }
    }

    public function render() { ?>
    <?php if ( !empty( $this->posts ) ) { ?>
        <div class="publicity publicity-no-slider">
            <div class="container">
                <section class="section__outer">
                <section class="section__inner">
                    <div class="publicity__wrapper">
                    <div class="publicity__title">
                        <h3><?php echo get_field('post','option')['tekst_publikaczii']?></h3>
                    </div>
                    <div class="publicity__controls">
                        <div class="publicity__controls-next">
                        <img src="<?=TEMPLATE_PATH?>static/svg-arrow.svg" alt="">
                        <img src="<?=TEMPLATE_PATH?>static/svg-line.svg" alt="">
                        </div>
                        <div class="publicity__controls-prev">
                        <img src="<?=TEMPLATE_PATH?>static/svg-arrow.svg" alt="">
                        </div>
                    </div>
                    <div class="publicity__items">
                        <ul>
                        <?php  foreach ( $this->posts as $item ) { ?>
                        <li class="publicity__item">
                            <a href="<?php echo get_the_permalink( $item->ID ); ?>">
                            <?php echo get_the_title( $item->ID ); ?>
                            </a>
                            <?php  if ( wpml_get_current_language() == 'uk' ) : 
                                                    $arr = [
                                                        'нулября',
                                                        'січня',
                                                        'лютого',
                                                        'березня',
                                                        'квітня',
                                                        'травня',
                                                        'червня',
                                                        'липня',
                                                        'серпня',
                                                        'вересня',
                                                        'жовтня',
                                                        'листопада',
                                                        'грудня'
                                                    ];
                                                else :
                                                    $arr = [
                                                        'нулября',
                                                        'января',
                                                        'февраля',
                                                        'марта',
                                                        'апреля',
                                                        'мая',
                                                        'июня',
                                                        'июля',
                                                        'августа',
                                                        'сентября',
                                                        'октября',
                                                        'ноября',
                                                        'декабря'
                                                    ];
                                                endif; 
                                                $post = get_post($item->ID);
                                                $month = $arr[date('n', strtotime($post->post_date))];
                                                $day = date('d', strtotime($post->post_date));
                                                $year = date('Y', strtotime($post->post_date));?>
                                                <span><?php echo $day . " " . $month . " " . $year; ?></span>
                        </li>
                        <?php } ?>
                        </ul>
                    </div>
                    </div>
                </section>
                </section>
            </div>
        </div>
        <?php } ?>
    <?php }

}
