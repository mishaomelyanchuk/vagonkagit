<?php

class Page_Offices_Content_Section {
    public function __construct() {
        $this->underbutton_text = get_field('underbutton_text');
        $this->button = get_field('button');
        $this->offices = get_terms( 'areas', 
        array(
          'hide_empty' => true,
        ) 
      );
    }

    public function render() { ?>
    <div class="container">
        <section class="section__outer">
          <section class="section__inner">
            <div class="offices__wrapper">
              <div class="offices-sidebar">
                <div class="offices__title">
                  <h1><?php echo get_the_title()?></h1>
                </div>
                <div class="offices__search">
                  <div class="offices__search-dropdowns">
                    <div class="offices__search-select">
                      <select name="area" id="area">
                       
                        <option value=""><?php echo get_field('dealers','option')['tekst_vse_oblasti'];?></option>
                        <?php $counter = 1; ?>
                        <?php foreach ( $this->offices as $item ) { ?>
                        <option value="<?php echo $counter; ?>"><?php echo $item->name; ?></option>
                        <?php $counter++; ?>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="offices__search-select">
                      <select name="type" id="type">
                        <option value=""><?php echo get_field('dealers','option')['tekst_vse_tipy_predstavitelstv'];?></option>
                        <option value="base"><?php echo get_field('dealers','option')['tekst_regionalnoe_predstavitelstvo'];?></option>
                        <option value="small"><?php echo get_field('dealers','option')['tekst_magazin'];?></option>
                      </select>
                    </div>
                  </div>
                  <div class="offices__result">
                    <ul class="offices__result-wrapper">
                    </ul>
                  </div>
                </div>
                <div class="offices__become-partner">
                  <?php if ( !empty( $this->underbutton_text ) ) { ?>
                  <p>
                    <?php echo $this->underbutton_text; ?>
                  </p>
                  <?php } ?>
                  <?php if ( !empty( $this->button ) ) { ?>
                  <div class="offices__become-partner-link">
                    <a class="btn btn-colored" href="<?php echo $this->button['url']; ?>"><?php echo $this->button['title']; ?></a>
                  </div>
                  <?php } ?>
                </div>
              </div>
              <div class="offices-bg">
                <div class="offices-bg__wrapper" id="map"></div>
              </div>
            </div>
          </section>
        </section>
      </div>
    </div>
    <?php }

}
