<?php

class Single_Post_Content {

    public function __construct(){
        $this->novelty_title = get_the_title();
        $post = get_post(get_the_ID());
        if ( wpml_get_current_language() == 'uk' ) : 
            $arr = [
                'нулября',
                'січня',
                'лютого',
                'березня',
                'квітня',
                'травня',
                'червня',
                'липня',
                'серпня',
                'вересня',
                'жовтня',
                'листопада',
                'грудня'
            ];
        else :
            $arr = [
                'нулября',
                'января',
                'февраля',
                'марта',
                'апреля',
                'мая',
                'июня',
                'июля',
                'августа',
                'сентября',
                'октября',
                'ноября',
                'декабря'
            ];
        endif; 
        $this->novelty_date_day = date('d', strtotime($post->post_date));
        $this->novelty_date_month = $arr[date('n', strtotime($post->post_date))];
        $this->novelty_date_year =date('Y', strtotime($post->post_date));
        $this->main_novelty_thumbnail = get_the_post_thumbnail();
        $this->novelty_text = get_field( 'text' );

        $this->novelty_image_slider = get_field( 'image_slider' );
        foreach ( $this->novelty_image_slider as &$item ) :
            $item = [
                'image' => $item['image']
            ];
        endforeach;

        $this->novelty_files = get_field('files_repeater');

        $this->archive_page_url = get_post_type_archive_link('post');
    }

    public function echo_filesize ( $file_url, $filesize_in_bytes) {
        $filesize_in_mb = number_format($filesize_in_bytes / 1048576, 2) . ' MB';
        $fileInfo = pathinfo($file_url);
        $extension = strtoupper( $fileInfo['extension'] );

        echo $extension . ' ' . $filesize_in_mb;
    }

    public function render() {
        ?>

        <div class="article">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="article__wrapper">
                            <?php if( !empty( $this->novelty_title ) ) : ?>
                                <div class="article__title">
                                        <h1><?php echo $this->novelty_title; ?></h1>
                                </div>
                            <?php endif; ?>
                            <?php if( !empty( $this->novelty_date_day ) && !empty( $this->novelty_date_year ) && !empty( $this->novelty_date_month ) ) : ?>
                                <div class="article__date">
                                    <div>
                                        <p><?php echo $this->novelty_date_day; ?></p>
                                        <span><?php echo $this->novelty_date_month; ?></span>
                                        <span><?php echo $this->novelty_date_year; ?></span>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php if( !empty( $this->main_novelty_thumbnail ) ) :?>
                                <picture>
                                    <?php echo $this->main_novelty_thumbnail; ?>
                                </picture>
                            <?php endif; ?>
                            <?php if( !empty( $this->novelty_text ) ) : ?>
                                <div class="article__text">
                                    <?php echo $this->novelty_text; ?>
                                </div>
                            <?php endif; ?>
                            <?php if( !empty( $this->novelty_image_slider ) && empty( $this->novelty_files ) ) : ?>
                                <style> div:after {
                                        visibility: hidden;
                                    }</style>
                                <div class="article__slider">
                                    <div class="article__slider-arrows">
                                        <div class="article__slider-next">
                                            <svg width="22" height="63">
                                                <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-left.svg#arrow"></use>
                                            </svg>
                                        </div>
                                        <div class="article__slider-prev">
                                            <svg width="22" height="63">
                                                <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-right.svg#arrow"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <ul>
                                        <?php foreach( $this->novelty_image_slider as $item ) : ?>
                                            <li>
                                                <div class="product__media-item">
                                                    <a href="<?php echo $item['image']; ?>" data-fancybox="product">
                                                        <img src="<?php echo $item['image']; ?>" alt="">
                                                    </a>
                                                </div>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php elseif( !empty( $this->novelty_image_slider) ) : ?>
                                <div class="article__slider">
                                    <div class="article__slider-arrows">
                                        <div class="article__slider-next">
                                            <svg width="22" height="63">
                                                <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-left.svg#arrow"></use>
                                            </svg>
                                        </div>
                                        <div class="article__slider-prev">
                                            <svg width="22" height="63">
                                                <use href="<?=TEMPLATE_PATH?>/static/svg-article-arrow-right.svg#arrow"></use>
                                            </svg>
                                        </div>
                                    </div>
                                    <ul>
                                        <?php foreach( $this->novelty_image_slider as $item ) : ?>
                                            <li>
                                                <div class="product__media-item">
                                                <a href="<?php echo $item['image']; ?>" data-fancybox="product">
                                                    <img src="<?php echo $item['image']; ?>" alt="">
                                                </a>
                                            </div>

                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php endif; ?>
                            <?php if( !empty( $this->novelty_files ) ) : ?>
                                <div class="article__files">
                                    <ul>
                                        <?php foreach( $this->novelty_files as $item ) : ?>
                                            <li>
                                                <a href="<?php echo $item['file']['url']; ?>"><?php echo $item['file']['title'] . ', '; ( new Single_Post_Content())->echo_filesize($item['file']['url'], $item['file']['filesize']);  ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </div>
                            <?php else : ?>
                                <div class="empty__block"></div>
                            <?php endif; ?>
                            <div class="article__back-link">
                                <a href="<?php echo (Page_Posts::get_url() ); ?>"><?php echo get_field('post','option')['tekst_vernutsya_k_spisku_publikaczij']?></a>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>

        <?php
    }


}