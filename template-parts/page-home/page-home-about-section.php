<?php

class Page_Home_About_Section {
    public function __construct() {
        $this->title = get_field('section_about')['title'];
        $this->text = get_field('section_about')['text'];
        $this->link = get_field('section_about')['link'];
        $this->video = get_field('section_about')['video'];
        
    }

    public function render() { ?>
       <div class="about">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="about__wrapper">
                            <div class="about__title">
                                <h2><?php echo $this->title; ?></h2>
                            </div>
                            <div class="about__description">
                                <p>
                                    <?php echo $this->text; ?>
                                </p>
                            </div>
                            <div class="about__btn">
                                <a class="btn btn-first btn-transparent" href="<?php echo $this->link['url']; ?>"> <?php echo $this->link['title']; ?></a>
                            </div>
                            <div class="media-block">
                                <div class="media-block-image" style="background-image: url('/static/about.jpg')"></div>
                                <div class="media-block-video">
                                    <video autoplay="" muted="" loop="">
                                        <source src="<?php echo $this->video['url']; ?>">
                                    </video>
                                </div>
                            </div>

                        </div>
                    </section>
                </section>
            </div>
        </div>
    <?php }

}
