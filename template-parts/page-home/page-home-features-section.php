<?php

class Page_Home_Features_Section {
    public function __construct() {
        $this->features = get_field('section_features')['features'];
        
    }

    public function render() { ?>
        <div class="features">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="features__wrapper">
                        <?php foreach ( $this->features as $item ) : ?>
                            <div class="feature">
                                <div class="feature__wrapper">
                                    <div class="feature__title">
                                        <h3><?php echo $item['title']; ?></h3>
                                    </div>
                                    <div class="feature__description">
                                        <p><?php echo $item['text']; ?></p>
                                    </div>
                                    <picutre class="feature__image">
                                        <img src="<?php echo $item['image']; ?>" alt="">
                                    </picutre>
                                </div>
                            </div>
                        <?php endforeach; ?>

                        </div>
                    </section>
                </section>
            </div>
        </div>
    <?php }

}
