<?php

class Page_Home_Products_Section {
    public function __construct() {

        $this->products = get_field('section_product')['products'];
    }

    public function render() { ?>
        <?php if ( !empty( $this->products ) ) : ?>
            <div class="products">
                <div class="container">
                    <section class="section__outer">
                        <section class="section__inner">
                            <div class="products__wrapper">
                                <?php $count = 1; ?>
                                <?php foreach( $this->products as $item ) : ?>
                                
                                <?php if ( ( $count % 2 ) == 0 ) : ?>
                                    <div class="offer offer-slider">
                                <?php else : ?>
                                    <div class="offer">
                                <?php endif; ?>
                                    <div class="offer__wrapper">
                                        <div class="offer__composition">
                                            <div class="offer__image">
                                                <img src="<?php echo get_field( 'product_main',$item )['image']; ?>" alt="">
                                            </div>
                                            <div class="offer__bg" style="background-image: url(<?php echo get_field( 'product_main',$item )['background_image']; ?>)"></div>
                                            <div class="offer__pattern">
                                                <img src="<?php echo get_field( 'product_main',$item )['background_pictures']; ?>" alt="">
                                            </div>
                                        </div>
                                        <div class="offer__description">
                                            <?php if ( !empty( get_field( 'product_main',$item )['lable'] ) ) : ?>
                                                <div class="offer__label"><span><?php echo get_field( 'product_main',$item )['lable']; ?></span></div>
                                            <?php endif; ?>
                                            <div class="offer__title">
                                                <h2><?php echo get_field( 'product_main',$item )['title']; ?></h2>
                                            </div>
                                            <div class="offer__subtitle">
                                                <p><?php echo get_field( 'product_main',$item )['subtitle']; ?></p>
                                            </div>
                                            <div class="offer__text">
                                                <p>
                                                <?php echo get_field( 'product_main',$item )['text']; ?>
                                                </p>
                                            </div>
                                            <div class="offer__btn">
                                                <a class="btn btn-colored btn-first" href="<?php echo get_field( 'product_main',$item )['link']['url']; ?>"> <?php echo get_field( 'product_main',$item )['link']['title']; ?></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $count++;?>
                                <?php endforeach; ?>
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        <?php endif; ?>
    <?php }

}
