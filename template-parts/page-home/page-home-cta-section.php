<?php

class Page_Home_Cta_Section
{
    public function __construct()
    {
        $this->title = get_field('tags_section')['title'];
        $this->subtitle = get_field('tags_section')['subtitle'];
        $this->text = get_field('tags_section')['text'];
        $this->ctaBtn = get_field('tags_section')['cta_btn'];
        $this->bgPattern = get_field('tags_section')['bg_image'];
        $this->tags = get_terms(array(
            'taxonomy' => 'product_tag',
            'hide_empty' => false,
        ));
        $this->tags_background = get_field('tags_section')["tags_background"];
    }

    public function render()
    { ?>
        <div class="cta">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="cta__wrapper">
                            <div class="cta__title">
                                <h2><?php echo $this->title; ?></h2>
                            </div>
                            <div class="cta__subtitle">
                                <span><?php echo $this->subtitle; ?></span>
                            </div>
                            <div class="cta__description">
                                <p>
                                    <?php echo $this->text; ?>
                                </p>
                            </div>
                            <?php
                            $cnt = count($this->tags);
                            $c = ceil($cnt / 6) + 1;
                            $k = 0;
                            ?>
                            <picture class="cta__composition">
                            <?php for($m = 1; $m <= $cnt; $m++) { ?>
                                <?php if(empty(get_field('show_in_main', 'term_' . $this->tags[$k]->term_id))) { $k++; continue; } ?>
                                <?php if(!isset($this->tags[$k])) break; ?>
                                <div data-variant="<?=$m?>" class="cta__composition-variant <?php if($m == 1) echo "show"; ?>">
                                    <div class="cta__composition-image">
                                        <img src="<?php echo get_field('main_image', 'term_' . $this->tags[$k]->term_id); ?>" alt="">
                                    </div>
                                    <div class="cta__composition-patter">
                                        <img src="<?php echo $this->bgPattern["url"]; ?>" alt="">
                                    </div>
                                </div>
                                <?php $k++; ?>
                            <?php } ?>
                            </picture>
                            <div class="cta__scroll">
                                <div class="cta__scroll-wrapper">
                                    <?php $k = 0;?>
                                    <?php for($m = 1; $m <= $cnt; $m++) { ?>
                                        <?php if(empty(get_field('show_in_main', 'term_' . $this->tags[$k]->term_id))) { $k++; continue; } ?>
                                        <?php if(!isset($this->tags[$k])) break; ?>
                                        <div data-variant="<?=$m?>" class="cta__scroll-item <?php if($m == 1) echo "active"; ?>">
                                            <div>
                                                <p>
                                                    <?php echo $this->tags[$k]->name; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <?php $k++; ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="cta__btn">
                                <a href="<?php echo $this->ctaBtn["url"]; ?>"><?php echo $this->ctaBtn["title"]; ?></a>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>

        <?php
    }

}
