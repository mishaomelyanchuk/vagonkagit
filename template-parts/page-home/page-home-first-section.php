<?php

class Page_Home_First_Section {
    public function __construct() {
        $this->slider = get_field('slider_section')['slider'];
    }

    public function render() { ?>
        <?php if ( !empty( $this->slider ) ) : ?>
            <div class="bg-slider swiper">
                <div class="bg-slider__wrapper swiper-wrapper">
                    <?php foreach ( $this->slider as $counter => $item ) : ?>
                        <div class="bg-slider__item swiper-slide">
                            <?php if(!empty($item['video'])) : ?>
                                <div class="bg-slider__item-bg">
                                  <video muted id="bg-slider__item-<?php echo $counter; ?>" <?php if(!empty($item['image'])){ ?>poster="<?php echo $item['image']; ?>"<?php } ?>>
                                    <source src="<?php echo $item['video']; ?>" type="video/mp4">
                                  </video>
                                    <div class="cover">
                                        <div class="container">
                                            <section class="section__outer">
                                                <section class="section__inner">
                                                    <div class="cover__wrapper">
                                                        <div class="cover__title">
                                                            <h1><?php echo $item['text']; ?></h1>
                                                        </div>
                                                        <div class="cover__btn">
                                                            <a class="btn btn-colored btn-first" href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a>
                                                        </div>
                                                    </div>
                                                </section>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="bg-slider__item-bg" st="background-image: url(<?php echo $item['image']; ?>);" style="background-image: url(<?php echo $item['image']; ?>);">
                                    <div class="cover">
                                        <div class="container">
                                            <section class="section__outer">
                                                <section class="section__inner">
                                                    <div class="cover__wrapper">
                                                        <div class="cover__title">
                                                            <h1><?php echo $item['text']; ?></h1>
                                                        </div>
                                                        <div class="cover__btn">
                                                            <a class="btn btn-colored btn-first" href="<?php echo $item['link']['url']; ?>"><?php echo $item['link']['title']; ?></a>
                                                        </div>
                                                    </div>
                                                </section>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="swiper-pagination slick-dots"></div>
            </div>
        <?php endif; ?>
        </div>
    <?php }
}

