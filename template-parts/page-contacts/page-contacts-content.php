<?php

class Page_Contacts_Content {

    public $is_sending = false;

    public $center_lat;

    public $center_lng;

    public $zoom;

    public $map;

    public function __construct() {
        $information_section = get_field('information_section');
        $this->company_name = $information_section['company_name'];

        $main_office  = $information_section['main'];
        $this->main_office_title = $main_office['title'];
        $this->main_office_adress = $main_office['adress'];
        $this->main_office_contacts = $main_office['contact_info'];

        $internet_sales = $information_section['internet'];
        $this->internet_sales_title = $internet_sales['title'];
        $this->internet_sales_contacts = $internet_sales['contact_info'];

        $sales_department = $information_section['sales_department'];
        $this->sales_department_title = $sales_department['title'];
        $this->sales_department_contacts = $sales_department['contact_info'];

        $sales_issues = $information_section['sales_issues'];
        $this->sales_issues_title = $sales_issues['title'];
        $this->sales_issues_contacts = $sales_issues['contact_info'];

        $technological_questions = $information_section['technological'];
        $this->technological_questions_title = $technological_questions['title'];
        $this->technological_questions_contacts = $technological_questions['contact_info'];

        $form_section = get_field('form_section');
        $this->form_title = $form_section['title'];

        $this->done_message = $form_section['done_message'];

        $this->lang = wpml_get_current_language();

    }

    public function render() {
        ?>
        <div class="contacts-first-screen">
            <div id="map"></div>
        </div>
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="contacts__wrapper">
                        <?php if ( !empty( $this->company_name ) ) : ?>
                            <div class="contacts__title">
                                <h1><?php echo $this->company_name; ?></h1>
                            </div>
                        <?php endif; ?>

                        <ul>
                            <li>
                                <?php if ( !empty( $this->main_office_title ) ) : ?>
                                    <h3><?php echo $this->main_office_title; ?></h3>
                                <?php endif; ?>
                                <?php if ( !empty( $this->main_office_adress ) ) : ?>
                                    <p><?php echo $this->main_office_adress; ?></p>
                                <?php endif; ?>
                                <?php if ( !empty( $this->main_office_contacts ) ) : ?>
                                    <?php foreach ( $this->main_office_contacts as $item ) : ?>
                                        <a class="link-phone" href="tel:<?php echo preg_replace( '/[( )-]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                    <?php endforeach; ?>
                                    <?php foreach ( $this->main_office_contacts as $item ) : ?>
                                        <a class="link-mail" href="mailto:<?=$item['email'];?>"><?php echo $item['email']; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>

                            <li>
                                <?php if ( !empty( $this->internet_sales_title ) ) : ?>
                                    <h3><?php echo $this->internet_sales_title; ?></h3>
                                <?php endif; ?>
                                <?php if ( !empty( $this->internet_sales_contacts ) ) : ?>
                                    <?php foreach ( $this->internet_sales_contacts as $item) : ?>
                                        <a class="link-phone" href="tel:<?php echo preg_replace( '/[( )-]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                    <?php endforeach; ?>
                                    <?php foreach ( $this->internet_sales_contacts as $item ) : ?>
                                        <a class="link-mail" href="mailto:<?=$item['email'];?>"><?php echo $item['email']; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>

                            <li>
                                <?php if ( !empty( $this->sales_department_title ) ) : ?>
                                    <h3><?php echo $this->sales_department_title; ?></h3>
                                <?php endif; ?>
                                <?php if( !empty( $this->sales_department_contacts ) ) : ?>
                                    <?php foreach ( $this->sales_department_contacts as $item) : ?>
                                        <a class="link-phone" href="tel:<?php echo preg_replace( '/[( )-]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                    <?php endforeach; ?>
                                    <?php foreach ( $this->sales_department_contacts as $item) : ?>
                                        <a class="link-mail" href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>

                            <li>
                                <?php if ( !empty( $this->sales_issues_title ) ) : ?>
                                    <h3><?php echo $this->sales_issues_title; ?></h3>
                                <?php endif; ?>
                                <?php if( !empty( $this->sales_issues_contacts ) ) : ?>
                                    <?php foreach ( $this->sales_issues_contacts as $item) : ?>
                                        <a class="link-phone" href="tel:<?php echo preg_replace( '/[( )-]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                    <?php endforeach; ?>
                                    <?php foreach ( $this->sales_issues_contacts as $item) : ?>
                                        <a class="link-mail" href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>

                            <li>
                                <?php if ( !empty( $this->technological_questions_title ) ) : ?>
                                    <h3><?php echo $this->technological_questions_title; ?></h3>
                                <?php endif; ?>
                                <?php if( !empty( $this->technological_questions_contacts ) ) : ?>
                                    <?php foreach ( $this->technological_questions_contacts as $item) : ?>
                                        <a class="link-phone" href="tel:<?php echo preg_replace( '/[( )-]/', '', $item['phone']); ?>"><?php echo $item['phone']; ?></a>
                                    <?php endforeach; ?>
                                    <?php foreach ( $this->technological_questions_contacts as $item) : ?>
                                        <a class="link-mail" href="mailto:<?php echo $item['email']; ?>"><?php echo $item['email']; ?></a>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </li>
                        </ul>
                        <div class="contacts__form">
                            <?php if( !empty( $this->form_title ) ) : ?>
                                <div class="contacts__form-title">
                                    <h2><?php echo $this->form_title; ?></h2>
                                </div>
                            <?php endif; ?>
                            <div class="contact-form" id="contact_form_id">
                                <?php if($this->is_sending === true) { ?>
                                    <h4><?php echo $this->done_message; ?></h4><br>
                                <?php } ?>
                                <div class="contact-form__wrapper">
                                    <form class="form" action="<?php if($this->lang == 'uk') echo '/contact'; else echo '/ru/contact'; ?>/#contact_form_id" method="post" onsubmit="return validationForms.onSubmitContacts()">
                                        <div class="form__input">
                                            <input type="text" name="f_name" id="p_name" placeholder="<?php echo get_field('contact_form','option')['tekst_imya']; ?>">
                                        </div>
                                        <div class="form__input">
                                            <input type="email" name="f_email" id="p_email" placeholder="<?php echo get_field('contact_form','option')['tekst_e-mail']; ?>">
                                        </div>
                                        <div class="form__textarea">
                                            <textarea id="p_text" name="f_offer" placeholder="<?php echo get_field('contact_form','option')['tekst_vashe_soobshhenie']; ?>"></textarea>
                                        </div>
                                        <div class="form__submit">
                                            <button class="btn btn-colored" type="submit"><?php echo get_field('contact_form','option')['tekst_otpravit']; ?></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </section>
            </section>
        </div>
        </div>

        <script type="text/javascript">
            const main_office = {
                center_lat: '<?php echo $this->center_lat; ?>',
                center_lng: '<?php echo $this->center_lng; ?>',
                marker_lat: '<?php echo $this->map['lat']; ?>',
                marker_lng: '<?php echo $this->map['lng']; ?>',
                address: '<?php echo $this->map['address']; ?>',
                zoom: '<?php echo $this->zoom; ?>',
                icon: '<?php echo TEMPLATE_PATH; ?>static/map_marker.png'
            };
        </script>

        <?php
    }

}

