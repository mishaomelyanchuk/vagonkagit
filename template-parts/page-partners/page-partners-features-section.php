<?php

class Page_Partners_Features_Section {

    public function __construct() {
        $features_section = get_field( 'features_section' );
        $this->features_title = $features_section['title'];
        $this->features_list = $features_section['features_list'];

        foreach ( $this->features_list as &$item ) :
            $item = [
                'features' => $item['features']
            ];
        endforeach;

    }

    public function render() {
        ?>
            <div class="partners-features">
                <div class="container">
                    <section class="section__outer">
                        <section class="section__inner">
                            <div class="partners-features__wrapper">
                                <?php if ( !empty ($this->features_title) ) : ?>
                                    <div class="partners-features__title">
                                      <span><?php echo $this->features_title; ?></span>
                                    </div>
                                <?php endif; ?>

                                <?php if ( !empty($this->features_list) ) : ?>
                                    <ul>
                                        <?php foreach ( $this->features_list as $item ) : ?>
                                            <li>
                                                <?php echo $item['features']; ?>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </section>
                    </section>
                </div>
            </div>

        <?php
    }

}