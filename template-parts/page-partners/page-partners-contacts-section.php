<?php

class Page_Partners_Contacts_Section {

    public $is_sending = false;

    public function __construct() {
        $contacts_section = get_field( 'contacts_section' );
        $this->contacts_title    = $contacts_section['title'];
        $this->contacts_name     = $contacts_section['name'];
        $this->contacts_position = $contacts_section['position'];
        $this->contacts_phone    = $contacts_section['phone'];
        $this->contacts_email    = $contacts_section['email'];
        $this->phone_for_call    = preg_replace( '/[( )-]/', '', $this->contacts_phone );
    }

    public function render() {
        ?>
            <div class="partners-contacts">
                <div class="container">
                    <section class="section__outer">
                        <section class="section__inner">
                            <div class="partners-contacts__wrapper">
                                <?php if ( !empty ($this->contacts_title) ) : ?>
                                    <div class="partners-contacts__title">
                                        <h2><?php echo $this->contacts_title; ?></h2>
                                    </div>
                                <?php endif; ?>
                                <div class="partners-contacts__person">
                                    <?php if ( !empty ($this->contacts_name) ) : ?>
                                        <h4><?php echo $this->contacts_name; ?></h4>
                                    <?php endif;?>

                                    <?php if ( !empty ($this->contacts_position) ) : ?>
                                        <span><?php echo $this->contacts_position; ?></span>
                                    <?php endif; ?>

                                    <?php if ( !empty ($this->contacts_phone) ) : ?>
                                        <a class="link-phone" href="tel:<?=$this->phone_for_call?>"><?php echo $this->contacts_phone; ?></a>
                                    <?php endif; ?>

                                    <?php if ( !empty ($this->contacts_email) ) : ?>
                                        <a class="link-mail" href="mailto:<?=$this->contacts_email?>"><?php echo $this->contacts_email; ?></a>
                                    <?php endif; ?>
                                </div>
                            <?php ( new Page_Partners_Contact_Form() )->render($this->is_sending); ?>
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        </div>
    <?php

    }

}