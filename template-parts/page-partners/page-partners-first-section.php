<?php

class Page_Partners_First_Section {

    public $is_sending = false;

    public function __construct()
    {
        $this->page_title = get_the_title();
        $first_section = get_field('first_section');
        $this->first_section_title = $first_section['title'];
        $this->region_list = $first_section['region_list'];

        foreach ($this->region_list as &$item) :
            $item = [
                'region' => $item['region']
            ];
        endforeach;

        $this->diller_text = $first_section['diller_text'];
        $diller_link = $first_section['diller_link'];
        $this->diller_link_text = $diller_link['title'];
        $this->diller_link_url = $diller_link['url'];

    }

    public function render() {
        ?>
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="partners__wrapper">
                        <?php if ( !empty( $this->page_title ) ) : ?>
                            <div class="partners__title">
                                <h1><?php echo $this->page_title; ?></h1>
                            </div>
                        <?php endif; ?>
                        <div class="partners__areas">
                            <?php if ( !empty( $this->first_section_title ) ) : ?>
                                <p><?php echo $this->first_section_title; ?></p>
                            <?php endif; ?>
                            <?php if ( !empty( $this->region_list ) ) : ?>
                                <ul>
                                    <?php foreach( $this->region_list as $item ) : ?>
                                        <li><?php echo $item['region']; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                        </div>
                        <div class="partners__offices">
                            <picture>
                                <img src="<?=TEMPLATE_PATH?>static/svg-map.svg" alt="">
                            </picture>
                            <?php if ( !empty( $this->diller_text ) ) : ?>
                                <div class="partners__offices-text">
                                    <p><?php echo $this->diller_text; ?></p>
                                </div>
                            <?php endif; ?>
                            <?php if ( !empty( $this->diller_link_text) && !empty( $this->diller_link_url) ) : ?>
                                <div class="partners__offices-btn">
                                    <a class="btn btn-transparent" href="<?php echo $this->diller_link_url; ?>"><?php echo $this->diller_link_text; ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </section>
            </section>
        </div>
        <div class="partners-first-screen-bg-main"></div>
        </div>

        <?php
    }

}