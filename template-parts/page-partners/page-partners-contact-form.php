<?php

class Page_Partners_Contact_Form {

    public function __construct() {
        $contacts_section = get_field('contacts_section');
        $this->done_message = $contacts_section['done_message'];
        $this->lang = wpml_get_current_language();
    }

    public static function init() {
        add_action( 'wp_ajax_nopriv_contact-us', [ __CLASS__, 'process' ] );
        add_action( 'wp_ajax_contact-us', [ __CLASS__, 'process' ] );

    }

    public function render($is_sending = false) {
        ?>
            <div class="contact-form" id="partners_contact_form_id">
                <?php if($is_sending === true): ?>
                    <h4><?php echo $this->done_message; ?></h4><br>
                <?php endif; ?>
                <div class="contact-form__wrapper">
                    <form method="post" class="form" action="<?php if($this->lang == 'uk') echo '/partners/'; else echo '/ru/partners'; ?>/#partners_contact_form_id" onsubmit="return validationForms.onSubmitPartners();">
                        <div class="form__input">
                            <input type="text" name="f_name" id="name" placeholder="<?php echo get_field('contact_form','option')['tekst_imya']; ?>">
                        </div>
                        <div class="form__input">
                            <input type="email" name="f_email" id="email" placeholder="<?php echo get_field('contact_form','option')['tekst_e-mail']; ?>">
                        </div>
                        <div class="form__textarea">
                            <textarea id="text" name="f_offer" placeholder="<?php echo get_field('contact_form','option')['tekst_vashe_predlozhenie']; ?>"></textarea>
                        </div>
                        <div class="form__submit">
                            <button class="btn btn-colored" type="submit"><?php echo get_field('contact_form','option')['tekst_otpravit']; ?></button>
                        </div>
                    </form>
                </div>
            </div>
        <?php
    }

    public function process() {
        $contacts_section = get_field( 'contacts_section' );
        $this->from = isset($_POST["email"]) ? $_POST["email"] : null;
        $this->to = !empty($contacts_section['email']) ? $contacts_section['email'] : null;
        $this->name = isset($_POST["name"]) ? $_POST["name"] : null;
        $this->offer = isset($_POST["offer"]) ? $_POST["name"] : null;
        if(!is_null($this->from) &&
            !is_null($this->to) &&
            !is_null($this->name) &&
            !is_null($this->from) &&
            !is_null($this->offer)) {
            wp_mail($this->to, "Обратная связь с сайта Вагонка", "Имя:" . $this->name . "<br>Email: " . $this->from . "<br>Предложение: " . $this->offer,"Content-Type: text/html; charset=UTF-8");
        }
    }
}

Page_Partners_Contact_Form::init();