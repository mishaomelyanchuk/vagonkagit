<?php
/*
 * Template name: Materials
 */
get_header();
global $VG_user;

if($VG_user->user_type == 0) {
    wp_redirect("/");
    exit;
}

$new_posts_count = $VG_user->get_count_new_materials();
$args = array(
    'numberposts' => -1,
    'orderby' => 'date',
    'order' => 'DESC',
    'post_type' => "b2b_materials",
    'post_status' => "publish",
);

$materials = get_posts($args);

function get_uri_from_filepath($file) {
    $file_parts = explode("/", $file);
    $need = [];
    $start = false;
    foreach ($file_parts as $part) {
        if($part == "wp-content") $start = true;
        if($start === true) {
            $need[] = $part;
        }
    }
    return "/" . implode("/", $need);
}

$lang = wpml_get_current_language();

?>


<div class="account">
    <div class="container">
        <section class="secton__outer">
            <section class="section__inner">
                <div class="account__wrapper">
                    <div class="account__header">
                        <div class="account__type business-account">
                            <?php if($VG_user->user_type == 0) { ?>
                                <p class="standart_account"><?php echo get_account_name(); ?></p>
                            <?php } else { ?>
                                <p class="b2b_account"><?php echo get_account_name(1); ?></p>
                            <?php } ?>
                        </div>
                        <div class="account__title">
                            <h1><?php echo get_post()->post_title; ?></h1>
                            <?php if($new_posts_count > 0) { ?>
                            <div class="account__count">
                                <span><?php echo $new_posts_count; ?></span>
                            </div>
                            <?php } ?>
                        </div>
                        <nav class="account__pages">
                            <ul>
                                <li><a href="<?php echo ($lang == 'ru' ? '/ru' : ''); ?>/profile"><?php echo get_page_by_slug("profile")->post_title; ?></a></li>
                                <li><a href="<?php echo ($lang == 'ru' ? '/ru' : ''); ?>/orders"><?php echo get_page_by_slug("orders")->post_title; ?></a></li>
                            </ul>
                            <div class="account__pages-toggle">
                                <img src="<?php echo get_template_directory_uri(); ?>/static/svg-arrow-select.svg" alt="">
                            </div>
                        </nav>
                    </div>
                    <div class="account__materials">
                        <ul class="news__list">
                            <?php foreach($materials as $material) { ?>
                                <?php

                                $files = get_field('files_repeater', $material->ID);
                                if(count($files) > 0) {

                                    $file = $files[0]["file"];
                                    $file_id = $file["ID"];
                                    $file_url = $file["url"];
                                    $file_size = number_format((int)$file["filesize"]/1048576, 2) . " MB";
                                    $file_name_array = explode('.', $file["filename"]);
                                    $file_ext = strtoupper(end($file_name_array));
                                }

                                $post_img = get_the_post_thumbnail_url($material->ID);

                                if(empty($post_img)) { $post_img = get_template_directory_uri() . "/static/svg-empty-file.svg";}
                                $post_uri = get_permalink($material->ID);
                                $post_content = get_field("text", $material->ID);
                                if ( wpml_get_current_language() == 'uk' ) : 
                                    $arr = [
                                        'нулября',
                                        'січня',
                                        'лютого',
                                        'березня',
                                        'квітня',
                                        'травня',
                                        'червня',
                                        'липня',
                                        'серпня',
                                        'вересня',
                                        'жовтня',
                                        'листопада',
                                        'грудня'
                                    ];
                                else :
                                    $arr = [
                                        'нулября',
                                        'января',
                                        'февраля',
                                        'марта',
                                        'апреля',
                                        'мая',
                                        'июня',
                                        'июля',
                                        'августа',
                                        'сентября',
                                        'октября',
                                        'ноября',
                                        'декабря'
                                    ];
                                endif; 
                                $month = $arr[date('n', strtotime($material->post_date))];
                                $day = date('d', strtotime($material->post_date));
                                $year = date('Y', strtotime($material->post_date));
                                ?>
                                <?php if($post_content == "") { ?>
                                    <?php if((int)$file_size === 0) continue; ?>
                                    <li class="news__list-item file-item">
                                    <div class="news__list-wrapper">
                                        <a href="<?php echo $file_url; ?>" onclick="cabinet.postRead(<?php echo $material->ID; ?>);" target="_blank">
                                            <picture>
                                                <div class="news__list-image materials" <?php if($post_img !== "") echo "style='background: url(" . $post_img . ");'" ?>></div>
                                            </picture>
                                        </a>
                                        <div class="news__list-text">
                                            <a href="<?php echo $file_url; ?>" onclick="cabinet.postRead(<?php echo $material->ID; ?>);" target="_blank">
                                                <p><?php echo $material->post_title; ?></p>
                                            </a>
                                            <div class="news__list-download">
                                                <a href="<?php echo $file_url; ?>" onclick="cabinet.postRead(<?php echo $material->ID; ?>);" target="_blank">скачать</a>
                                                <p>Файл <?php echo $file_ext; ?> <span><?php echo $file_size; ?></span></p>
                                            </div>
                                        </div>

                                        <div class="news__list-date">
                                            <span><?php echo $day . " " . $month . " " . $year; ?></span>
                                        </div>
                                    </div>
                                </li>
                                <?php } else { ?>
                                    <li class="news__list-item">
                                    <a href="<?php echo $post_uri; ?>">
                                    <div class="news__list-wrapper open_post">
                                        <picture>
                                            <div class="news__list-image materials"
                                                 style="background: url(<?php if($post_img !== "") echo $post_img; else echo get_template_directory_uri() . "/static/about.jpg"; ?>)"></div>
                                        </picture>
                                        <div class="news__list-text">
                                            <p><?php echo $material->post_title; ?></p>
                                        </div>

                                        <div class="news__list-date">
                                            <span><?php echo $day . " " . $month . " " . $year; ?></span>
                                        </div>
                                    </div>
                                    </a>
                                </li>
                                <?php } ?>
                            <?php } ?>
                        </ul>
                    </div>
                    <!--div class="news__pagination">
                        <ul>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li class="active"><a href="#">4</a></li>
                            <li><a href="#">...</a></li>
                            <li><a href="#">20</a></li>
                        </ul>
                    </div-->
                </div>
            </section>
        </section>
    </div>
</div>
<?php get_footer(); ?>

