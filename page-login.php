<?php
/*
 * Template name: Login page
 */

?>
<?php if ( !wp_is_mobile() ) :
    wp_redirect( home_url() );
endif;
$login_form = get_field('login_form', 'option')[0]; ?>
<?php get_header(); ?>
<div class="container">
    <section class="section__outer">
        <section class="section__inner">
            <div class="login__wrapper">
            <div class="login__title">
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                <h1>Вхід</h1>
            <?php else : ?>
                <h1>Вход</h1>
            <?php endif; ?>
            </div>
            <div class="login__form">
            <form class="form" action="/login" method="post">
                    <div class="form__section">
                        <div class="form__input input-required">
                            <input name="email" id="login_email_mobile" type="email" placeholder="<?php echo $login_form["email_label"]; ?>">
                        </div>

                        <div class="form__input input-required">
                        <input type="password" id="login_pass_mobile" placeholder="<?php echo $login_form["password_label"]; ?>"
                                        required>                   
                        </div>
                        <div id="login_error_message_mobile"></div>
                    </div>
                    <div class="form__submit">
                        <button type="button" onclick="Login_Mobile.request('<?php echo ( wpml_get_current_language() == 'uk' ) ? 'uk' : 'ru' ?>');" class="btn btn-colored">
                            <?php echo $login_form["button_label"]; ?>
                        </button>
                    </div>
                </form>
                 <ul class="login__form-links">
                    <li><a href="<?php echo $login_form["forgot_password"]["url"]; ?>"><?php echo $login_form["forgot_password"]["title"]; ?></a></li>
                    <li><a href="<?php echo $login_form["registration"]["url"]; ?>"><?php echo $login_form["registration"]["title"]; ?></a></li>
                </ul>
            </div>
            </div>
        </section>
    </section>
</div>
<?php get_footer(); ?>
