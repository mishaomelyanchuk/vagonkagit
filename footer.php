<?php
$footer = get_field('options_footer', 'option');
if(count($footer) > 0) $footer = $footer[0];
?>

<footer class="footer">
  <div class="container">
    <section class="section__outer">
      <section class="section__inner">
        <div class="footer__wrapper">
          <div class="footer__logo">
            <?php if ( !empty( $footer['footer-logo'] ) ) { ?>
            <img src="<?php echo $footer['footer-logo']['url']; ?>" alt="">
            <?php } ?>
            <?php if ( !empty( $footer['tekst_pod_logotipom'] ) ) { ?>
            <p><?php echo $footer['tekst_pod_logotipom']; ?></p>
            <?php } ?>
          </div>
          <div class="footer__contacts">
            <ul>
            <?php if ( !empty( $footer['phones'] ) ) { ?>
              <?php foreach ( $footer['phones'] as $item ) { ?>
              <li class="footer__contacts-phone">
                <a href="tel:<?=preg_replace('/[^\d]/', '', $item['phone']);?>"><?php echo $item['phone']; ?></a>
              </li>
              <?php } ?>
              <?php } ?>
              <?php if ( !empty( $footer['emails'] ) ) { ?>
              <?php foreach ( $footer['emails'] as $item ) { ?>
              <li class="footer__contacts-mail">
                <a href="mailto:<?=preg_replace('/[^\d]/', '', $item['email']);?>"><?php echo $item['email']; ?></a>
              </li>
              <?php } ?>
              <?php } ?>
            </ul>
          </div>
          <?php if ( !empty( $footer['footer_menu'] ) ) { ?>
          <nav class="footer__nav">
            <ul>
            <?php foreach ( $footer['footer_menu'] as $item ) { ?>
              <li><a href="<?php echo $item['menu_link']['url']; ?>"><?php echo $item['menu_link']['title']; ?></a></li>
            <?php } ?>
            </ul>
          </nav>
          <?php } ?>
          <div class="footer__politics">
            <ul>
            <?php if ( !empty( $footer['ssylka_polzovatelskogo_soglashenie'] ) ) { ?>
              <li>
                <a href="<?php echo $footer['ssylka_polzovatelskogo_soglashenie']['url'];?>"><?php echo $footer['ssylka_polzovatelskogo_soglashenie']['title'];?></a>
              </li>
            <?php } ?>
            <?php if ( !empty( $footer['ssylka_politiki_konfidenczialnosti'] ) ) { ?>
              <li>
                <a href="<?php echo $footer['ssylka_politiki_konfidenczialnosti']['url']; ?>"><?php echo $footer['ssylka_politiki_konfidenczialnosti']['title']; ?></a>
              </li>
            <?php } ?>
            <?php if ( !empty( $footer['ssylka_cookies'] ) ) { ?>
              <li>
                <a href="<?php echo $footer['ssylka_cookies']['url']; ?>"><?php echo $footer['ssylka_cookies']['title']; ?></a>
              </li>
            <?php } ?>
            <?php if ( !empty( $footer['ssylka_public_offer'] ) ) { ?>
              <li>
                <a href="<?php echo $footer['ssylka_public_offer']['url']; ?>"><?php echo $footer['ssylka_public_offer']['title']; ?></a>
              </li>
            <?php } ?>
            </ul>
          </div>
          <?php if ( !empty( $footer['other_icons'] ) ) { ?>
          <div class="footer__permissions">
            <?php foreach ( $footer['other_icons'] as $item ) { ?>
            <img src="<?php echo $item['icon']['url']; ?>" alt="">
            <?php } ?>
          </div>
          <?php } ?>
          <?php if ( !empty( $footer['footer_socials'] ) ) { ?>
          <div class="footer__socials">
            <ul>
            <?php foreach ( $footer['footer_socials'] as $item ) { ?>
              <li>
                  <a href="<?php echo $item['social_link']['url']; ?>" target="<?php echo $item['social_link']['target']; ?>">
                      <svg height="18px" width="24px">
                          <use href="<?php echo $item["icon"]["url"] . "#" . strtolower($item["social_link"]["title"]); ?>"></use>
                      </svg>
                      <p><?php echo $item["social_link"]["title"]; ?></p>
                  </a>
              </li>
              <?php } ?>
            </ul>
          </div>
          <?php } ?>
          <?php if ( !empty( $footer['copyright_field'] ) ) { ?>
          <div class="footer__copyrights">
            <p>
             <?php echo $footer['copyright_field']; ?>
            </p>
          </div>
          <?php } ?>
          <div class="footer__madeby">
            <img src="<?=TEMPLATE_PATH?>static/svg-madeby-logo.svg" alt="">
            <?php if ( wpml_get_current_language() == 'uk' ): ?>
              <p>Сайт створено в Page Media Solutions</p>
            <?php else : ?>
              <p>Сайт создан в Page Media Solutions</p>
            <?php endif; ?>
          </div>
        </div>
      </section>
    </section>
  </div>
</footer>

<div>
    <?php $tt = get_field('constacts_map_settings', Page_Contacts::get_ID() )['marks']; ?>
    <?php //print_r($tt); ?>
</div>


<script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" referrerpolicy="no-referrer" /-->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.css">
<script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script>
<!--script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js" integrity="sha512-j7/1CJweOskkQiS5RD9W8zhEG9D9vpgByNGxPIqkO5KrXrwyDAroM9aQ9w8J7oRqwxGyz429hPVk/zR6IOMtSA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script-->
<!--script src="https://cdn.jsdelivr.net/npm/@fancyapps/ui@4.0/dist/fancybox.umd.js"></script-->





<script src="<?=TEMPLATE_PATH?>js/script.js"></script>

    <?php if ( get_the_ID() == Page_About::get_ID() ) : ?>
      <script src="https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js"></script>
      <script src="//naver.github.io/egjs-jquery-pauseresume/release/latest/dist/pauseresume.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/company.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/slider.js"></script>
    <?php endif; ?>

    <?php if ( get_the_ID() == Page_Posts::get_ID() ) : ?>
        <script src="<?=TEMPLATE_PATH?>js/news.js"></script>
    <?php endif; ?>

    <script src="<?=TEMPLATE_PATH?>js/article.js"></script>
    <script src="<?=TEMPLATE_PATH?>js/account.js"></script>

    <?php if ( get_the_ID() == Page_Partners::get_ID() ) : ?>

    <?php endif; ?>

    <?php if ( is_shop() or taxonomy_exists('product_tag') ) : ?>
      <script src="https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js"></script>
      <script src="//naver.github.io/egjs-jquery-pauseresume/release/latest/dist/pauseresume.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/catalog.js"></script>
    <?php endif; ?>

    
    <?php if ( is_cart() ) : ?>
      <script src="https://cdn.jsdelivr.net/npm/interactjs/dist/interact.min.js"></script>
      <script src="//naver.github.io/egjs-jquery-pauseresume/release/latest/dist/pauseresume.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/cart.js"></script>
    <?php endif; ?>

    
    <?php if ( is_product() ) : ?>

      <script src="<?=TEMPLATE_PATH?>js/product.js"></script>
    <?php endif; ?>


  
    <?php if ( is_front_page() ) : ?>
      <script src="<?=TEMPLATE_PATH?>js/indexBgSlider.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/offer.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/indexCtaSlider.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/indexOrderSlider.js"></script>
      <script src="<?=TEMPLATE_PATH?>js/publisity.js"></script>

    <?php endif; ?>



    <?php if ( get_the_ID() == Page_Offices::get_ID() ) { ?>
      <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBj70DjlmDfi-cP-IyRWVV7t-tLXHkNpGQ&amp"></script>
      <script src="<?=TEMPLATE_PATH?>js/offices.js"></script>
    <script>
      const icons = {
          base: '<?php echo TEMPLATE_PATH; ?>static/map_marker.png',
          small: '<?php echo TEMPLATE_PATH; ?>static/map_marker-small.png',
        };
      let features = [];
      <?php $counter = 1; ?>
      <?php $region = get_terms( 'areas',
        array(
          'hide_empty' => true,
        )
      );?>
      <?php foreach ( $region as $item ) { ?>
          <?php $representative = get_posts([
             'numberposts'      => -1,
             'post_type'        => 'offices',
             'tax_query' => array(
              array(
                  'taxonomy' => $item->taxonomy,
                  'field'    => 'slug',
                  'terms'    => $item->slug,
              ),
          ),
          ]);
          ?>
          <?php foreach ( $representative as $office ) { ?>
            <?php if ( get_field( 'office_type' , $office->ID ) == 'regional_office' ) { ?>
                features.push({
                  area: {
                      id: '<?php echo $counter; ?>'
                  },
                  position: new google.maps.LatLng(<?php echo get_field( 'map', $office->ID )['lat']; ?>, <?php echo get_field( 'map', $office->ID )['lng']; ?>),
                  type: {
                      id: 'base',
                      text: '<?php echo get_field('dealers','option')['tekst_regionalnoe_predstavitelstvo'];?>'
                  },
                  name: '<?php echo get_the_title( $office->ID ); ?>',
                  addresses: ["<?php echo get_field( 'adress', $office->ID ); ?>"],
                  phones: [
                    <?php foreach( get_field( 'phones', $office->ID ) as $phones ) { ?>
                      {
                          number: '<?php echo $phones['phone']?>',
                          link: 'tel:<?=preg_replace('/[^\d]/', '', $phones['phone']);?>',
                          text: ', <?php echo $phones['position']?>'
                      },
                    <?php } ?>
                  ],
                  links: [
                    <?php foreach( get_field( 'emails', $office->ID ) as $emails ) { ?>
                      {
                          link: 'mailto:<?php echo $emails['email']; ?>',
                          text: '<?php echo $emails['email']; ?>'
                      },
                    <?php } ?>
                    <?php foreach( get_field( 'links', $office->ID ) as $links ) { ?>
                      {
                          link: '<?php echo $links['link']['url']; ?>',
                          text: '<?php echo $links['link']['title']; ?>'
                      },
                    <?php } ?>
                    
                  ]
                })
            <?php }?>
            <?php if ( get_field( 'office_type', $office->ID ) == 'shop' ) { ?>
                features.push({
                  area: {
                      id: '<?php echo $counter; ?>'
                  },
                  position: new google.maps.LatLng(<?php echo get_field( 'map', $office->ID )['lat']; ?>, <?php echo get_field( 'map', $office->ID )['lng']; ?>),
                  type: {
                      id: 'small',
                      text: '<?php echo get_field('dealers','option')['tekst_magazin'];?>'
                  },
                  name: '<?php echo get_the_title( $office->ID ); ?>',
                  addresses: ["<?php echo get_field( 'adress', $office->ID ); ?>"],
                  phones: [
                    <?php foreach( get_field( 'phones', $office->ID ) as $phones ) { ?>
                      {
                          number: '<?php echo $phones['phone']?>',
                          link: 'tel:<?=preg_replace('/[^\d]/', '', $phones['phone']);?>',
                          text: ', <?php echo $phones['position']?>'
                      },
                    <?php } ?>
                  ],
                  links: [
                    <?php foreach( get_field( 'emails', $office->ID ) as $emails ) { ?>
                      {
                          link: 'mailto:<?php echo $emails['email']; ?>',
                          text: '<?php echo $emails['email']; ?>'
                      },
                    <?php } ?>
                    <?php foreach( get_field( 'links', $office->ID ) as $links ) { ?>
                      {
                          link: '<?php echo $links['link']['url']; ?>',
                          text: '<?php echo $links['link']['title']; ?>'
                      },
                    <?php } ?>
                    
                  ]
                })
            <?php }?>
          <?php }?>
         
          <?php $counter++; ?>
      <?php }?>

      let unique_features = []
      let doubles_features = []
      features.map(function(item){
          let isDouble = unique_features.find(function(findItem){
              if(item.name === findItem.name){
                  if(item.addresses[0] === findItem.addresses[0]){
                      doubles_features.push(item)
                      return true;
                  }
              }
          })

          if(!isDouble){
              unique_features.push(item)
          }
      })
    </script>
    <?php } ?>

    <?php if ( get_the_ID() == Page_Contacts::get_ID() ) { ?>
      <script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBj70DjlmDfi-cP-IyRWVV7t-tLXHkNpGQ&amp"></script>
      <script src="<?=TEMPLATE_PATH?>js/contacts.js"></script>
        <script src="<?=TEMPLATE_PATH?>js/cart.js"></script>
    <script>
 

      const center_setting = new google.maps.LatLng(<?php echo get_field('constacts_map_settings', Page_Contacts::get_ID() )['center']['lat'] ?> ,<?php echo get_field('constacts_map_settings', Page_Contacts::get_ID() )['center']['lng'] ?>);
      const zoon_setting =  <?php echo get_field('constacts_map_settings', Page_Contacts::get_ID() )['zoom']; ?>;
      const icons = {
          base: '<?php echo TEMPLATE_PATH; ?>static/map_marker.png',
          small: '<?php echo TEMPLATE_PATH; ?>static/map_marker-small.png',
        };
      const features = [];
  
      <?php
      foreach ( get_field('constacts_map_settings', Page_Contacts::get_ID() )['marks'] as $map ) {?>
      features.push({
        position: new google.maps.LatLng(<?php echo $map['map']['lat']; ?>, <?php echo$map['map']['lng']; ?>),
        type: 'base'
      })
      <?php }?>
      

      initMap();

    </script>
    <?php } ?>
    <?php wp_footer(); ?>
  </body>
</html>