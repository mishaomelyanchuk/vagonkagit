<?php
global $wpdb;
global $woocommerce;
require_once __DIR__ . "/classes/vg_user.php";
$vg_user = new VG_User();

// $login = isset($_POST["customer-login"]) ? $_POST["customer-login"] : null;
// $password = isset($_POST["customer-password"]) ? $_POST["customer-password"] : null;
// if(!is_null($login) && !is_null($password)) {
//     $result = wp_signon(array(
//         'user_login'    => $login,
//         'user_password' => $password,
//         'remember'      => true
//     ));
//     if(get_class($result) == "WP_Error") {
//         $error_login = "Неверный логин или пароль";
//     } else {
//         wp_redirect("/checkout");
//         exit;
//     }
// }

// $new_customer_fname = isset($_POST["new-customer-fname"]) ? $_POST["new-customer-fname"] : null;
// $new_customer_name  = isset($_POST["new-customer-name"]) ? $_POST["new-customer-name"] : null;
// $new_customer_email = isset($_POST["new-customer-email"]) ? $_POST["new-customer-email"] : null;
// $new_customer_phone = isset($_POST["new-customer-phone"]) ? $_POST["new-customer-phone"] : null;

$user = wp_get_current_user();
if($user->ID == 0) {
    $user = null;
    $user_data = null;
} else {
    $user_data = $vg_user->get_current_user();
}

// if(!is_null($new_customer_fname) &&
//     !is_null($new_customer_name) &&
//     !is_null($new_customer_email) &&
//     !is_null($new_customer_phone)) {
//     $register_data = $vg_user->add_user($new_customer_fname, $new_customer_name, $new_customer_email, $new_customer_phone, 0, "", "", "", 0);
//     $user_data = (array)$vg_user->get_user($new_customer_email);
//     $result = wp_signon(array(
//         'user_login'    => $register_data["login"],
//         'user_password' => $register_data["pass"],
//         'remember'      => true
//     ));
//     // wp_redirect("/checkout");
//     exit;
// }

$items = $woocommerce->cart->get_cart();
function get_product_attributes(WC_Product_Variation $product_variation, $attr_name) {
    $attributes = $product_variation->get_attributes();
    $slug = $attributes["pa_" . $attr_name];
    return get_term_by('slug', $slug, "pa_" . $attr_name);
}

function get_price(WC_Product_Variation $data) {
    return (int)$data->get_price();
}

$cart_count = WC()->cart->get_cart_contents_count();
//$cart_total = WC()->cart->get_cart_total();
$amount = WC()->cart->get_total(null);
$cart_count_text = $cart_count . "&nbsp;товара";
if ( $cart_count == 1 ) {
    $cart_count_text = $cart_count . "&nbsp;товар";
}

if ( wpml_get_current_language() == 'uk' ) {
    if($cart_count_text > 4) $cart_count_text = $cart_count . "&nbsp;товарів";

} else {
    if($cart_count_text > 4) $cart_count_text = $cart_count . "&nbsp;товаров";
}
   

$flat_rate = new WC_Shipping_Flat_Rate(6);
$courier_cost = $flat_rate->instance_settings["cost"];

if(isset($_POST["action"]) && $_POST["action"] == "create_order") {
    $new_user = isset($_POST["new_user"]) ? $_POST["new_user"] : "";
    $payment_method = isset($_POST["payment_method"]) ? $_POST["payment_method"] : "on_receive";
    $delivery_method = isset($_POST["delivery"]) ? $_POST["delivery"] : "self";
    $delivery_cost = isset($_POST["delivery_cost"]) ? $_POST["delivery_cost"] : "0";
    $delivery_address = isset($_POST["delivery_address"]) ? $_POST["delivery_address"] : "Не указан";
    $total_amount = isset($_POST["amount"]) ? $_POST["amount"] : null;
    $promo_code = isset($_POST["promo_code"]) ? $_POST["promo_code"] : null;
    require_once __DIR__ . "/classes/vg_order.php";
    $vg_order = new VG_Order();
    $new_order = $vg_order->create(array(
        "payment_method"   => $payment_method,
        "delivery_method"  => $delivery_method,
        "delivery_address" => $delivery_address,
        "delivery_cost"    => $delivery_cost,
        "total_amount"     => $total_amount,
        "promo_code"       => $promo_code
    ));
    $order_id = $new_order["order_id"];
    if($payment_method === "liqpay") {
        require_once __DIR__ . "/classes/LiqPay.php";
        $settings = get_option('woocommerce_liqpay-webplus_settings');
        $order = wc_get_order( $order_id );
        if ( $new_user ) {
            $result_url = $order->get_checkout_order_received_url() . '?new_user=1';
        } else {
            $result_url = $order->get_checkout_order_received_url();
        }
        $liqp = array(
            'action' => 'pay',
            'amount' => $total_amount,
            'currency' => 'UAH',
            'description' => 'Заказ №' . $order_id,
            'order_id' => $order_id,
            'language' => 'ru',
            'version' => '3',
            'server_url' => str_replace( '/ru', '' , home_url() ) . '/wp-admin/admin-ajax.php?action=set_new_order_status' ,
            'result_url' => Page_Liqpay_Success::get_url() . '?order='. base64_encode( $order_id ) . '&new_user='. $new_user ,
            // 'result_url' => $result_url,
            'type' => 'buy'
        );
        $liqPay = new LiqPay($settings["public_key"], $settings["private_key"]);
        $link = $liqPay->cnb_link($liqp);
        wp_redirect($link);
        exit;
    }

    if ( $payment_method == 'invoice' or $payment_method == 'cheque' ) { 
        update_post_meta( $order_id, 'invoce_generete', '1' );
    }

    $order = wc_get_order( $new_order["order_id"] );
    if ( $new_user ) {
        $redirect_url = $order->get_checkout_order_received_url() . '?new_user=1';
    } else {
        $redirect_url = $order->get_checkout_order_received_url();
    }
    wp_redirect($redirect_url);
}

$np_areas = $wpdb->get_results("SELECT * FROM `wc_ukr_shipping_np_areas` ORDER BY `description`");

$shipping_classes = WC()->shipping()->get_shipping_classes();

?>

<?php get_header(); ?>
    <div class="order" data-lang="<?php echo wpml_get_current_language(); ?>">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="order__wrapper">
                        <div class="order__title">
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                <h1>Оформлення замовлення</h1>
                            <?php else : ?>
                                <h1>Оформление заказа</h1>
                            <?php endif; ?>                        
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
<?php if(is_wc_endpoint_url( 'order-received' )) { ?>
    <?php 
        global $wp;
        $order_id  = absint( $wp->query_vars['order-received'] );    
        $payment_method = get_post_meta( $order_id, '_payment_method', true );
        $order = wc_get_order( $order_id );
        $order_status = $order->get_status();
    ?>
    <?php if ( $payment_method === "liqpay" or $payment_method === "liqpay-webplus" ) : ?>
        <?php if ( $order_status == 'processing' ) : ?>
            <div class="order__details">
                <div class="container">
                    <section class="section__outer">
                        <section class="section__inner">
                            <div class="order__details-wrapper">
                                <div class="order__products">
                                    <div class="order__products-header">
                                        <div>
                                            <p>
                                                <?php echo str_replace( '[order_id]', '№ ' . $order_id , get_field( 'checkout_settings', 'option' )['checkout_text']); ?>
                                            </p>
                                            <br><br><br>
                                            <?php if ( !empty( $_GET['new_user'] ) ) : ?>
                                                <p><?php echo get_field('success_order','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></p><br><br>
                                            <?php endif; ?> 
                                            <div class="order__checkout-submit">
                                                <button type="button" class="btn btn-colored btn-big" onclick="window.location.href='<?php echo get_field( 'checkout_settings', 'option' )['button']['url']; ?>';"><?php echo get_field( 'checkout_settings', 'option' )['button']['title']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        <?php else : ?>
            <div class="order__details">
                <div class="container">
                    <section class="section__outer">
                        <section class="section__inner">
                            <div class="order__details-wrapper">
                                <div class="order__products">
                                    <div class="order__products-header">
                                        <div>
                                            <?php 
                                            require_once __DIR__ . "/classes/LiqPay.php";
                                            $settings = get_option('woocommerce_liqpay-webplus_settings');
                                            if ( $new_user ) {
                                                $result_url = $order->get_checkout_order_received_url() . '?new_user=1';
                                            } else {
                                                $result_url = $order->get_checkout_order_received_url();
                                            }
                                            $liqp = array(
                                                'action' => 'pay',
                                                'amount' => $order->get_total(),
                                                'currency' => 'UAH',
                                                'description' => 'Заказ №' . $order_id,
                                                'order_id' => $order_id,
                                                'language' => 'ru',
                                                'version' => '3',
                                                'server_url' => str_replace( '/ru', '' , home_url() ) . '/wp-admin/admin-ajax.php?action=set_new_order_status' ,
                                                'result_url' => Page_Liqpay_Success::get_url() . '?order='. $_GET['order'] . '&new_user=' ,
                                                // 'result_url' => $result_url,
                                                'type' => 'buy'
                                            );
                                            $liqPay = new LiqPay($settings["public_key"], $settings["private_key"]);
                                            $link = $liqPay->cnb_link($liqp);
                                            ?>
                                            <p>
                                            <?php echo get_field('success_order','option')['tekst_u_vas_est_chas_dlya_povtornoj_oplaty_zakaza_vy_mozhete_sdelat_eto_po_ssylke_v_nizu_ili_po_ssylka_v_moem_kabineta_na_danom_zakaze']; ?>
                                            </p>
                                            <br><br><br>
                                            <p>
                                                <a href="<?php echo $link?>"> <?php echo get_field('success_order','option')['tekst_ssylka_na_povtornuyu_oplatu']; ?></a>
                                            </p>
                                            <br><br>

                                            <?php if ( !empty( $_GET['new_user'] ) ) : ?>
                                                <p><?php echo get_field('success_order','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></p><br><br>
                                            <?php endif; ?> 
                                            <div class="order__checkout-submit">
                                                <button type="button" class="btn btn-colored btn-big" onclick="window.location.href='<?php echo get_field( 'checkout_settings', 'option' )['button']['url']; ?>';"><?php echo get_field( 'checkout_settings', 'option' )['button']['title']; ?></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                </div>
            </div>
        <?php endif; ?>
    <?php else : ?>
        <div class="order__details">
            <div class="container">
                <section class="section__outer">
                    <section class="section__inner">
                        <div class="order__details-wrapper">
                            <div class="order__products">
                                <div class="order__products-header">
                                    <div>
                                        <p>
                                            <?php echo str_replace( '[order_id]', '№ ' . $order_id, get_field( 'checkout_settings', 'option' )['checkout_text']); ?>
                                        </p><br>
                                        <?php  $current_user = wp_get_current_user();
                                        if ( $current_user->data->user_type == '1' and ( $payment_method == 'invoice' or $payment_method == 'cheque' ) ) :?>
                                        <?php echo do_shortcode('[wcpdf_download_invoice link_text="'. get_field( 'checkout_settings', 'option' )['dow_link'] .'" order_id="'.$order_id .'"]'); ?>
                                        <?php endif; ?>
                                        <br><br><br>
                                        <?php if ( !empty( $_GET['new_user'] ) ) : ?>
                                            <p><?php echo get_field('success_order','option')['tekst_na_vashu_elektronnuyu_pochtu_bylo_vyslano_pismo_s_dalnejshimi_instrukcziyami_dlya_aktivaczii_akkaunta']; ?></p><br><br>
                                        <?php endif; ?> 
                                        <div class="order__checkout-submit">
                                            <button type="button" class="btn btn-colored btn-big" onclick="window.location.href='<?php echo get_field( 'checkout_settings', 'option' )['button']['url']; ?>';"><?php echo get_field( 'checkout_settings', 'option' )['button']['title']; ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        </div>
    <?php endif; ?>
<?php } else { ?>
    <div class="order__details">
        <div class="container">
            <section class="section__outer">
                <section class="section__inner">
                    <div class="order__details-wrapper">
                        <div class="order__products">
                            <div class="order__products-header">
                                <div>
                                    <h2><?php echo get_field('checkout','option')['tekst_zakaz']; ?></h2>
                                </div>
                                <div><a href="<?php echo wc_get_cart_url(); ?>"><?php echo get_field('checkout','option')['tekst_vernutsya_k_korzine']; ?></a></div>
                            </div>
                            <ul class="order__products-list">
                                <?php $i = 1; foreach($items as $item => $values) { ?>
                                    <?php
                                    $_product = $values['data'];
                                    if ( $_product->is_on_sale() ) {
                                        $current_user = wp_get_current_user();
                                        if ( $current_user->data->user_type == '1' ) {
                                            if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                                $regular_price = $_product->regular_price - ( $_product->regular_price / 100 * get_field( 'b2b_sale', 'option' ) );
                                                $sale_price = $_product->sale_price - ( $_product->sale_price / 100 * get_field( 'b2b_sale', 'option' ) );
                                            }
                                            $cart_total += $regular_price * $values['quantity'];
                                            $discount = ($regular_price - $sale_price) * $values['quantity'];
                                            $discount_total += $discount;
                                        } else {
                                            $cart_total += $_product->regular_price * $values['quantity'];
                                            $regular_price = $_product->regular_price;
                                            $sale_price = $_product->sale_price;
                                            $discount = ($regular_price - $sale_price) * $values['quantity'];
                                            $discount_total += $discount;
                                        }
                                    
                                    } else {
                                        if ( $current_user->data->user_type == '1' ) {
                                            if ( !empty( get_field( 'b2b_sale', 'option' ) ) ) {
                                                $regular_price = $_product->regular_price - ( $_product->regular_price / 100 * get_field( 'b2b_sale', 'option' ) );
                                            }
                                            $cart_total += $regular_price * $values['quantity'];
                                           
                                        } else {
                                            $cart_total += $_product->regular_price * $values['quantity'];
                                        }
                                    }

                                    $image = wp_get_attachment_image_src(get_post_thumbnail_id($values["product_id"]), 'single-post-thumbnail');
                                    $product = wc_get_product($values["product_id"]);
                                    $obem = get_product_attributes($values["data"], "obem");
                                    $czvet = get_product_attributes($values["data"], "czvet");
                                    $data = $values["data"];
                                    // $color = "#fff";
                                    // switch ($czvet->term_id) {
                                    //     case 28:
                                    //         $color = "#fff";
                                    //         break;
                                    //     case 29:
                                    //         $color = "#0f0f0f";
                                    //         break;
                                    //     case 30:
                                    //         $color = "#757575";
                                    //         break;
                                    //     case 31:
                                    //         $color = "#757575";
                                    //         break;
                                    //     case 32:
                                    //         $color = "rgb(251, 213, 13)";
                                    //         break;
                                    // }

                                    ?>
                                    <li class="order__products-item">
                                        <div class="order__products-position"><span> <?php echo $i; $i++; ?> </span></div>
                                        <picture>
                                            <img src="<?php echo isset($image[0]) && $image[0] != "" ? $image[0] : get_template_directory_uri() . "/static/svg-empty-file.svg"; ?>" alt="">
                                        </picture>
                                        <div class="order__products-title">
                                            <a href="<?php echo $product->get_permalink(); ?>"><?php echo $product->get_title(); ?></a>
                                        </div>
                                        <div class="order__products-info">
                                            <div class="order__products-info_size"><?php echo $obem->name; ?></div>
                                            <div class="order__products-info_color" style="background-color: <?php echo get_field( 'color', 'term_'.$czvet->term_id ); ?>"></div>
                                            <div class="order__products-info_name"><?php echo $czvet->name; ?></div>
                                        </div>
                                        <ul class="order__products-price">
                                            <li>
                                                <p><?php echo get_field('checkout','option')['tekst_czena']; ?></p>
                                                <span><?php echo bcdiv($data->price,1,2); ?><span> </span><span class="currency">₴</span></span>
                                            </li>
                                            <li>
                                                <p><?php echo get_field('checkout','option')['tekst_kol-vo']; ?></p>
                                                <span><?php echo $values["quantity"] ?></span>
                                            </li>
                                            <li>
                                                <p><?php echo get_field('checkout','option')['tekst_summa']; ?></p>
                                                <span><?php echo bcdiv($data->price * $values["quantity"],1,2); ?><span> </span><span class="currency">₴</span></span>
                                            </li>
                                        </ul>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <div class="order__client">
                            <div class="order__client-title">
                                <h2><?php echo get_field('checkout','option')['tekst_kontaktnye_dannye']; ?></h2>
                            </div>
                            <?php if(!is_null($user_data)) { ?>
                                <input type="hidden" name="client_id" id="order__client-id" value="<?php echo $user_data["ID"]; ?>"/>
                                
                                    <p><?php echo $user_data["customer-name"] . " " . $user_data["customer-fname"]; ?><br>
                                        <?php echo $user_data["phone"]; ?></br>
                                        <?php echo $user_data['user_email']; ?>
                                    </p>
                            
                            <?php } else { ?>
                                <input type="hidden" name="client_id" id="order__client-id" value="-1"/>
                                <div class="order__client-toggle">
                                    <ul>
                                        <li class="active" data-field="order-login"><?php echo get_field('checkout','option')['tekst_vojti']; ?>
                                           
                                        </li>
                                        <li data-field="order-new-client">
                                        <?php echo get_field('checkout','option')['tekst_novyj_klient']; ?>
                                        </li>
                                    </ul>
                                </div>
                                <div data-field="order-login" class="order__client-fields login-client active">
                                    <!-- <?php if(isset($error_login)) { ?>
                                        <p class="error"><?php echo $error_login; ?></p>
                                    <?php } ?> -->
                                    <p class="error" id="error_log" style="display: none;"><?php echo get_field('checkout','option')['tekst_nevernyj_login_ili_parol']; ?></p>
                                    <form class="form" method="post">
                                        <div class="form__input input-required">
                                            <input name="customer-login" id="checkout_login" type="text" placeholder="<?php echo get_field('checkout','option')['tekst_elektronnaya_pochta']; ?>" required>
                                        </div>
                                        <div class="form__input input-required">
                                            <input name="customer-password" id="checkout_password" type="password" placeholder="<?php echo get_field('checkout','option')['tekst_parol']; ?>" required>
                                        </div>
                                        <div class="form__submit">
                                            <button type="button" onclick="Login_Checkout.request();" class="btn btn-colored"><?php echo get_field('checkout','option')['tekst_vojti']; ?></button>
                                        </div>
                                    </form>
                                </div>
                                <div data-field="order-new-client" class="order__client-fields new-client">
                                    <!-- <form class="form" method="post" onsubmit="return registration.checkForm();"> -->
                                    <p class="error" id="error_reg" style="display: none;"><?php echo get_field('checkout','option')['tekst_imya']; ?></p>
                                    <form class="form" method="post">
                                        <div class="form__input input-required">
                                            <input id="name" name="new-customer-name" type="text" placeholder="<?php echo get_field('checkout','option')['tekst_imya']; ?>" required>
                                        </div>
                                        <div class="form__input input-required">
                                            <input id="fname" name="new-customer-fname" type="text" placeholder="<?php echo get_field('checkout','option')['tekst_familiya']; ?>" required>
                                        </div>
                                        <div class="form__input input-required">
                                            <input id="email" name="new-customer-email" type="email" placeholder="<?php echo get_field('checkout','option')['tekst_elektronnaya_pochta']; ?>" required>
                                        </div>
                                        <div class="form__input input-required">
                                            <input id="phone" name="new-customer-phone" type="tel" placeholder="<?php echo get_field('checkout','option')['tekst_kontaktnyj_telefon']; ?>" required>
                                        </div>
                                        <div class="form__submit">
                                            <!-- <button type="submit" class="btn btn-colored">Регистрация</button> -->
                                            <button type="button" class="btn btn-colored" onclick="Registration_Checkout.request();"><?php echo get_field('checkout','option')['tekst_registracziya']; ?></button>
                                        </div>
                                    </form>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="order__delivery">
                            <div class="order__delivery-title">
                                <h2><?php echo get_field('checkout','option')['tekst_oplata_i_dostavka']; ?></h2>
                            </div>
                            <div class="order__delivery-type">
                                <div class="order__delivery-subtitle">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <h3>Доставка</h3>
                                <?php else : ?>
                                    <h3>Доставка</h3>
                                <?php endif; ?>
                                </div>
                                <div class="form">
                                    <div class="form__radio">
                                        <label for="self">
                                            <input type="radio" delivery_type="self" name="delivery" value="0" id="self" checked="checked">
                                            <div class="form__radio-custom"></div>
                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                <span>Самовивіз</span>
                                            <?php else : ?>
                                                <span>Самовывоз</span>
                                            <?php endif; ?>
                                        </label>
                                    </div>
                                    <!-- <div class="form__radio">
                                        <label for="courier">
                                            <input type="radio" delivery_type="courier" name="delivery" value="<?php echo $courier_cost; ?>" id="courier">
                                            <div class="form__radio-custom"></div>
                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                <span>Доставка кур`єром</span>
                                            <?php else : ?>
                                                <span>Доставка курьером</span>
                                            <?php endif; ?>
                                        </label>
                                    </div> -->
                                    <div class="form__radio">
                                        <label for="novaposhta">
                                            <input type="radio" delivery_type="novaposhta" value="novaposhta" name="delivery" id="novaposhta">
                                            <div class="form__radio-custom"></div>
                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                <span>Нова Пошта</span>
                                            <?php else : ?>
                                                <span>Новая Почта</span>
                                            <?php endif; ?>
                                        </label>
                                    </div>
                                    <form class="order__delivery-descriptions">
                                        <div class="order__delivery-descriptions_item show" data-delivery="self">
                                            <input type="hidden" name="delivery" id="courier" class="not-required">
                                            <?php if ( !empty( get_field( 'delivery_methods', 'option' )['tekst_samovyvoza'] ) ) : ?>
                                            <p>
                                                <?php echo get_field( 'delivery_methods', 'option' )['tekst_samovyvoza']; ?>
                                            </p>
                                            <?php endif; ?>
                                            <!--h3><?php _e( 'Billing details', 'woocommerce' ); ?></h3>
                                            <table class="pilot_cafe_custom_show_shipping">
                                                <?php if ( WC()->cart->needs_shipping() && WC()->cart->show_shipping() ) : ?>
                                                    <?php do_action( 'woocommerce_review_order_before_shipping' ); ?>
                                                    <?php wc_cart_totals_shipping_html(); ?>
                                                    <?php do_action( 'woocommerce_review_order_after_shipping' ); ?>
                                                <?php endif; ?>
                                            </table-->
                                            <select name="pickup" id="delivery_self_id" required>
                                                <option value="">
                                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                    Виберіть представництво або магазин
                                                <?php else : ?>
                                                    Выберите представительство или магазин
                                                <?php endif; ?>
                                                </option>
                                                <?php foreach($shipping_classes as $shipping_class) { ?>
                                                    <option value="<?php echo $shipping_class->description ?>"><?php echo $shipping_class->description ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="order__delivery-descriptions_item" data-delivery="courier">
                                            <?php if ( !empty( get_field( 'delivery_methods', 'option' )['tekst_curier'] ) ) : ?>
                                            <p>
                                                <?php echo get_field( 'delivery_methods', 'option' )['tekst_curier']; ?>
                                            </p>
                                            <?php endif; ?>
                                            <div class="form__input" style="max-width: 100%; margin: 0;">
                                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                    <input name="courier_address" id="courier_address_id" type="text" placeholder="Введіть адресу доставки">
                                                <?php else : ?>
                                                    <input name="courier_address" id="courier_address_id" type="text" placeholder="Введите адрес доставки">
                                                <?php endif; ?>
                                            </div>
                                        </div>
                                        <div class="order__delivery-descriptions_item" data-delivery="novaposhta">
                                            <?php if ( !empty( get_field( 'delivery_methods', 'option' )['tekst_nova'] ) ) : ?>
                                            <p>
                                                <?php echo get_field( 'delivery_methods', 'option' )['tekst_nova']; ?>
                                            </p>
                                            <?php endif; ?>
                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                <h4>Вкажіть адресу доставки</h4><br>
                                            <?php else : ?>
                                                <h4>Укажите адрес доставки</h4><br>
                                            <?php endif; ?>
                                            <select name="novaposhta_region" id="novaposhta_region" class="allow-type">
                                            <?php if ( wpml_get_current_language() == 'ru' ) : ?>
                                                <option value="">Выберите область</option>
                                            <?php else : ?>
                                                <option value="">Виберіть область</option>
                                            <?php endif; ?>
                                                <?php foreach($np_areas as $area) { ?>
                                                    <option value="<?php echo $area->ref ?>"><?php echo $area->description; ?></option>                
                                                <?php } ?>
                                            </select><br><br>
                                            <select name="novaposhta_city" id="novaposhta_city" class="allow-type">
                                                <?php if ( wpml_get_current_language() == 'ru' ) : ?>   
                                                    <option value="">Выберите город</option>
                                                <?php else : ?>
                                                    <option value="">Виберіть місто</option>
                                                <?php endif; ?>
                                            </select><br><br>
                                            <select name="novaposhta_houses" id="novaposhta_houses" class="allow-type">
                                                <?php if ( wpml_get_current_language() == 'ru' ) : ?>  
                                                    <option value="">Выберите отделение</option>
                                                <?php else : ?>
                                                    <option value="">Виберіть відділення</option>
                                                <?php endif; ?>
                                            </select>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="order__delivery-payment">
                                <div class="order__delivery-subtitle"><p>Оплата</p></div>
                                <?php //woocommerce_checkout_payment(); ?>
                                
                                <div class="form">
                                    <div class="form__radio" style="padding-top: 5px;">
                                        <label for="cash">
                                            <input type="radio" value="on_receive" name="delivery-payment" id="cash" checked="checked">
                                            <div class="form__radio-custom"></div>
                                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                <span>При отриманні</span>
                                            <?php else : ?>
                                                <span>При получении</span>
                                            <?php endif; ?>
                                        </label>
                                    </div>
                                    <!-- <div class="form__radio">
                                        <label for="cash_liqpay">
                                            <input type="radio" value="liqpay" name="delivery-payment" id="cash_liqpay">
                                            <div class="form__radio-custom"></div>
                                            <span>Оплата онлайн (LiqPay)</span>
                                        </label>
                                    </div> -->
                                    <?php  $current_user = wp_get_current_user();
                                    if ( $current_user->data->user_type == '1' ) : ?>
                                        <div class="form__radio">
                                            <label for="invoice_generation">
                                                <input type="radio" value="invoice" name="delivery-payment" id="invoice_generation">
                                                <div class="form__radio-custom"></div>
                                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                                    <span>Формування рахунку</span>
                                                <?php else : ?>
                                                    <span>Формирование счета</span>
                                                <?php endif; ?>
                                            </label>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="order__checkout">
                            <div class="order__checkout-title">
                            <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                <h2>всього</h2>
                            <?php else : ?>
                                <h2>итого</h2>
                            <?php endif; ?>
                            </div>
                            <ul class="order__checkout-counts">
                                <li>
                                    <p><?php echo $cart_count_text; ?></p>
                                    <span><?php echo bcdiv($cart_total,1,2); ?><span> </span><span class="currency">₴</span></span>
                                </li>
                                <?php if ( $discount_total > 0 ) : ?>
                                <li class="accent">
                                    <?php if ( wpml_get_current_language() == 'ru' ) : ?>
                                        <p>Cкидка</p>
                                    <?php else : ?>
                                        <p>Знижка</p>
                                    <?php endif; ?>
                                    <span>-<?php echo bcdiv($discount_total,1,2); ?><span> </span><span class="currency">₴</span></span>
                                </li>
                                <?php endif; ?>
                                <li>
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <p>Вартість доставки</p>
                                <?php else : ?>
                                    <p>Стоимость доставки</p>
                                <?php endif; ?>
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <span id="delivery_cost_id">безкоштовно</span>
                                <?php else : ?>
                                    <span id="delivery_cost_id">бесплатно</span>
                                <?php endif; ?>
                                </li>
                                <li class="accent" id="discount_wrapper_id" style="display: none;">
                                    <p id="promo_description"></p>
                                    <span id="promo_amount"><span>
                                </li>
                            </ul>
                            <?php //woocommerce_checkout_coupon_form(); ?>
                            <div class="order__checkout-code">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <span class="code-toggle">Є промокод?</span>
                                <?php else : ?>
                                    <span class="code-toggle">Есть промокод?</span>
                                <?php endif; ?>
                                 <form class="form">
                                     <div class="form__input">
                                         <input type="text" id="promo_el" placeholder="Ввести промокод">
                                     </div>
                                     <div class="form__submit">
                                         <button type="button" class="btn btn-colored" onclick="checkout.setPromoCode();">ок</button>
                                     </div>
                                 </form>
                             </div>
                            <div class="order__checkout-total">
                                <div class="order__checkout-total-text">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <p>До оплати</p>
                                <?php else : ?>
                                    <p>К оплате</p>
                                <?php endif; ?>
                                </div>
                                <div class="order__checkout-price">
                                    <p>
                                        <span class="woocommerce-Price-amount amount">
                                            <bdi><span id="total_price_id"><?php echo $amount; ?></span>&nbsp;
                                                <span class="woocommerce-Price-currencySymbol">₴
                                            </span>
                                            </bdi>
                                        </span>
                                    </p>
                                </div>
                                <div class="order__checkout-total-vat">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <p>В т.ч. ПДВ 20%</p>
                                <?php else : ?>
                                    <p>В т.ч. НДС 20%</p>
                                <?php endif; ?>
                                </div>
                            </div>
                            <form method="post" onsubmit="return checkout.onSubmit();">
                                <input type="hidden" name="action" value="create_order">
                                <input type="hidden" name="delivery" value="self" id="delivery_id">
                                <input type="hidden" name="delivery_cost" value="0" id="shipping_cost_id">
                                <input type="hidden" name="delivery_address" id="delivery_address_id">
                                <input type="hidden" name="payment_method" value="on_receive" id="payment_method_id">
                                <input type="hidden" name="promo_code" value="" id="promo_code_id">
                                <input type="hidden" name="new_user" value="" id="new_user_stat">
                                <input id="order_total" type="hidden" name="amount" value="<?php echo $amount; ?>">
                                <div class="order__checkout-submit">
                                <?php if ( wpml_get_current_language() == 'uk' ): ?>
                                    <button type="submit" class="btn btn-colored btn-big">замовити</button>
                                <?php else : ?>
                                    <button type="submit" class="btn btn-colored btn-big">заказать</button>
                                <?php endif; ?>
                                </div>
                            </form>
                            <div class="order__checkout-terms">
                                <?php echo get_field('tekst_soglasheniya'); ?>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </div>
    </div>
<?php } ?>



<?php 

get_footer(); ?>